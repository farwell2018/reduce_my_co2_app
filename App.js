/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from "react";
import { NativeBaseProvider, Box } from 'native-base';
import {
  LogBox,
} from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Toast from "react-native-toast-message";
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from "react-native/Libraries/NewAppScreen";
import { Icon } from "react-native-elements";
// Import Navigators from React Navigation
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

// Import Screens
import SplashScreen from "./Screen/SplashScreen";
import LoginScreen from "./Screen/LoginScreen";
import RegisterScreen from "./Screen/RegisterScreen";
import ValidationScreen from "./Screen/ValidationScreen";
import SuccessScreen from "./Screen/SuccessScreen";
import DrawerNavigationRoutes from "./Screen/DrawerNavigationRoutes";
import ProfileScreen from "./Screen/DrawerScreens/ProfileScreen";
import EcoProfile from "./Screen/DrawerScreens/EcoProfile"
import CongratsScreen from "./Screen/Components/Congratulations";
import EcoProfileScreen from "./Screen/DrawerScreens/EcoProfile";
import EcoDisplayScreen from "./Screen/EcoDisplayScreen";
import VehicleDetails from "./Screen/Components/VehicleDetails";
import FlightScreen from "./Screen/FlightScreen";
import DietScreen from "./Screen/DietScreen";
import HouseholdScreen from "./Screen/HouseholdScreen";
import Car from "./Image/SVG/car_tab.svg"
import Plane from "./Image/SVG/plane_tab.svg"
import Home from "./Image/SVG/home_tab.svg"
import Food from "./Image/SVG/food_tab.svg";

const Tab = createMaterialTopTabNavigator();
const Stack = createStackNavigator();
LogBox.ignoreAllLogs(true); 
const Auth = () => {
  // Stack Navigator for Login and Sign up Screen
  return (
    <Stack.Navigator initialRouteName="LoginScreen">
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{
          title: "",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{
          title: "", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="ValidationScreen"
        component={ValidationScreen}
        options={{
          title: "", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="SuccessScreen"
        component={SuccessScreen}
        options={{
          title: "", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Edit Profile"
        component={ProfileScreen}
        options={{
          title: "", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
};
const EcoProfileTabs = () => {
    return (
      <Tab.Navigator
        initialRouteName="Vehicle"
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            let iconColor = focused ? "#23A9C6" : "#c2c3c8";
            if (route.name === "Vehicle") {
              iconName = focused ? "directions-car" : "directions-car";
            } else if (route.name === "Flight") {
              iconName = focused ? "flight" : "flight";
            } else if (route.name === "Diet") {
              iconName = focused ? "restaurant" : "restaurant";
            } else if (route.name === "Home") {
              iconName = focused ? "house" : "house";
            }

            
            return <Icon name={iconName} size={18} color={iconColor} raised />;
          },
        })}
        tabBarOptions={{
          showIcon: true,
          showLabel: false,
          activeTintColor: "#23A9C6",
          inactiveTintColor: "gray",
          iconStyle: {
            width: "auto",
            height: 50,
          },
        }}
      >
        <Tab.Screen
          name="Vehicle"
          component={EcoProfile}
          // options={{
          //   tabBarShowIcon: true,
          //   tabBarIcon: <Home />,
          // }}
        />
        <Tab.Screen name="Flight" component={FlightScreen} />
        <Tab.Screen name="Diet" component={DietScreen} />
        <Tab.Screen name="Home" component={HouseholdScreen} />
      </Tab.Navigator>
    );
}
const App = () => {
  return (
    <NativeBaseProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashScreen">
          {/* SplashScreen which will come once for 5 Seconds */}
          <Stack.Screen
            name="SplashScreen"
            component={SplashScreen}
            // Hiding header for Splash Screen
            options={{ headerShown: false }}
          />
          {/* Auth Navigator: Include Login and Signup */}
          <Stack.Screen
            name="Auth"
            component={Auth}
            options={{ headerShown: false }}
          />
          {/* Navigation Drawer as a landing page */}
          <Stack.Screen
            name="DrawerNavigationRoutes"
            component={DrawerNavigationRoutes}
            // Hiding header for Navigation Drawer
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Eco Profile"
            component={EcoProfileTabs}
            // Hiding header for Navigation Drawer
            options={{
              title: "Eco Profile", //Set Header Title
              headerStyle: {
                backgroundColor: "#7EC904", //Set Header color
              }, //Set Header Title
              headerTintColor: "#fff", //Set Header text color
              headerTitleStyle: {
                fontWeight: "bold", //Set Header text style
              },
            }}
          />
        </Stack.Navigator>
        <Toast swipeEnabled={true} ref={(ref) => Toast.setRef(ref)} />
      </NavigationContainer>
    </NativeBaseProvider>
  );
};

export default App;
