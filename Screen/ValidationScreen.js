import * as React from "react";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { View, useWindowDimensions, StyleSheet, Text } from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import EcoProfileScreen from "./DrawerScreens/EcoProfile";
import { Icon } from "react-native-elements";
import { HScrollView } from "react-native-head-tab-view";
import { CollapsibleHeaderTabView } from "react-native-tab-view-collapsible-header";


const getTabBarIcon = (props) => {
  const { route } = props;

  if (route.key === "vehicle") {
    return <Icon name="directions-car" size={30} color={"#23A9C6"} raised />;
  } else if (route.key === "flight")  {
    return <Icon name="flight" size={30} color={"#23A9C6"} raised />;
  }else if (route.key === "restaurant") {
    return <Icon name="restaurant" size={30} color={"#23A9C6"} raised />;
  } else {
    return <Icon name="house" size={30} color={"#23A9C6"} raised />;
  }
};
const FirstRoute = () => (
  <HScrollView index={0}>
    <EcoProfileScreen />
  </HScrollView>
);

const SecondRoute = () => <View style={{ flex: 1, backgroundColor: "#ff4081" }} />;

const renderScene = SceneMap({
  vehicle: FirstRoute,
  flight: SecondRoute,
  restaurant: SecondRoute,
  house: SecondRoute,
});

export default function TabViewExample() {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "vehicle", title: "Vehicle", },
    { key: "flight", title: "Flight" },
    { key: "restaurant", title: "Restaurant" },
    { key: "house", title: "House" },
  ]);

  return (
    <CollapsibleHeaderTabView
      renderScrollHeader={() => (
        <View>
          <Text style={styles.loginheaderStyle}>Eco Profile</Text>
        </View>
      )}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
      renderTabBar={(props) => (
        <TabBar
          {...props}
          indicatorStyle={{ backgroundColor: "#23A9C6" }}
          renderIcon={(props) => getTabBarIcon(props)}
          tabStyle={styles.bubble}
          labelStyle={styles.noLabel}
        />
      )}
    />
  );
}
const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  noLabel: {
    display: "none",
    height: 50,
  },
  bubble: {
    backgroundColor: "#fff",
    paddingHorizontal: 18,
    paddingVertical: 10,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 0,
    height: 80,
    alignItems: "center",
    backgroundColor: "#fff",
    justifyContent:"center",
    paddingTop:20,
    borderBottomColor:"#fff",
  },
});