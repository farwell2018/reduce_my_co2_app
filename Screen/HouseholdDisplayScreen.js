import React, { useEffect, useState, useCallback } from "react";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Card, BottomSheet, Button, Icon } from "react-native-elements";
import { useNavigation, StackActions } from "@react-navigation/native";
import Car from "../Image/SVG/car_tab.svg";
import { List, DataTable } from "react-native-paper";
import Edit from "../Image/SVG/edit.svg";
import Toast from "react-native-toast-message";
import {
  Stack,
  Input,
  useToast,
  Select,
  Modal,
  CheckIcon,
  Center,
  HStack,
  VStack,
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableHighlight,
  SafeAreaView,
} from "react-native";


const HouseDisplayScreen = () => {
  const [profile, setProfile] = useState([]);
  const [profileComplete, setProfileComplete] = useState();
  const [loading, setLoading] = useState(true);
  const [vehicles, setVehicle] = useState([]);
  const [journeyVehicles, setJourneyVehicle] = useState([]);
  const [journey, setJourney] = useState([]);
  const [commute, setCommute] = useState(null);
  const [measurement, setMeasurement] = useState("");
  const [ownsVehicle, setOwnsVehicle] = useState();
  const navigation = useNavigation();
  const toast = useToast();
  const [isFirstLaunch, setIsFirstLaunch] = useState(null);
  const [houseId, setHouseId] = useState("");
  const [isVisible, setIsVisible] = useState(false);
  const [houses, setHouses] = useState([]);
  const [sizes, setSizes] = useState([]);
  const [lives_in, setLivesIn] = useState("");
  const [home_size, setHomeSize] = useState("");
  const [no_of_shared_people, setSharedPeople] = useState("");
  const [electricity_consumption, setElectricityConsumption] = useState("");
  const [water_consumption_amount, setWaterConsumption] = useState("");
  const [homeType, setHomeType] = useState("");
  const [size, setSize] = useState("");
  const [people, setPeople] = useState("");
  const [kilowatt, setKilowatt] = useState("");
  const [litres, setLitres] = useState("");
  const [country, setCountry] = useState("");

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      //  alert("Refreshed!");
      // getHouse();
      getData();
    });
    return unsubscribe;
  }, [navigation]);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          const user = responseJson.data;
          setCountry(user.country.name);
          
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/houses/details",
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson.data);
          if (responseJson.status === "success") {
            const items = responseJson.data;
            setHouses(items.houses);
            setSizes(items.sizes);
            setLoading(false);
          }
        });
    });
  }, []);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/houses/details/retrieve/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommute(responseJson.data);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  const getData = () => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/houses/details/retrieve/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommute(responseJson.data);
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  // console.log(commute)
  const getHouse = (houseId, livesIn, homeSize) => {
    setHouseId(houseId);
    setSize(homeSize);
    setHomeType(livesIn);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/houses/user/" +
        houseId,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setIsVisible(true);
            setLitres(JSON.stringify(user.water_consumption_amount));
            setKilowatt(user.electricity_consumption);
            setPeople(JSON.stringify(user.no_of_shared_people));
            // setFlightsNew(responseJson.data);
            // setIsVisible(true);
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  const submitHouseholdDetails = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      lives_in: lives_in,
      home_size: home_size,
      no_of_shared_people: no_of_shared_people,
      electricity_consumption: electricity_consumption,
      water_consumption_amount: water_consumption_amount,
    });
    // console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/houses/user/update/" +

        houseId,
        {
          method: "PUT",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            setLoading(false);
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
            calculateHomeEmission();
            setIsVisible(false);
            getData();
          } else {
            setLoading(false);
            Toast.show({
              type: "error",
              text1: responseJson.message,
              text2: "Please check the data and try again",
            });
          }
        });
    });
  };
  const calculateHomeEmission = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/houses/calculate/emission/" +
        user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
          } else {
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader
          // setLoading(false);
          console.error(error);
        });
    });
  };
  return (
    <ScrollView style={styles.itemsContainer}>
      {/* <TabBar/> */}
      <View style={styles.itemsContainer}>
        <View>
          <Text style={styles.loginheaderStyle}>Your Household Breakdown</Text>
        </View>
        <List.Section title="">
          <List.Accordion
            titleStyle={styles.accordion}
            title="My Household"
            left={(props) => <List.Icon icon="home" />}
          >
            {commute && commute.map((u, i) => {
              return (
                <List.Item
                  key={i}
                  title=""
                  left={(props) => (
                    <Card containerStyle={styles.card}>
                      {/* <View style={styles.cardStyle}>
                        <Text style={styles.name}>Residence Type:</Text>
                        <Text style={styles.userInfo}>{u.lives_in}</Text>
                      </View> */}
                      {/* <Card.Divider /> */}
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Occupants:</Text>
                        <Text style={styles.userInfo}>
                          {u.no_of_shared_people}
                        </Text>
                      </View>
                      <Card.Divider />
                      {/* <View style={styles.cardStyle}>
                        <Text style={styles.name}>Size:</Text>
                        <Text style={styles.userInfo}>{u.home_size}</Text>
                      </View> */}
                      {/* <Card.Divider /> */}
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>
                          Electricity Consumption:
                        </Text>
                        <Text style={styles.userInfo}>
                          {u.electricity_consumption + " "} {country == "Kenya" ? "Ksh" : "£"}
                        </Text>
                      </View>
                      <Card.Divider />
                      {/* <View style={styles.cardStyle}>
                        <Text style={styles.name}>Water Consumption:</Text>
                        <Text style={styles.userInfo}>
                          {u.water_consumption_amount + " Ltr"}
                        </Text>
                      </View> */}

                      <BottomSheet
                        isVisible={isVisible}
                      // containerStyle={styles.itemsContainer}
                      >
                        <Stack
                          space={2}
                          alignItems="center"
                          style={styles.editsContainer}
                        >
                          {/* <Select
                            defaultValue={homeType}
                            variant="underlined"
                            selectedValue={lives_in}
                            minWidth={350}
                            accessibilityLabel="Select your house type"
                            placeholder="I live in a e.g House"
                            onValueChange={(lives_in) => setLivesIn(lives_in)}
                            _selectedItem={{
                              bg: "teal.600",
                              endIcon: <CheckIcon size={4} />,
                            }}
                          >
                            {houses &&
                              houses.map((house, index) => (
                                <Select.Item
                                  key={house.id}
                                  label={house.type}
                                  value={house.type}
                                />
                              ))}
                          </Select> */}

                          <HStack space={2} alignItems="center">
                            {/* <Center>
                              <Select
                                defaultValue={size}
                                variant="underlined"
                                selectedValue={home_size}
                                minWidth={350}
                                accessibilityLabel="Select your favorite programming language"
                                placeholder="The size is e.g 4 bedrooms"
                                onValueChange={(home_size) =>
                                  setHomeSize(home_size)
                                }
                                _selectedItem={{
                                  bg: "teal.600",
                                  endIcon: <CheckIcon size={4} />,
                                }}
                              >
                                {sizes &&
                                  sizes.map((size, index) => (
                                    <Select.Item
                                      key={size.id}
                                      label={size.type}
                                      value={size.type}
                                    />
                                  ))}
                              </Select>
                            </Center> */}
                          </HStack>
                          <HStack space={2} alignItems="center">
                            <Center>
                              <Input
                                defaultValue={people}
                                keyboardType="number-pad"
                                variant="underlined"
                                // ref={peopleInputRef}
                                onChangeText={(no_of_shared_people) =>
                                  setSharedPeople(no_of_shared_people)
                                }
                                size="sm"
                                placeholder="I share my home with"
                                _light={{
                                  placeholderTextColor: "blueGray.400",
                                }}
                                _dark={{
                                  placeholderTextColor: "blueGray.50",
                                }}
                                minWidth={350}
                                InputRightElement={"People"}
                              />
                            </Center>
                          </HStack>
                          <HStack space={2} alignItems="center">
                            <Center>
                              <Input
                                defaultValue={kilowatt}
                                // ref={electricityInputRef}
                                keyboardType="number-pad"
                                variant="underlined"
                                onChangeText={(electricity_consumption) =>
                                  setElectricityConsumption(
                                    electricity_consumption
                                  )
                                }
                                size="sm"
                                placeholder="I consume"
                                _light={{
                                  placeholderTextColor: "blueGray.400",
                                }}
                                _dark={{
                                  placeholderTextColor: "blueGray.50",
                                }}
                                minWidth={350}
                                InputRightElement={country == "Kenya" ? "Ksh" : "£"}
                              />
                            </Center>
                          </HStack>
                          {/* <HStack space={2} alignItems="center">
                            <Center>
                              <Input
                                defaultValue={litres}
                                // ref={waterInputRef}
                                variant="underlined"
                                keyboardType="number-pad"
                                onChangeText={(water_consumption_amount) =>
                                  setWaterConsumption(water_consumption_amount)
                                }
                                size="sm"
                                placeholder="I consume"
                                _light={{
                                  placeholderTextColor: "blueGray.400",
                                }}
                                _dark={{
                                  placeholderTextColor: "blueGray.50",
                                }}
                                minWidth={350}
                                InputRightElement={" Ltr/Month"}
                              />
                            </Center>
                          </HStack> */}
                          <VStack space={1} alignItems="center">
                            <HStack space={2} alignItems="center">
                              <Center>
                                <TouchableOpacity
                                  style={styles.buttonCancelStyle}
                                  activeOpacity={0.5}
                                  onPress={() => setIsVisible(false)}
                                >
                                  <Text style={styles.buttonTextStyle}>
                                    Cancel
                                  </Text>
                                </TouchableOpacity>
                              </Center>
                              <Center>
                                <TouchableOpacity
                                  style={styles.buttonEditStyle}
                                  activeOpacity={0.5}
                                  onPress={submitHouseholdDetails}
                                >
                                  <Text style={styles.buttonTextStyle}>
                                    Save
                                  </Text>
                                </TouchableOpacity>
                              </Center>
                            </HStack>
                          </VStack>
                        </Stack>
                      </BottomSheet>
                      <TouchableOpacity
                        style={styles.addContent}
                        activeOpacity={0.2}
                        onPress={() => getHouse(u.id, u.lives_in, u.home_size)}
                      >
                        {/* <View style={styles.addContent}> */}
                        <Text style={{ color: "#707070" }}>Edit Home</Text>
                        <Edit style={styles.editIcon} />
                        {/* </View> */}
                      </TouchableOpacity>

                    </Card>
                  )}
                />
              );
            })}
          </List.Accordion>
        </List.Section>
        <VStack space={1} alignItems="center">
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={() => navigation.navigate("Home Details")}
          >
            <Text style={styles.buttonTextStyle}>Add Home</Text>
          </TouchableOpacity>
        </VStack>
      </View>
    </ScrollView>
  );
};

export default HouseDisplayScreen;
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 45,
    marginRight: 45,
    marginTop: 20,
    marginBottom: 25,
    width: 300,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    fontSize: 20,
    padding: 10,
    paddingLeft: 30,
    marginTop: 20,
    // fontWeight:"bold
    paddingTop: 0,
  },
  congratsHeaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    // paddingLeft: 30,
    paddingTop: 0,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 18,
    color: "#727272",
    fontWeight: "800",
    paddingRight: 20,
  },
  userInfo: {
    fontSize: 17,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    // marginTop: 10,
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 80,
    height: 50,
    // marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoUsage: {
    fontSize: 14,
    // marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    // flexDirection: "row",
    // paddingLeft: 30,
    // paddingRight: 30,
    width: 360,
  },
  vehicleSelect: {
    // width: 190,
  },
  fuelSelect: {
    // width: 190,
  },
  addContent: {
    // paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  editIcon: {
    width: 50,
    height: 40,
  },
  itemUsage: {
    // display: "flex",
    flexDirection: "row",
    marginTop: 10,
    // justifyContent: "space-evenly",
  },
  usageInputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  infoinnerUsage: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    // marginLeft: 50,
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
    // marginLeft: 50,
  },
  miniheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 15,
    padding: 10,
    // width:350,
    // paddingLeft: 30,
    // paddingTop: 20,
  },
  titleStyle: {
    color: "#23A9C6",
  },
  rightTitleStyle: {
    color: "#23A9C6",
    fontSize: 16,
    paddingTop: 5,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    // height: 700,
  },
  addCommute: {
    display: "flex",
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  user: {
    alignItems: "center",
    flexDirection: "row",
  },
  card: {
    width: "90%",
    minWidth: 300,
    // padding: 5,
  },
  cardStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
  },
  cellStyle: {
    minWidth: 70,
    padding: 2,
  },
  accordion: {
    color: "#23A9C6",
  },
  editsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    paddingTop: 20,
  },
  buttonEditStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 150,
    justifyContent: "center",
  },
  buttonCancelStyle: {
    backgroundColor: "#707070",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#707070",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 150,
    justifyContent: "center",
  },
});
