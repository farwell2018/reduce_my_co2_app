import React, { useState, createRef } from "react";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  KeyboardAvoidingView,
  Keyboard,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  TouchableHighlight,
} from "react-native";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import PhoneInput from "react-native-phone-number-input";
import PasswordInputText from "react-native-hide-show-password-input";

export const RegisterScreen = ({ navigation }) => {
  const [userFirstName, setUserFirstName] = useState("");
  const [userLastName, setUserLastName] = useState("");
  const [userPhoneNumber, setUserPhoneNumber] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [userConfirmPassword, setConfirmPassword] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [userVerification, setVerificationCode] = useState("");
  const [loading, setLoading] = useState(false);
  const [errortext, setErrortext] = useState("");
  const [isRegistrationSuccess, setIsRegistrationSuccess] = useState(false);
  const [data, setData] = useState([]);
  const [userData, setuserData] = useState([]);
  const [isVerificationSuccess, setIsVerifiedSuccess] = useState(false);

  const emailInputRef = createRef();
  const passwordInputRef = createRef();
  const firstNameInputRef = createRef();
  const lastNameInputRef = createRef();
  const PhoneNumberInputRef = createRef();

  fetch("https://reducemyco2.farwell-consultants.com/api/v1/countries/all", {
    method: "GET",
    headers: {
      //Header Defination
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
  })
    .then((response) => response.json())
    .then((responseJson) => {
      //Hide Loader

      console.log(responseJson.data.name);
      // If server response message same as Data Matched
    })
    .catch((error) => {
      //Hide Loader
      console.error(error);
    });

  const handleSubmitButton = () => {
    setErrortext("");
    if (!userFirstName) {
      alert("Please fill  First Name");
      return;
    }
    if (!userLastName) {
      alert("Please fill  Last Name");
      return;
    }
    if (!userEmail) {
      alert("Please fill Email");
      return;
    }
    if (!userPhoneNumber) {
      alert("Please fill Phone Number");
      return;
    }
    if (!userPassword) {
      alert("Please fill Password");
      return;
    }

    if (!userConfirmPassword) {
      alert("Please fill Confirm Password");
      return;
    }
    if (userPassword != userConfirmPassword) {
      alert("Confirm Password does not match Password");
      return;
    }
    //Show Loader
    setLoading(true);

    var formBody = JSON.stringify({
      first_name: userFirstName,
      last_name: userLastName,
      email: userEmail,
      password: userPassword,
      phone_number: userPhoneNumber,
    });
    console.log(formBody);
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/auth/signup", {
      method: "POST",
      body: JSON.stringify({
        first_name: userFirstName,
        last_name: userLastName,
        email: userEmail,
        password: userPassword,
        phone_number: userPhoneNumber,
      }),
      headers: {
        //Header Definition
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //Hide Loader
        setLoading(false);
        const userData = responseJson.data;
        setuserData(userData);
        console.log(responseJson + "=================");
        const info = responseJson.errors;
       
        setData(info);
        // If server response message same as Data Matched
        if (responseJson.status === "success") {
          AsyncStorage.multiSet([
            ["access_token", responseJson.access_token],
            ["user_id", JSON.stringify(responseJson.data.id)],
          ]).then((res) => {
            AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
              // console.log(value);
            });
          });
          // alert(responseJson.msg);
          console.log(responseJson);
          setIsRegistrationSuccess(true);
        } else {
          setErrortext(responseJson.message);
          console.log(responseJson);
          // alert(data);
        }
      })

      .catch((error) => {
        //Hide Loader
        setLoading(false);
        console.error(error);
      });
  };
  // console.log(userData);
  const handleVerification = () => {
    // if (isVerificationSuccess) {
    //   return <SuccessScreen />;
    // }

    setLoading(true);

    var formBody = JSON.stringify({
      verification_code: userVerification,
    });

    console.log(formBody);

    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
      console.log(value);
      fetch("https://reducemyco2.farwell-consultants.com/api/v1/auth/verify", {
        method: "POST",
        body: JSON.stringify({
          verification_code: userVerification,
        }),
        headers: {
          //Header Definition
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson.msg);

          setLoading(false);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            navigation.replace("DrawerNavigationRoutes");
          } else {
            setErrortext(responseJson.msg);
            // setIsVerifiedSuccess(true);
            //    console.log(isVerificationSuccess);
            // alert(responseJson.msg);
          }
        })

        .catch((error) => {
          //Hide Loader
          setLoading(false);
          console.error(error);
        });
    });
  };
  const resendCode = () => {
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;

      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/auth/resend/code",
        {
          method: "POST",
          // body: JSON.stringify({
          //   verification_code: userVerification,
          // }),
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);

          setLoading(false);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            // alert(responseJson.msg);
          } else {
            setErrortext(responseJson.msg);
            // setIsVerifiedSuccess(true);
            //    console.log(isVerificationSuccess);
            // alert(responseJson.msg);
          }
        })

        .catch((error) => {
          //Hide Loader
          setLoading(false);
          console.error(error);
        });
    });
  };
  if (isRegistrationSuccess) {
    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={{ flex: 1, padding: 16 }}>
          <View style={{ alignItems: "center" }}>
            <Image
              source={require("../Image/aboutreact.png")}
              style={{
                width: "100%",
                height: 150,
                resizeMode: "contain",
                margin: 5,
              }}
            />
          </View>
          <View style={styles.sectionCard}>
            <View>
              <Text style={styles.loginheaderStyle}>Enter Code</Text>
            </View>
            <View style={styles.SectionStyle}>
              <OTPInputView
                style={OtpStyle}
                pinCount={6}
                // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                onCodeChanged={(userVerification) =>
                  setVerificationCode(userVerification)
                }
                autoFocusOnLoad={false}
                codeInputFieldStyle={OtpStyle.underlineStyleBase}
                codeInputHighlightStyle={OtpStyle.underlineStyleHighLighted}
                onCodeFilled={(userVerification) =>
                  setVerificationCode(userVerification)
                } //Use function to check if code passes then redirect to successScreen
              />
            </View>
            <View>
              <Text style={styles.ValidateTextStyle}>
                Enter the 6 digit code sent via SMS to validate your account and
                conclude your registration
              </Text>
            </View>
            {errortext != "" ? (
              <Text style={styles.errorTextStyle}>{errortext}</Text>
            ) : null}
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleVerification}
            >
              <Text style={styles.buttonTextStyle}>Validate</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.buttonRegisterStyle}
              activeOpacity={0.5}
              onPress={resendCode}
            >
              <Text style={styles.buttonTextStyle}>Resend Code</Text>
            </TouchableOpacity>
          </View>
          <Text
            style={{
              fontSize: 18,
              textAlign: "center",
              color: "grey",
            }}
          ></Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: "center",
              color: "grey",
            }}
          ></Text>
        </View>
      </ScrollView>
    );
  }
   else if (isVerificationSuccess) {
     navigation.navigate("SuccessScreen");
    }
  else
    return (
      <View style={{ flex: 1, backgroundColor: "#FCFAFA" }}>
        <Loader loading={loading} />
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{
            justifyContent: "center",
            alignContent: "center",
          }}
        >
          <View style={{ alignItems: "center" }}>
            <Image
              source={require("../Image/aboutreact.png")}
              style={{
                width: "100%",
                height: 150,
                resizeMode: "contain",
                margin: 5,
              }}
            />
          </View>
          <KeyboardAvoidingView enabled>
            <View style={styles.sectionCard}>
              <View>
                <Text style={styles.loginheaderStyle}>Register</Text>
              </View>
              <View style={styles.SectionStyle}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(UserFirstName) =>
                    setUserFirstName(UserFirstName)
                  }
                  underlineColorAndroid="#f000"
                  placeholder="First Name"
                  placeholderTextColor="#8b9cb5"
                  autoCapitalize="sentences"
                  returnKeyType="next"
                  onSubmitEditing={() =>
                    firstNameInputRef.current &&
                    firstNameInputRef.current.focus()
                  }
                  blurOnSubmit={false}
                />
              </View>
              <View style={styles.SectionStyle}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(UserLastName) => setUserLastName(UserLastName)}
                  underlineColorAndroid="#f000"
                  placeholder="Last Name"
                  placeholderTextColor="#8b9cb5"
                  autoCapitalize="sentences"
                  returnKeyType="next"
                  onSubmitEditing={() =>
                    lastNameInputRef.current && lastNameInputRef.current.focus()
                  }
                  blurOnSubmit={false}
                />
              </View>
              <View style={styles.SectionStyle}>
                <TextInput
                  style={styles.inputStyle}
                  onChangeText={(UserEmail) => setUserEmail(UserEmail)}
                  underlineColorAndroid="#f000"
                  placeholder="Email Address"
                  placeholderTextColor="#8b9cb5"
                  keyboardType="email-address"
                  ref={emailInputRef}
                  returnKeyType="next"
                  onSubmitEditing={() =>
                    emailInputRef.current && emailInputRef.current.focus()
                  }
                  blurOnSubmit={false}
                />
              </View>

              <View style={styles.SectionStyle}>
                <PhoneInput
                  // style={styles.inputStyle}
                  onChangeFormattedText={(UserPhoneNumber) =>
                    setUserPhoneNumber(UserPhoneNumber)
                  }
                  underlineColorAndroid="#f000"
                  defaultCode="KE"
                  layout="first"
                  // withShadow
                  autoFocus
                  placeholder="Phone Number"
                  placeholderTextColor="#8b9cb5"
                  autoCapitalize="sentences"
                  ref={PhoneNumberInputRef}
                  returnKeyType="next"
                  onSubmitEditing={Keyboard.dismiss}
                  blurOnSubmit={false}
                  containerStyle={styles.phoneContainer}
                  textContainerStyle={styles.inputStyle}
                />
              </View>
              <View style={styles.PasswordSectionStyle}>
                <PasswordInputText
                  style={styles.PasswordinputStyle}
                  onChangeText={(UserPassword) => setUserPassword(UserPassword)}
                  tintColor="#f000"
                  keyboardType="default"
                  label="Set Password"
                  baseColor="#8b9cb5"
                  ref={passwordInputRef}
                  returnKeyType="next"
                  secureTextEntry={true}
                  onSubmitEditing={() =>
                    passwordInputRef.current && passwordInputRef.current.focus()
                  }
                  blurOnSubmit={false}
                  fontSize={15}
                  lineWidth={1}
                  iconColor="#8b9cb5"
                />
              </View>

              <View style={styles.PasswordSectionStyle}>
                <PasswordInputText
                  style={styles.PasswordinputStyle}
                  onChangeText={(ConfirmPassword) =>
                    setConfirmPassword(ConfirmPassword)
                  }
                  tintColor="#f000"
                  keyboardType="default"
                  label="Confirm Password"
                  baseColor="#8b9cb5"
                  ref={passwordInputRef}
                  returnKeyType="next"
                  secureTextEntry={true}
                  onSubmitEditing={() =>
                    passwordInputRef.current && passwordInputRef.current.focus()
                  }
                  blurOnSubmit={false}
                  fontSize={15}
                  lineWidth={1}
                  iconColor="#8b9cb5"
                />
              </View>

              {errortext != "" ? (
                <Text style={styles.errorTextStyle}>{errortext}</Text>
              ) : null}
              <TouchableHighlight
                style={styles.buttonStyle}
                underlayColor={"#23A9C6"}
                activeOpacity={1}
                onPress={handleSubmitButton}
              >
                <Text style={styles.buttonTextStyle}>Register</Text>
              </TouchableHighlight>
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
};
export default RegisterScreen;

const styles = StyleSheet.create({
  SectionStyle: {
    flexDirection: "row",
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  PasswordSectionStyle: {
    flexDirection: "row",
    // height: 40,
    // marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    // margin: 10,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  buttonStyle: {
    backgroundColor: "#7DE24E",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7DE24E",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 20,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    borderBottomColor: "#8b9cb5",
    borderColor: "#fff",
  },
  PasswordinputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  successTextStyle: {
    color: "#000",
    textAlign: "center",
    fontSize: 18,
    padding: 30,
  },

  sectionCard: {
    backgroundColor: "#ffffff",
    borderColor: "#000",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    margin: 10,
    borderRadius: 25,
    padding: 10,
    paddingTop: 20,
    paddingBottom: 20,
  },

  loginheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 0,
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  buttonRegisterStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  textInput: {
    paddingVertical: 0,
  },
  phoneContainer: {
    width: "100%",
    height: 55,
    backgroundColor: "#fff",
  },
});
const OtpStyle = StyleSheet.create({
  borderStyleBase: {
    width: 30,
    height: 45,
    color: "black",
  },

  borderStyleHighLighted: {
    borderColor: "#03DAC6",
  },

  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: "black",
  },

  underlineStyleHighLighted: {
    borderColor: "#03DAC6",
    color: "green",
  },
});
