import React, { useEffect, useState, useLayoutEffect } from "react";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Card, ListItem, Icon } from "react-native-elements";
import { VStack, CheckIcon, Avatar, Center, Modal, Select, Heading, FlatList, Text, Box, HStack, Spacer } from "native-base";
import * as rssParser from "react-native-rss-parser";
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Feed from "./Components/Feed";
import Conservation from "./Components/Conservation";
import Toast from "react-native-toast-message";
import {
  useNavigation,
  useFocusEffect,
  useIsFocused,
} from "@react-navigation/native";
import RNRestart from 'react-native-restart';
import axios from "axios";
import MultiSelect from 'react-native-multiple-select';
import {
  StyleSheet,
  View,

  ScrollView,
  Button,
  useWindowDimensions
} from "react-native";



const NewsFeed = () => {
  const axios = require('axios');
  const [newsfeed, setNewsfeed] = useState(false);
  const [userFeed, setUserFeed] = useState("");
  const [selectedItems, setSelectedItems] = useState([]);
  const isFocused = useIsFocused();
  const [profile, setProfile] = useState([]);
  const navigation = useNavigation();
  const [lastRefresh, setLastRefresh] = useState(Date(Date.now()).toString());
  useEffect(() => {
    getUser();
    getUserFeed();
  }, [isFocused]);
  // useEffect(() => {
  //   const unsubscribe = navigation.addListener("focus", () => {
  //     getUser();
  //   });
  //   return unsubscribe;
  // }, [navigation]);
  const getUser = () => {
    // setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
        user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setProfile(user);
            // setProfileComplete(user.profile_is_complete);
            setLoading(false);
            // setCountry(user.country.name);
          } else {
            // setLoading(false);
          }
        });
    });
  };
  const refreshScreen = () => {
    setLastRefresh(Date(Date.now()).toString());
  }

  const FirstRoute = () => (
    <Feed url="http://media-tor.herokuapp.com/api/bbc" />
  );

  const SecondRoute = () => (
    <Feed url="http://media-tor.herokuapp.com/api/conservation" />
  );
  // add third route
  //const ThirdRoute = () => (
  //  <Feed />
  //);
  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  // post multiple select items to the server
  const postSelectedItems = () => {
    // setLoading(true)
    console.log(selectedItems);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {

      let token = value[0][1];
      let user_id = value[1][1];
      const config = {
        headers: { Authorization: `Bearer ${token}` }
      };

      var formBody = JSON.stringify({
        newsfeed: selectedItems,
        user_id: user_id
      });
      console.log(formBody);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/profile/newsfeed/create",
        {
          method: "POST",
          body: formBody,
          headers: {
            //Header Defination
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            console.log(user)
            setLoading(false);
            setShowModal(false);
            navigation.navigate("NewsFeed");
            refreshScreen();
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });

          }
          else {
            setLoading(false);
          }
        });
      // .then(function (response) {
      //   console.log(response);
      //   setLoading(false);
      // })
      // .catch(function (error) {
      //   console.log(error);

      // });
    });
  };
  const getUserFeed = () => { 
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/newsfeed/user/" +
        user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            console.log(responseJson.data);
            const user = responseJson.data;
            console.log(user + "user");
            setUserFeed(user);
            setLoading(false);
          }
          else {
            setLoading(false);
            console.log("siko")
          }
        });
    });
  }
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/newsfeed/user/" +
        user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            console.log(responseJson.data);
            const user = responseJson.data;
            console.log(user + "user");
            setUserFeed(user);
            setLoading(false);
          }
          else {
            setLoading(false);
            console.log("siko")
          }
        });
    });
  }, []);
  const [showModal, setShowModal] = useState(false);
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  // console.log(userFeed)

  const feedArray = []
  //add dynamic routes for react-native-tab-view


  const [routes, setRoutes] = React.useState([]);
  useEffect(() => {
    console.log(userFeed)
    //look through userfeed if it contains bbc or conservation add to feedArray
    if (userFeed) {
      userFeed.map(item => {
        if (item.title === "BBC") {
          feedArray.push({ key: 'first', title: 'BBC' })
        }
        if (item.title === "Conservation") {
          feedArray.push({ key: 'second', title: 'Conservation International' })
        }
      })
    }
    setRoutes(feedArray)
  }, [userFeed])



  //create tabs dynamically for react-native-tab-view using route titles from userFeed.title array
  // const [routes, setRoutes] = React.useState([]);
  // useEffect(() => {
  //   setRoutes(() => {
  //     return userFeed.map((item, index) => {
  //       return { key: index, title: item.title }
  //     })
  //   });
  // }, [userFeed]);


  // const [routes] = React.useState([
  //   { key: 'first', title: 'BBC' },
  //   { key: 'second', title: 'Conservation' },
  // ]);
  const [feed, setFeed] = useState([]);
  const [loading, setLoading] = useState(false);

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        // <Button
        //   onPress={() => navigation.navigate("AddFeed")}
        //   title="Add News Feed"
        //   color="#23A9C6"
        // />
        <View style={styles.addCommute}>
          <Text style={{ color: "#fff" }}>Add Feed</Text>
          <Icon
            color="#23A9C6"
            containerStyle={{}}
            disabledStyle={{}}
            iconProps={{}}
            iconStyle={{}}
            name="add-circle-outline"
            onLongPress={() => console.log("onLongPress()")}
            onPress={() => navigation.navigate("AddFeed")}
            size={30}
            type="material"
          />
        </View>

      ),
    });
  }, [navigation]);

  useEffect(() => {
    async function getNews() {
      // setLoading(true)
      try {
        const response = await axios.get('https://reducemyco2.farwell-consultants.com/api/v1/newsfeed/all')
          .then((response) => response.data)
          .then((responseJson) => {
            // console.log(responseJson.data);
            if (responseJson.status === "success") {
              const feed = responseJson.data;
              setNewsfeed(responseJson.data);
              console.log(feed);
              setLoading(false)
              console.log(newsfeed + "newsfeed")
            }
            else {
              // setLoading(true)
            }
          })
      } catch (error) {
        console.error(error);
      }
    }

    getNews();
  }, [])
  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: 'blue' }}
      style={{ backgroundColor: '#707070' }}
    />
  );

  return (
    <>
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
        renderTabBar={renderTabBar}
      />

      <Center>
        {/* <Loader loading={loading} /> */}
        <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
          <Modal.Content maxWidth="400px">
            <Modal.CloseButton />
            <Modal.Header>Selected Feeds</Modal.Header>
            <Modal.Body>
              <Center>
                <Box>
                  {/* <Heading fontSize="xl"  pb="3">
                    Selected Feeds
                  </Heading> */}
                  {(userFeed && userFeed.map(
                    (item, index) => (
                      <Text fontSize="lg" key={index}>{item.title}</Text>
                    )))}
                  {newsfeed && (
                    <Select
                      minWidth={200}
                      variant="underlined"
                      accessibilityLabel="Select your country"
                      placeholder="Add News Feed"
                      onValueChange={(selectedItems) => setSelectedItems(selectedItems)}
                      _selectedItem={{
                        bg: "teal.600",
                        endIcon: <CheckIcon size={5} />,
                      }}
                      mt={1}
                    >
                      {newsfeed &&
                        newsfeed.map((news, index) => (
                          <Select.Item
                            key={index}
                            label={news.title}
                            value={news.id}
                          />
                        ))}
                    </Select>
                  )}
                </Box>
                <VStack space={3} width={"100%"}>


                </VStack>
              </Center>
            </Modal.Body>
            <Modal.Footer>

              <Button
                onPress={postSelectedItems}
                title="Submit"
                color="#7EC904"
              />


            </Modal.Footer>
          </Modal.Content>
        </Modal>
      </Center>
    </>
  );
};

export default NewsFeed;
const styles = StyleSheet.create({
  loginheaderStyle: {
    color: "#23A9C6",
    fontSize: 20,
    padding: 10,
    paddingLeft: 30,
    marginTop: 20,
    paddingTop: 0,
  },
  name: {
    fontSize: 13,
    color: "#ACA9A9",
    fontWeight: "100",
    // paddingRight: 20,
    // width: 120,
  },
  Title: {
    fontSize: 17,
    color: "#778899",
    fontWeight: "bold",
    width: 300,
    flex: 1,
  },
  description: {
    fontSize: 15,
    color: "#778899",
    fontWeight: "normal",
    width: 300,
    flex: 1,
  },
  itemsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    // height: 700,
  },
  card: {
    width: "90%",
    minWidth: 300,
    borderRadius: 15,
    elevation: 5,
  },
  cardStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
    // borderRadius: 10,
    padding: 6,
  },
  cellStyle: {
    minWidth: 70,
    padding: 2,
  },
  newsButton: {
    borderRadius: 20,
    backgroundColor: "#2EA9C4",
    width: "80%",
    height: 35,
  },
  addCommute: {
    display: "flex",
    paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    // marginTop: 20,
  },
});
