import React, { useEffect, useState, useCallback } from "react";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Card, BottomSheet, Button, Icon } from "react-native-elements";
import { useNavigation, StackActions } from "@react-navigation/native";
import Car from "../Image/SVG/car_tab.svg";
import { List, DataTable } from "react-native-paper";
import Edit from "../Image/SVG/edit.svg";
import Toast from "react-native-toast-message";
import {
  Stack,
  Input,
  useToast,
  Select,
  Modal,
  CheckIcon,
  Center,
  HStack,
  VStack,
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableHighlight,
  SafeAreaView,
} from "react-native";


const DietDisplayScreen = () => {
  const [loading, setLoading] = useState(true);
  const [commute, setCommute] = useState(null);
  const navigation = useNavigation();
  const toast = useToast();
  const [dietId, setDietId] = useState(null);
  const [isVisible, setIsVisible] = useState(false);
  const [diet, setUserDiet] = useState("");
  const [diet_preferences, setDiet] = useState([]);
  const [calories, setCalories] = useState("");
  const [caloriesAmt, setCaloriesAmt] = useState("");
  const [height, setUserHeight] = useState("");
  const [weight, setUserWeight] = useState("");
  const [knows_calories, setKnowsCalories] = useState([]);
  const [userCalories, setUserCalories] = useState("");
  const [userHeight, setHeight] = useState("");
  const [userWeight, setWeight] = useState("");
  const [prevDiet, setPrevDiet] = useState("");
  const [beef, setBeef] = useState(0);
  const [lamb, setLamb] = useState(0);
  const [prawns, setPrawns] = useState(0);
  const [fish, setFish] = useState(0);
  const [pork, setPork] = useState(0);
  const [chicken, setChicken] = useState(0);
  const [cheese, setCheese] = useState(0);
  const [milk, setMilk] = useState(0);
  const [eggs, setEggs] = useState(0);
  const [beans, setBeans] = useState(0);
  const [people, setPeople] = useState(0);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getData();
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/diet/details",
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson.data);
          if (responseJson.status === "success") {
            const items = responseJson.data;
            setDiet(items.diet_preferences);
            setKnowsCalories(items.knows_calories_i_eat);
            setLoading(false);
          }
        });
    });
  }, []);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/diet/details/retrieve/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommute(responseJson.data);
            // setProfileComplete(user.profile_is_complete);
            // setVehicle(user.vehicles);
            // setLoading(false);
            console.log(commute);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  const getData = () => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/diet/details/retrieve/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommute(responseJson.data);
            // setProfileComplete(user.profile_is_complete);
            // setVehicle(user.vehicles);
            // setLoading(false);
            // console.log(user[0]);
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  const getDiet = (dietId, dietPreference) => {
    setDietId(dietId);
    console.log(dietId);
    setPrevDiet(dietPreference);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/diet/user/" +
        dietId,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            console.log(user)
            setIsVisible(true);
            setUserCalories(user.calories)
            setHeight(user.height)
            setWeight(user.weight)
            setBeef(user.beef)
            setLamb(user.lamb)
            setPrawns(user.prawns)
            setFish(user.fish)
            setPork(user.pork)
            setChicken(user.chicken)
            setCheese(user.cheese)
            setMilk(user.milk)
            setEggs(user.eggs)
            setBeans(user.beans)
            setPeople(user.people)
            console.log(user.milk)
            // setLitres(JSON.stringify(user.water_consumption_amount));
            // setKilowatt(user.electricity_consumption);
            // setPeople(JSON.stringify(user.no_of_shared_people));
            // setFlightsNew(responseJson.data);
            // setIsVisible(true);
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  const submitDietDetails = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      user_diet_preference: diet,
      knows_calories_i_consume: calories,
      calories: caloriesAmt,
      weight: weight,
      height: height,
    });
    console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/diet/user/update/" +
        dietId,
        {
          method: "PUT",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          //Hide Loader
          // setLoading(true);
          // console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
            getData();
            setIsVisible(false);
            calculateDietEmission()
          } else {
            setLoading(false);
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "error",
              text1: responseJson.message,
              text2: responseJson.msg,
            });
            // setErrors(responseJson.message);
            // setErrorMessage(responseJson.errors);
          }
        });
    });
    // heightInputRef.current.clear();
    // caloriesInputRef.current.clear();
    // weightInputRef.current.clear();
    // setCalories("");
    // setUserDiet("");
  };
  const calculateDietEmission = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/diet/calculate/emission/" +
        user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
          } else {
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader
          // setLoading(false);
          console.error(error);
        });
    });
  };
  return (
    <ScrollView style={styles.itemsContainer}>
      {/* <TabBar/> */}
      <View style={styles.itemsContainer}>
        <View>
          <Text style={styles.loginheaderStyle}>Your Diet Breakdown</Text>
        </View>
        <List.Section title="">
          <List.Accordion
            titleStyle={styles.accordion}
            title="My Diet"
            left={() => <List.Icon icon="food" />}
          >
            {commute && commute.map((u, i) => {
              // console.log(u);
              return (
                <List.Item
                  key={i}
                  title=""
                  left={(props) => (
                    <Card containerStyle={styles.card} key={u.id}>
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Beef</Text>
                        <Text style={styles.userInfo}>{u.beef}</Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Pork:</Text>
                        <Text style={styles.userInfo}>{u.pork + " per Week"}</Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Prawns:</Text>
                        <Text style={styles.userInfo}>{u.prawns + " per Week"}</Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Milk:</Text>
                        <Text style={styles.userInfo}>
                          {u.milk + " per Week"}
                        </Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Lamb:</Text>
                        <Text style={styles.userInfo}>
                          {u.lamb + " per Week"}
                        </Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Chicken:</Text>
                        <Text style={styles.userInfo}>
                          {u.chicken + " per Week"}
                        </Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Eggs:</Text>
                        <Text style={styles.userInfo}>
                          {u.eggs + " per Week"}
                        </Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Fish:</Text>
                        <Text style={styles.userInfo}>
                          {u.fish + " per Week"}
                        </Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Cheese:</Text>
                        <Text style={styles.userInfo}>
                          {u.cheese + " per Week"}
                        </Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Beans:</Text>
                        <Text style={styles.userInfo}>
                          {u.beans + " per Week"}
                        </Text>
                      </View>
                      <BottomSheet
                        isVisible={isVisible}

                      >
                        <Stack
                          space={2}
                          alignItems="center"
                          style={styles.editsContainer}
                        >
                          <Select
                            defaultValue={prevDiet}
                            variant="underlined"
                            selectedValue={diet}
                            minWidth={350}
                            accessibilityLabel="Select your favorite programming language"
                            placeholder="I consider myself"
                            onValueChange={(diet) => setUserDiet(diet)}
                            _selectedItem={{
                              bg: "teal.600",
                              endIcon: <CheckIcon size={4} />,
                            }}
                          >
                            {diet_preferences &&
                              diet_preferences.map((diet, index) => (
                                <Select.Item
                                  key={diet.id}
                                  label={diet.type}
                                  value={diet.type}
                                />
                              ))}
                          </Select>

                          <HStack space={2} alignItems="center">
                            <Center>
                              <Select
                                //  defaultValue={"Yes"}
                                variant="underlined"
                                selectedValue={calories}
                                minWidth={350}
                                accessibilityLabel="Select your favorite programming language"
                                placeholder="I know how many calories I eat"
                                onValueChange={(calories) =>
                                  setCalories(calories)
                                }
                                _selectedItem={{
                                  bg: "teal.600",
                                  endIcon: <CheckIcon size={4} />,
                                }}
                              >
                                {knows_calories &&
                                  knows_calories.map((item, index) => (
                                    <Select.Item
                                      key={item.id}
                                      label={item.type}
                                      value={item.type}
                                    />
                                  ))}
                              </Select>
                            </Center>
                          </HStack>
                          <HStack space={2} alignItems="center">
                            <Center>
                              <Input
                                defaultValue={userCalories}
                                // ref={caloriesInputRef}
                                onChangeText={(caloriesAmt) =>
                                  setCaloriesAmt(caloriesAmt)
                                }
                                variant="underlined"
                                size="sm"
                                placeholder="I eat"
                                _light={{
                                  placeholderTextColor: "blueGray.400",
                                }}
                                _dark={{
                                  placeholderTextColor: "blueGray.50",
                                }}
                                minWidth={350}
                                InputRightElement={"Calories"}
                              />
                            </Center>
                          </HStack>
                          <HStack space={2} alignItems="center">
                            <Center>
                              <Input
                                defaultValue={userWeight}
                                // ref={weightInputRef}
                                onChangeText={(weight) => setUserWeight(weight)}
                                variant="underlined"
                                size="sm"
                                placeholder="I currently weigh"
                                _light={{
                                  placeholderTextColor: "blueGray.400",
                                }}
                                _dark={{
                                  placeholderTextColor: "blueGray.50",
                                }}
                                minWidth={350}
                                InputRightElement={"Kg"}
                              />
                            </Center>
                            {/* <Center>
                              <Text>kWh/Month</Text>
                            </Center> */}
                          </HStack>
                          <HStack space={2} alignItems="center">
                            <Center>
                              <Input
                                defaultValue={userHeight}
                                // ref={heightInputRef}
                                onChangeText={(height) => setUserHeight(height)}
                                variant="underlined"
                                size="sm"
                                placeholder="My height is"
                                _light={{
                                  placeholderTextColor: "blueGray.400",
                                }}
                                _dark={{
                                  placeholderTextColor: "blueGray.50",
                                }}
                                minWidth={350}
                                InputRightElement={"cm"}
                              />
                            </Center>
                            {/* <Center>
                              <Text>Ltr/Month</Text>
                            </Center> */}
                          </HStack>
                          <VStack space={1} alignItems="center">
                            <HStack space={2} alignItems="center">
                              <Center>
                                <TouchableOpacity
                                  style={styles.buttonCancelStyle}
                                  activeOpacity={0.5}
                                  onPress={() => setIsVisible(false)}
                                >
                                  <Text style={styles.buttonTextStyle}>
                                    Cancel
                                  </Text>
                                </TouchableOpacity>
                              </Center>
                              <Center>
                                <TouchableOpacity
                                  style={styles.buttonEditStyle}
                                  activeOpacity={0.5}
                                  onPress={submitDietDetails}
                                >
                                  <Text style={styles.buttonTextStyle}>
                                    Save
                                  </Text>
                                </TouchableOpacity>
                              </Center>
                            </HStack>
                          </VStack>
                        </Stack>
                      </BottomSheet>
                      <TouchableOpacity
                        style={styles.addContent}
                        activeOpacity={0.2}
                        onPress={() => getDiet(u.id, u.diet_preference)}
                      >

                        <Text style={{ color: "#707070" }}>Edit Diet</Text>
                        <Edit style={styles.editIcon} />

                      </TouchableOpacity>
                      {/* <View style={styles.addContent}>
                        <Text style={{ color: "#707070" }}>Edit</Text>
                        <Icon
                          color="#23A9C6"
                          containerStyle={{}}
                          disabledStyle={{}}
                          iconProps={{}}
                          iconStyle={{}}
                          name="add-circle-outline"
                          onLongPress={() => console.log("onLongPress()")}
                          onPress={() => getDiet(u.id)}
                          size={40}
                          type="material"
                        />
                      </View> */}
                    </Card>
                  )}
                />
              );
            })}
          </List.Accordion>
        </List.Section>
        <VStack space={1} alignItems="center">
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={() => navigation.navigate("Diet Details")}
          >
            <Text style={styles.buttonTextStyle}>Add Diet</Text>
          </TouchableOpacity>
        </VStack>
      </View>
    </ScrollView>
  );
};

export default DietDisplayScreen;
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 45,
    marginRight: 45,
    marginTop: 20,
    marginBottom: 25,
    width: 300,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    fontSize: 20,
    padding: 10,
    paddingLeft: 30,
    marginTop: 20,
    // fontWeight:"bold
    paddingTop: 0,
  },
  congratsHeaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    // paddingLeft: 30,
    paddingTop: 0,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 18,
    color: "#727272",
    fontWeight: "800",
    paddingRight: 20,
  },
  userInfo: {
    fontSize: 17,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    // marginTop: 10,
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 80,
    height: 50,
    // marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoUsage: {
    fontSize: 14,
    // marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    // flexDirection: "row",
    // paddingLeft: 30,
    // paddingRight: 30,
    width: 360,
  },
  vehicleSelect: {
    // width: 190,
  },
  fuelSelect: {
    // width: 190,
  },
  addContent: {
    // paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  editIcon: {
    width: 50,
    height: 40,
  },
  itemUsage: {
    // display: "flex",
    flexDirection: "row",
    marginTop: 10,
    // justifyContent: "space-evenly",
  },
  usageInputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  infoinnerUsage: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    // marginLeft: 50,
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
    // marginLeft: 50,
  },
  miniheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 15,
    padding: 10,
    // width:350,
    // paddingLeft: 30,
    // paddingTop: 20,
  },
  titleStyle: {
    color: "#23A9C6",
  },
  rightTitleStyle: {
    color: "#23A9C6",
    fontSize: 16,
    paddingTop: 5,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    // height: 700,
  },
  addCommute: {
    display: "flex",
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  user: {
    alignItems: "center",
    flexDirection: "row",
  },
  card: {
    width: "90%",
    minWidth: 300,
    // padding: 5,
  },
  cardStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
  },
  cellStyle: {
    minWidth: 70,
    padding: 2,
  },
  accordion: {
    color: "#23A9C6",
  },
  editsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    paddingTop: 20,
  },
  buttonEditStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 150,
    justifyContent: "center",
  },
  buttonCancelStyle: {
    backgroundColor: "#707070",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#707070",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 150,
    justifyContent: "center",
  },
});
