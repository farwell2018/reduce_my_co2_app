import React from "react";
import { styles } from "../Styles/Styles";
import { VStack, Center, Heading, HStack } from "native-base";
import { Icon } from "react-native-elements";
import {
  useNavigation,
} from "@react-navigation/native";
import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableHighlight,
} from "react-native";
const Target = () => {
  let iconColor = "#23A9C6";
  const navigation = useNavigation();
  return (
    <ScrollView style={styles.itemsContainer}>
      <View style={styles.reduceCard}>
        <View style={styles.homeHeaderStyle}>
          <Text style={styles.loginheaderStyle}>Coming Soon</Text>
          <View style={styles.targetContainer}>
            <Image
              source={require("../Image/coming.png")}
              style={styles.hintsIcon}
            />
          </View>
          <View style={styles.buttonContainer}>
            <TouchableHighlight
              style={styles.offsetButtonStyle}
              underlayColor={"#7EC904"}
              activeOpacity={1}
              onPress={() => navigation.navigate("HomeScreen")}
            >
              <Text style={styles.buttonTextStyle}>Back</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Target;
