import React from "react";
import { Icon } from "react-native-elements";
import { Button } from "react-native";
// Import Navigators from React Navigation
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
// Import Screens
import HomeScreen from "./DrawerScreens/HomeScreen";
import SettingsScreen from "./DrawerScreens/SettingsScreen";
import ProfileScreen from "./DrawerScreens/ProfileScreen";
import CustomSidebarMenu from "./Components/CustomSidebarMenu";
import NavigationDrawerHeader from "./Components/NavigationDrawerHeader";
import EcoProfileScreen from "./DrawerScreens/EcoProfile";
import EcoTabView from "./DrawerScreens/EcoProfileMain";
import ProfileView from "./Components/ProfileView";
import SuccessScreen from "./SuccessScreen";
import VehicleDetails from "./Components/VehicleDetails";
import CongratsScreen from "./Components/Congratulations";
import FlightScreen from "./FlightScreen";
import EcoProfile from "./DrawerScreens/EcoProfile";
import DietScreen from "./DietScreen";
import HouseholdScreen from "./HouseholdScreen";
import EcoDisplayScreen from "./EcoDisplayScreen";
import FlightDisplayScreen from "./FlightDisplayScreen";
import HouseDisplayScreen from "./HouseholdDisplayScreen";
import DietDisplayScreen from "./DietDisplayScreen";
import NewsFeed from "./NewsFeedScreen.";
import JourneyDetails from "./Components/JourneyScreen";
import Offset from "./Components/Offset";
import PaymentSubmission from "./Components/PaymentSubmission";
import Target from "./TargetScreen";
import AddFeed from "./Components/addFeed";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createMaterialTopTabNavigator();

const homeScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="HomeScreen">
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          title: "Home", //Set Header Title
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Eco Tab"
        component={EcoTabView}
        options={{
          title: "Hints", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Offset"
        component={Offset}
        options={{
          title: "Offset", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />

      <Stack.Screen
        name="PaymentSubmission"
        component={PaymentSubmission}
        options={{
          title: "Donations", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />


      <Stack.Screen
        name="Target"
        component={Target}
        options={{
          title: "My Targets", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Edit Profile"
        component={ProfileScreen}
        options={{
          title: "Edit Profile", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="My Profile"
        component={ProfileView}
        options={{
          title: "My Profile",
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="NewsFeed"
        component={NewsFeed}
        options={{
          title: "NewsFeed", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
   
        }}
      />
      <Stack.Screen
        name="AddFeed"
        component={AddFeed}
        options={{
          title: "Add Feed", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },

        }}
      />
      <Stack.Screen
        name="SuccessScreen"
        component={SuccessScreen}
        options={{
          title: "", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Eco Profile"
        component={EcoProfileTabs}
        options={{
          title: "Eco Profile",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
          headerRight: () => (
            <Button
              onPress={() => navigation.navigate("HomeScreen")}
              title="Home"
              color="#7EC904"
            />
          ),
        }}
      />
      <Stack.Screen
        name="Vehicle Details"
        component={VehicleDetails}
        options={{
          title: "Vehicle Details", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Journey Details"
        component={JourneyDetails}
        options={{
          title: "Journey Details", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="FlightScreen"
        component={FlightScreen}
        options={{
          title: "Flight Details",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          }, //Set Header Title
        }}
      />
      <Stack.Screen
        name="Home Details"
        component={HouseholdScreen}
        options={{
          title: "Home Details", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Diet Details"
        component={DietScreen}
        options={{
          title: "Diet Details", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Congrats"
        component={CongratsScreen}
        options={{
          title: "Congrats", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Eco Home"
        component={EcoProfileScreen}
        options={{
          title: "Vehicle Details",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          }, //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
};

// const settingScreenStack = ({navigation}) => {
//   return (
//     <Stack.Navigator
//       initialRouteName="SettingsScreen"
//       screenOptions={{
//         headerLeft: () => (
//           <NavigationDrawerHeader navigationProps={navigation} />
//         ),
//         headerStyle: {
//           backgroundColor: '#7EC904',
//         },
//         headerTintColor: '#fff',
//         headerTitleStyle: {
//           fontWeight: 'bold',
//         },
//       }}>
//       <Stack.Screen
//         name="SettingsScreen"
//         component={SettingsScreen}
//         options={{
//           title: 'Settings',
//         }}
//       />
//     </Stack.Navigator>
//   );
// };

const profileScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="My Profile">
      <Stack.Screen
        name="My Profile"
        component={ProfileView}
        options={{
          title: "My Profile",
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />

      {/* name="My Profile"
        component={ProfileView}
        options={{
          title: "My Profile", 
        }}
      /> */}
      <Stack.Screen
        name="Edit Profile"
        component={ProfileScreen}
        options={{
          title: "Edit Profile",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      {/* <Stack.Screen
        name="Eco Tab"
        component={EcoTabView}
        options={{
          title: "Hints",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      /> */}
      {/* <Stack.Screen
        name="Eco Tab"
        component={EcoTabView}
        options={{
          title: "Eco Profile",
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      /> */}
      <Stack.Screen
        name="Vehicle Details"
        component={VehicleDetails}
        options={{
          title: "Vehicle Details",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
};
const ecoProfileScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="Eco Profile Page">
      {/* <Stack.Screen
        name="Eco Tab"
        component={EcoTabView}
        options={{
          title: "Hints",
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      /> */}
      {/* <Stack.Screen
        name="Eco Profile"
        component={EcoTabView}
        options={{
          title: "Eco Profile",
        }}
      /> */}
      <Stack.Screen
        name="Vehicle Summary"
        component={EcoDisplayScreen}
        options={{
          title: "Vehicle Summary", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Congrats"
        component={CongratsScreen}
        options={{
          title: "Congrats", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="Eco Home"
        component={EcoProfileScreen}
        options={{
          title: "Eco Profile Screen",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          }, //Set Header Title
        }}
      />
      <Stack.Screen
        name="Vehicle Details"
        component={VehicleDetails}
        options={{
          title: "Vehicle Details",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          }, //Set Header Title
        }}
      />
      <Stack.Screen
        name="Journey Details"
        component={JourneyDetails}
        options={{
          title: "Journey Details", //Set Header Title
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
      <Stack.Screen
        name="FlightScreen"
        component={FlightScreen}
        options={{
          title: "FlightScreen",
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          }, //Set Header Title
        }}
      />
      <Stack.Screen
        name="My Profile"
        component={ProfileView}
        options={{
          title: "My Profile",
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          },
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
};
const EcoProfileTabs = () => {
  return (
    <Tab.Navigator
      initialRouteName="Vehicle"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          let iconColor = focused ? "#23A9C6" : "#c2c3c8";
          if (route.name === "Vehicle") {
            iconName = focused ? "directions-car" : "directions-car";
          } else if (route.name === "Flight") {
            iconName = focused ? "flight" : "flight";
          } else if (route.name === "Diet") {
            iconName = focused ? "restaurant" : "restaurant";
          } else if (route.name === "Home") {
            iconName = focused ? "house" : "house";
          }

          return <Icon name={iconName} size={18} color={iconColor} raised />;
        },
      })}
      tabBarOptions={{
        showIcon: true,
        showLabel: false,
        activeTintColor: "#23A9C6",
        inactiveTintColor: "gray",
        iconStyle: {
          width: "auto",
          height: 50,
        },
      }}
    >
      <Tab.Screen name="Vehicle" component={EcoDisplayScreen} />
      <Tab.Screen name="Flight" component={FlightDisplayScreen} />
      <Tab.Screen name="Diet" component={DietDisplayScreen} />
      <Tab.Screen name="Home" component={HouseDisplayScreen} />
    </Tab.Navigator>
  );
};
const DrawerNavigatorRoutes = (props) => {
  return (
    <Drawer.Navigator
      drawerContentOptions={{
        activeTintColor: "#cee1f2",
        color: "#ffffff",
        itemStyle: { marginVertical: 5, color: "white", fontWeight: "bold" },
        labelStyle: {
          color: "#ffffff",
        },
      }}
      screenOptions={{ headerShown: false }}
      drawerContent={CustomSidebarMenu}
    >
      <Drawer.Screen
        name="homeScreenStack"
        options={{ drawerLabel: "Home" }}
        component={homeScreenStack}
      />
      <Drawer.Screen
        name="profileScreenStack"
        options={{ drawerLabel: "My Profile" }}
        component={profileScreenStack}
      />
      <Drawer.Screen
        name="Eco Profile"
        options={{
          drawerLabel: "Eco Profile",
          headerShown: true,
          headerStyle: {
            backgroundColor: "#7EC904", //Set Header color
          }, //Set Header Title
          headerTintColor: "#fff", //Set Header text color
          headerTitleStyle: {
            fontWeight: "bold", //Set Header text style
          },
        }}
        component={EcoProfileTabs}
      />
      {/* <Drawer.Screen
        name="profileEditStack"
        options={{ drawerLabel: "Edit Profile" }}
        component={profileEditStack}
      /> */}
      {/* <Drawer.Screen
        name="settingScreenStack"
        options={{ drawerLabel: "Settings" }}
        component={settingScreenStack}
      /> */}
    </Drawer.Navigator>
  );
};

export default DrawerNavigatorRoutes;
