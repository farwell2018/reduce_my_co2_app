import React, { useEffect, useState, createRef } from "react";
import { Icon } from "react-native-elements";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CheckerComponent from "./Components/CheckerComponent";
import { useNavigation, StackActions } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import {
  Stack,
  Input,
  useToast,
  Select,
  Box,
  CheckIcon,
  Center,
  HStack,
  Modal,
  Button,
  FormControl
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Pressable,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableHighlight,
} from "react-native";

const FlightScreen = () => {
  const [classes, setClasses] = useState([]);
  const [flights, setFlights] = useState([]);
  const [loading, setLoading] = useState(true);
  const [addFlights, setAddFlights] = useState(false);
  const [doesFly, setDoesFly] = useState(true);
  const [flightType, setFlightType] = useState("");
  const [flightClass, setFlightClass] = useState("");
  const [roundDistance, setRoundDistance] = useState("");
  const [duration, setDuration] = useState("");
  const [hours, setHours] = useState("");
  const [minutes, setMinutes] = useState("");
  const [destination, setDestination] = useState("");
  const [purpose, setPurpose] = useState("");
  const [oneWayRound, setOneWayRound] = useState("");
  const [passengers, setPassengers] = useState("");
  const distanceInputRef = createRef();
  const passengerInputRef = createRef();
  const destinationInputRef = createRef();
  const hourInputRef = createRef();
  const minuteInputRef = createRef();
  const [errorMessage, setErrorMessage] = useState("");
  const [error, setErrors] = useState("");
  const toast = useToast();
  const navigation = useNavigation();
  const [isFirstLaunch, setIsFirstLaunch] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [aircraft, setAircraft] = useState("");
  const [commercial, setShowCommercial] = useState(false);
  const [chartered, setShowChartered] = useState(false);
  const [engine, setEngine] = useState("");
  const [commercialFlights, setCommercialFlights] = useState([]);
  const [chateredFlight, setChateredFlight] = useState([]);
  // if flightType === "Commercial" then show commercial flights else if flightType === "Chartered" then show chartered flights
  useEffect(() => {
    if(flightType === "Commercial"){
      setShowCommercial(true);
      setShowChartered(false);
    }
    else if (flightType === "Chartered") {
      setShowCommercial(false);
      setShowChartered(true);
    }
  }, [flightType]);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/flights/private")
      .then((response) => response.json())
      .then((data) => {
        if (data.status === "success") {
          setChateredFlight(data.data);
        } else {
          setLoading(true);
        }
      }
      ).catch((error) => {
        console.log(error);
      }
      );
  }, []);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/flights/commercial")
      .then((response) => response.json())
      .then((data) => {
        if (data.status === "success") {
          setCommercialFlights(data.data);
        } else {
          setLoading(true);
        }
      }
      ).catch((error) => {
        console.log(error);
      }
      );
  }, []);
  // calculate duration of the flight based on hours and minutes input
  useEffect(() => {
    if (hours !== "" && minutes !== "") {
      setDuration(
        parseInt(hours) + parseInt(minutes)/60 
      );
    }
  }, [hours, minutes]);
  console.log(duration);
  // console.log(commercialFlights)
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/classes/all",
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const items = responseJson.data;
            // console.log(responseJson.data);
            setClasses(items);
            setLoading(false);
          }
        });
    });
  }, []);
  // console.log(classes);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch("https://reducemyco2.farwell-consultants.com/api/v1/flights/all", {
        method: "GET",
        headers: {
          //Header Definition
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const items = responseJson.data;
            // console.log(responseJson.data);
            setFlights(items);
            setLoading(false);
          } else {
            setLoading(false);
          }
        });
    });
  }, []);
  // console.log(flights);
  const flightButton = () => {
    setIsFirstLaunch(isFirstLaunch);
    setDoesFly(false);
    setAddFlights(true);
  };
  const saveFlightButton = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      flight_type: flightType,
      flight_class: flightClass,
      duration: duration,
      one_way_or_round: oneWayRound,
      number_of_passengers: passengers,
      model: aircraft,
      engine_type: engine,
    });
    console.log(formBody);
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/create/usage",
        {
          method: "POST",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            setLoading(false);
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
            // setModalVisible(true)
          } else {
            setLoading(false);
            Toast.show({
              type: "error",
              text1: responseJson.message,
              text2: "Please check the data and try again",
            });
            setErrors(responseJson.message);
            setErrorMessage(responseJson.errors);
          }
        });
    });
  };
  const submitFlightButton = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      flight_type: flightType,
      flight_class: flightClass,
      duration: duration,
      one_way_or_round: oneWayRound,
      number_of_passengers: passengers,
      model: aircraft,
      engine_type: engine,
    });
    console.log(formBody);
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/create/usage",
        {
          method: "POST",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            setLoading(false);
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
            // setModalVisible(true)
            calculateButton();
          } else {
            console.log(responseJson.message);
            setLoading(false);
            Toast.show({
              type: "error",
              text1: responseJson.message,
              text2: "Please check the data and try again",
            });
            setErrors(responseJson.message);
            setErrorMessage(responseJson.errors);
          }
        });
    });
  };
  const addFlight = () => {
    saveFlightButton();
    distanceInputRef.current.clear();
    passengerInputRef.current.clear();
    setFlightClass("");
    setFlightType("");
    setOneWayRound("");
    destinationInputRef.current.clear();
    setPurpose("");
  };
  const calculateFlightEmission = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/calculate/emission/" +
        user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
          } else {
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader
          // setLoading(false);
          console.error(error);
        });
    });
  };
  const calculateButton = () => {
    calculateFlightEmission();
    navigation.navigate("Eco Profile", { screen: "Diet" });
  };
  useEffect(() => {
    CheckerComponent().then((isFirstLaunch) => {
      setIsFirstLaunch(isFirstLaunch);
    });
  }, []);

  if (isFirstLaunch === null) return null;

  return isFirstLaunch ? (
    <View>
      <View>
        <Text style={styles.miniheaderStyle}>If you don’t fly at all,</Text>
      </View>
      <View>
        <Text style={styles.loginheaderStyle}>Congratulations!</Text>
      </View>
      <View style={styles.item}>
      </View>
      <View style={styles.addContent}>
        <Text style={styles.miniheaderStyle}>
          Please continue with {"\n"} the rest of your CO
          <Text style={{ fontSize: 10, lineHeight: 37 }}>2</Text> footprint
          profile.
        </Text>
        <Icon
          color="#23A9C6"
          containerStyle={{}}
          disabledStyle={{}}
          iconProps={{}}
          iconStyle={{}}
          name="arrow-forward-circle-outline"
          onLongPress={() => console.log("onLongPress()")}
          onPress={() => navigation.navigate("Eco Profile", { screen: "Diet" })}
          size={40}
          type="ionicon"
        />
      </View>
      <View style={styles.buttonContainer}>
        <TouchableHighlight
          style={styles.buttonStyle}
          underlayColor={"#7EC904"}
          activeOpacity={1}
          onPress={flightButton}
        >
          <Text style={styles.buttonTextStyle}>Add Flight</Text>
        </TouchableHighlight>
      </View>
    </View>
  ) : (
    <View>
      <Loader loading={loading} />
      <Modal
        isOpen={modalVisible}
        onClose={setModalVisible}
        size="lg"
        style={styles.centeredView}
      >
        <Modal.Content
          maxH="350"
          style={{ justifyContent: "center", alignItems: "center" }}
        >
          {/* <Modal.CloseButton /> */}
          <Modal.Header>Eco Profile Update Successful</Modal.Header>
          <Modal.Body>
            <Center>
              <Image
                source={require("../Image/aboutreact.png")}
                style={{
                  width: "100%",
                  height: 150,
                  resizeMode: "contain",
                  margin: 5,
                }}
              />
              <Text>Press the button to calculate total flight Emission.</Text>
            </Center>
            <ScrollView></ScrollView>
          </Modal.Body>
          <Modal.Footer>
            <Button.Group space={2}>
              {/* <Button
                variant="ghost"
                colorScheme="blueGray"
                onPress={() =>
                  navigation.navigate("Eco Profile", { screen: "Diet" })
                }
              >
                Continue
              </Button> */}
              <Button onPress={calculateButton}>Calculate</Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
      <View style={styles.addFlight}>
        <Text style={styles.miniheaderStyle}>Flights in the last month</Text>
      </View>
      <Stack space={2} alignItems="center">
        <HStack space={2} alignItems="center">
          <FormControl maxW="350px">
            <FormControl.Label>How long was the flight?</FormControl.Label>
            <HStack space={2} alignItems="center">
              <Input
                size="sm"
                ref={hourInputRef}
                onChangeText={(hours) => setHours(hours)}
                keyboardType="number-pad"
                variant="underlined"
                placeholder="Hours"
                minW="175px" />
                <Input
                  size="sm"
                  keyboardType="number-pad"
                  ref={minuteInputRef}
                  onChangeText={(minutes) => setMinutes(minutes)}
                  variant="underlined"
                  placeholder="Minutes"
                  minW="175px" />
            </HStack>
          </FormControl>
        </HStack>
        <Select
          variant="underlined"
          selectedValue={flightType}
          minWidth={350}
          fontSize={14}
          accessibilityLabel="Select your flight"
          placeholder="Type of flight"
          onValueChange={(flightType) => setFlightType(flightType)}
          _selectedItem={{
            bg: "teal.600",
            endIcon: <CheckIcon size={4} />,
          }}
        >
          {/* {flights &&
            flights.map((flight, index) => (
              <Select.Item
                key={flight.id}
                label={flight.flight_type}
                value={flight.flight_type}
              />
            ))} */}
            <Select.Item label="Commercial Flight" value="Commercial" />
            <Select.Item label="Chartered Flight" value="Chartered" />
          </Select>
          {commercial && (
            <HStack space={2} alignItems="center">
              <Center>
                <Select
                  variant="underlined"
                  selectedValue={aircraft}
                  minWidth={350}
                  fontSize={14}
                  accessibilityLabel="Select plane make"
                  placeholder="Select plane make (Commercial)"
                  onValueChange={(aircraft) => setAircraft(aircraft)}
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={4} />,
                  }}
                >
                  {commercialFlights &&
                    commercialFlights.map((commercialFlights, index) => (
                      <Select.Item
                        key={commercialFlights.id}
                        label={commercialFlights.aircraft}
                        value={commercialFlights.aircraft}
                      />
                    ))}
                </Select>
              </Center>
            </HStack>
          )
            }
          {chartered && (
            <Center>
              <Select
                variant="underlined"
                selectedValue={aircraft}
                minWidth={350}
                fontSize={14}
                accessibilityLabel="Select plane make"
                placeholder="Select plane make (Charter)"
                onValueChange={(aircraft) => setAircraft(aircraft)}
                _selectedItem={{
                  bg: "teal.600",
                  endIcon: <CheckIcon size={4} />,
                }}
              >
                {chateredFlight &&
                  chateredFlight.map((chateredFlight, index) => (
                    <Select.Item
                      key={chateredFlight.id}
                      label={chateredFlight.aircraft}
                      value={chateredFlight.aircraft}
                    />
                  ))}
              </Select>
            </Center>
)}

        <HStack space={2} alignItems="center">
          <Center>
            {/* <Input
              keyboardType="number-pad"
              variant="underlined"
              size="sm"
              placeholder="Km"
              ref={distanceInputRef}
              onChangeText={(distance) => setDistance(distance)}
              minWidth={150}
            /> */}
            <Select
              variant="underlined"
                selectedValue={engine}
              minWidth={150}
              fontSize={14}
              accessibilityLabel="Engine Type"
              placeholder="Engine Type"
              onValueChange={(engine) => setEngine(engine)}
              _selectedItem={{
                bg: "teal.600",
                endIcon: <CheckIcon size={4} />,
              }}
            >
                <Select.Item label="Jet" value="Jet" />
                <Select.Item label="Propeller" value="Propeller" />
            </Select>
          </Center>
          <Center>
            <Select
              variant="underlined"
              selectedValue={oneWayRound}
              minWidth={200}
              fontSize={14}
              accessibilityLabel="Ticket Type"
              placeholder="Ticket Type"
              onValueChange={(oneWayRound) => setOneWayRound(oneWayRound)}
              _selectedItem={{
                bg: "teal.600",
                endIcon: <CheckIcon size={4} />,
              }}
            >
              <Select.Item label="One way" value={0} />
              <Select.Item label="Return" value={1} />
            </Select>
          </Center>
        </HStack>
        <HStack space={2} alignItems="center">
          <Center>
            <Select
              variant="underlined"
              selectedValue={flightClass}
              minWidth={200}
              fontSize={14}
              accessibilityLabel="Select your flight class"
              placeholder="Class"
              onValueChange={(flightClass) => setFlightClass(flightClass)}
              _selectedItem={{
                bg: "teal.600",
                endIcon: <CheckIcon size={4} />,
              }}
            >
              {classes &&
                classes.map((classes, index) => (
                  <Select.Item
                    key={classes.id}
                    label={classes.flight_class_name}
                    value={classes.flight_class_name}
                  />
                ))}
            </Select>
          </Center>
          <Center>
            <Input
              keyboardType="number-pad"
              variant="underlined"
              size="sm"
              placeholder="No of passengers"
              ref={passengerInputRef}
              onChangeText={(passengers) => setPassengers(passengers)}
              minWidth={150}
            />
          </Center>
        </HStack>
      </Stack>
      <View>
        <View style={styles.addCommute}>
          <Text style={{ color: "#707070" }}>Add Another</Text>
          <Icon
            color="#23A9C6"
            containerStyle={{}}
            disabledStyle={{}}
            iconProps={{}}
            iconStyle={{}}
            name="add-circle-outline"
            onLongPress={() => console.log("onLongPress()")}
            onPress={addFlight}
            size={40}
            type="material"
          />
        </View>
        {/* {error != "" ? (
                <Text style={styles.errorTextStyle}>{error}</Text>
              ) : null} */}
        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.buttonStyle}
            underlayColor={"#7EC904"}
            activeOpacity={1}
            onPress={submitFlightButton}
          >
            <Text style={styles.buttonTextStyle}>Save</Text>
          </TouchableHighlight>
        </View>
      </View>
    </View>
  );
};

export default FlightScreen;
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 45,
    marginRight: 45,
    marginTop: 20,
    marginBottom: 25,
    width: 300,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    // borderColor: "#000",
    // shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // elevation: 5,
    // margin: 10,
    // borderRadius: 25,
    // padding: 10,
    // paddingTop: 20,
    // paddingBottom: 20,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    // paddingLeft: 30,
    paddingTop: 0,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#727272",
    fontWeight: "bold",
  },
  userInfo: {
    fontSize: 12,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    // marginTop: 10,
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoUsage: {
    fontSize: 14,
    // marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    // flexDirection: "row",
    // paddingLeft: 30,
    // paddingRight: 30,
    width: 360,
  },
  vehicleSelect: {
    // width: 190,
  },
  fuelSelect: {
    // width: 190,
  },
  addContent: {
    paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
  itemUsage: {
    // display: "flex",
    flexDirection: "row",
    marginTop: 10,
    // justifyContent: "space-evenly",
  },
  usageInputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  infoinnerUsage: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    // marginLeft: 50,
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
    // marginLeft: 50,
  },
  miniheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 18,
    padding: 10,
    // width:350,
    // paddingLeft: 30,
    // paddingTop: 20,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
  },
  addCommute: {
    display: "flex",
    paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  addFlight: {
    display: "flex",
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 20,
  },
  //Modal Styles
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
});
