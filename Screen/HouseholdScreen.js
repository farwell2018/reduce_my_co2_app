import React, { useEffect, useState, createRef } from "react";
import { Icon } from "react-native-elements";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-toast-message";
import {
  Stack,
  Input,
  useToast,
  Select,
  VStack,
  CheckIcon,
  Center,
  HStack,
  Modal,
  Button,
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  TouchableHighlight,
} from "react-native";

const HouseholdScreen = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [answers, setAnswers] = useState([]);
  const [heats, setHeats] = useState([]);
  const [houses, setHouses] = useState([]);
  const [sizes, setSizes] = useState([]);
  const [details, setDetails] = useState("");
  const [loading, setLoading] = useState(true);
  const [lives_in, setLivesIn] = useState("");
  const [home_size, setHomeSize] = useState("");
  const [no_of_shared_people, setSharedPeople] = useState("");
  const [knows_electricity_consumption, setKnowsElectricity] = useState("");
  const [electricity_consumption, setElectricityConsumption] = useState("");
  const [knows_water_consumption, setKnowsWaterConsumption] = useState("");
  const [water_consumption_amount, setWaterConsumption] = useState("");
  const [heat_consumption_amount, setHeatConsumption] = useState("");
  const [heats_water_with, setHeatsWaterWith] = useState("");
  const [heats_house_with, setHeatsHouseWith] = useState("");
  const [knowsConsumption, setKnowsConsumption] = useState(false);
  const [knowsElecConsumption, setKnowsElecConsumption] = useState(false);
  const waterInputRef = createRef();
  const electricityInputRef = createRef();
  const heatInputRef = createRef();
  const peopleInputRef = createRef();
  const toast = useToast();
  const navigation = useNavigation();
  useEffect(() => {
    if (knows_water_consumption === "Yes") {
      setKnowsConsumption(true);
    } else {
      setKnowsConsumption(false);
    }
    if (knows_electricity_consumption === "Yes") {
      setKnowsElecConsumption(true);
    } else {
      setKnowsElecConsumption(false);
    }
  }, [knows_water_consumption, knows_electricity_consumption]);

  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/houses/details",
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson.data);
          if (responseJson.status === "success") {
            const items = responseJson.data;
            setDetails(items);
            setAnswers(items.answers);
            setHeats(items.heats);
            setHouses(items.houses);
            setSizes(items.sizes);
            setLoading(false);
          }
        });
    });
  }, []);
  // console.log(answers);
  // console.log(sizes);
  const submitHouseholdDetails = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      no_of_shared_people: no_of_shared_people,
      electricity_consumption: electricity_consumption,
    });
    // console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/houses/details/create/",
        {
          method: "POST",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          //Hide Loader
          // setLoading(true);
          // console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
               Toast.show({
                 type: "success",
                 text1: responseJson.status,
                 text2: responseJson.msg,
               });
            // setModalVisible(true)
            calculateHomeEmission();
            navigation.navigate("Eco Profile", { screen: "Home" });
          } else {
            setLoading(false);
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
              Toast.show({
                type: "error",
                text1: responseJson.message,
                text2: responseJson.msg,
              });
            // setErrors(responseJson.message);
            // setErrorMessage(responseJson.errors);
          }
        });
    });
    // heatInputRef.current.clear();
    // waterInputRef.current.clear();
    // peopleInputRef.current.clear();
    // electricityInputRef.current.clear();
    setLivesIn("");
    setHomeSize("");
    setKnowsElectricity("");
    setKnowsWaterConsumption("");
    setHeatsWaterWith("");
    setHeatsHouseWith("");
  };

    const calculateHomeEmission = () => {
      setLoading(true);
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        // console.log(user_id);
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/houses/calculate/emission/" +
            user_id,
          {
            method: "POST",
            headers: {
              //Header Definition
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.status === "success") {
              setLoading(false);
              // toast.show({
              //   title: responseJson.status,
              //   status: "success",
              //   description: responseJson.msg,
              //   placement: "top",
              // });
                 Toast.show({
                   type: "success",
                   text1: responseJson.status,
                   text2: responseJson.msg,
                 });
            } else {
              setLoading(false);
            }
          })
          .catch((error) => {
            //Hide Loader
            // setLoading(false);
            console.error(error);
          });
      });
    };
    const calculateButton = () => {
      calculateHomeEmission()
      navigation.navigate("HomeScreen")
    }
  return (
    <ScrollView style={styles.itemsContainer}>
      <View>
        {/* <Loader loading={loading} /> */}
        <Center>
          <Text style={styles.loginheaderStyle}>Home Details</Text>
        </Center>
        <Loader loading={loading} />
        <Modal
          isOpen={modalVisible}
          onClose={setModalVisible}
          size="lg"
          style={styles.centeredView}
        >
          <Modal.Content
            maxH="350"
            style={{ justifyContent: "center", alignItems: "center" }}
          >
            {/* <Modal.CloseButton /> */}
            <Modal.Header>Eco Profile Update Successful</Modal.Header>
            <Modal.Body>
              <Center>
                <Image
                  source={require("../Image/aboutreact.png")}
                  style={{
                    width: "100%",
                    height: 150,
                    resizeMode: "contain",
                    margin: 5,
                  }}
                />
                <Text>
                  Press the button to calculate total household Emission.
                </Text>
              </Center>
              <ScrollView></ScrollView>
            </Modal.Body>
            <Modal.Footer>
              <Button.Group space={2}>
                {/* <Button
                  variant="ghost"
                  colorScheme="blueGray"
                  onPress={() =>
                    navigation.navigate("Eco Profile", { screen: "Home" })
                  }
                >
                  Continue
                </Button> */}
                <Button onPress={calculateButton}>Calculate</Button>
              </Button.Group>
            </Modal.Footer>
          </Modal.Content>
        </Modal>
        <Stack space={3} alignItems="center">
          <VStack space={2} alignItems="center">
            {/* <Heading>HStack</Heading> */}
            <HStack space={1} alignItems="flex-start">
              {/* <Center> */}
              {/* <Select
                variant="underlined"
                selectedValue={lives_in}
                minWidth={350}
                accessibilityLabel="Select your house type"
                placeholder="I live in a e.g House"
                onValueChange={(lives_in) => setLivesIn(lives_in)}
                _selectedItem={{
                  bg: "teal.600",
                  endIcon: <CheckIcon size={4} />,
                }}
              >
                {houses &&
                  houses.map((house, index) => (
                    <Select.Item
                      key={house.id}
                      label={house.type}
                      value={house.type}
                    />
                  ))}
              </Select> */}
              {/* </Center> */}
              {/* <Center>
              <Input
                variant="underlined"
                size="xs"
                placeholder="Km"
                _light={{
                  placeholderTextColor: "blueGray.400",
                }}
                _dark={{
                  placeholderTextColor: "blueGray.50",
                }}
                minWidth={100}
              />
            </Center> */}
            </HStack>
            <HStack space={1} alignItems="center">
              {/* <Center>
                <Select
                  variant="underlined"
                  selectedValue={home_size}
                  minWidth={350}
                  accessibilityLabel="Select your favorite programming language"
                  placeholder="The size is e.g 4 bedrooms"
                  onValueChange={(home_size) => setHomeSize(home_size)}
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={4} />,
                  }}
                >
                  {sizes &&
                    sizes.map((size, index) => (
                      <Select.Item
                        key={size.id}
                        label={size.type}
                        value={size.type}
                      />
                    ))}
                </Select>
              </Center> */}
              {/* <Center>
              <Input
                variant="underlined"
                size="xs"
                placeholder="No of passengers"
                _light={{
                  placeholderTextColor: "blueGray.400",
                }}
                _dark={{
                  placeholderTextColor: "blueGray.50",
                }}
                minWidth={100}
              />
            </Center> */}
            </HStack>
            <Center>
              <Input
                keyboardType="number-pad"
                variant="underlined"
                ref={peopleInputRef}
                onChangeText={(no_of_shared_people) =>
                  setSharedPeople(no_of_shared_people)
                }
                size="sm"
                placeholder="How many people are in your household?"
                _light={{
                  placeholderTextColor: "blueGray.400",
                }}
                _dark={{
                  placeholderTextColor: "blueGray.50",
                }}
                minWidth={350}
              />
            </Center>
            {/* <Center>
              <Select
                variant="underlined"
                selectedValue={knows_electricity_consumption}
                minWidth={350}
                accessibilityLabel="Select your favorite programming language"
                placeholder="I know my electricity consumption"
                onValueChange={(knows_electricity_consumption) =>
                  setKnowsElectricity(knows_electricity_consumption)
                }
                _selectedItem={{
                  bg: "teal.600",
                  endIcon: <CheckIcon size={4} />,
                }}
              >
                {answers &&
                  answers.map((answer, index) => (
                    <Select.Item
                      key={answer.id}
                      label={answer.type}
                      value={answer.type}
                    />
                  ))}
              </Select>
            </Center> */}
          </VStack>
        </Stack>
        <View>
          <Stack space={2} alignItems="center">
            <VStack space={2} alignItems="center">
              {/* <Heading>HStack</Heading> */}
           
                <HStack space={2} alignItems="center">
                  <Center>
                    <Input
                      ref={electricityInputRef}
                      keyboardType="number-pad"
                      variant="underlined"
                      onChangeText={(electricity_consumption) =>
                        setElectricityConsumption(electricity_consumption)
                      }
                      size="sm"
                      placeholder="Main Electricity bill"
                      _light={{
                        placeholderTextColor: "blueGray.400",
                      }}
                      _dark={{
                        placeholderTextColor: "blueGray.50",
                      }}
                      minWidth={270}
                    />
                  </Center>
                  <Center>
                    <Text>Ksh/Month</Text>
                  </Center>
                </HStack>
             

              <Center>
                {/* <Select
                  variant="underlined"
                  selectedValue={heats_house_with}
                  minWidth={350}
                  accessibilityLabel="Select your favorite programming language"
                  placeholder="I heat my house with"
                  onValueChange={(heats_house_with) =>
                    setHeatsHouseWith(heats_house_with)
                  }
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={4} />,
                  }}
                >
                  {heats &&
                    heats.map((heat, index) => (
                      <Select.Item
                        key={heat.id}
                        label={heat.type}
                        value={heat.type}
                      />
                    ))}
                </Select> */}
              </Center>
              <HStack space={2} alignItems="center">
                {/* <Center>
                  <Input
                    ref={heatInputRef}
                    variant="underlined"
                    keyboardType="number-pad"
                    onChangeText={(heat_consumption_amount) =>
                      setHeatConsumption(heat_consumption_amount)
                    }
                    size="sm"
                    placeholder="My heat consumption is"
                    _light={{
                      placeholderTextColor: "blueGray.400",
                    }}
                    _dark={{
                      placeholderTextColor: "blueGray.50",
                    }}
                    minWidth={270}
                  />
                </Center> */}
                {/* <Center>
                  <Text>kWh/Month</Text>
                </Center> */}
              </HStack>
              {/* <Center>
                <Select
                  variant="underlined"
                  selectedValue={heats_water_with}
                  minWidth={350}
                  accessibilityLabel="Select your favorite programming language"
                  placeholder="I heat my water with"
                  onValueChange={(heats_water_with) =>
                    setHeatsWaterWith(heats_water_with)
                  }
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={4} />,
                  }}
                >
                  {heats &&
                    heats.map((waterHeat, index) => (
                      <Select.Item
                        key={waterHeat.id}
                        label={waterHeat.type}
                        value={waterHeat.type}
                      />
                    ))}
                </Select>
              </Center> */}
              {/* <Center>
                <Select
                  variant="underlined"
                  selectedValue={knows_water_consumption}
                  minWidth={350}
                  accessibilityLabel="Select your favorite programming language"
                  placeholder="I know my water consumption"
                  onValueChange={(knows_water_consumption) =>
                    setKnowsWaterConsumption(knows_water_consumption)
                  }
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={4} />,
                  }}
                >
                  {answers &&
                    answers.map((answers, index) => (
                      <Select.Item
                        key={answers.id}
                        label={answers.type}
                        value={answers.type}
                      />
                    ))}
                </Select>
              </Center> */}
              {/* {knowsConsumption && (
                <HStack space={2} alignItems="center">
                  <Center>
                    <Input
                      ref={waterInputRef}
                      variant="underlined"
                      keyboardType="number-pad"
                      onChangeText={(water_consumption_amount) =>
                        setWaterConsumption(water_consumption_amount)
                      }
                      size="sm"
                      placeholder="I consume"
                      _light={{
                        placeholderTextColor: "blueGray.400",
                      }}
                      _dark={{
                        placeholderTextColor: "blueGray.50",
                      }}
                      minWidth={270}
                    />
                  </Center>
                  <Center>
                    <Text>Ltr/Month</Text>
                  </Center>
                </HStack>
              )} */}
            </VStack>
          </Stack>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.buttonStyle}
            underlayColor={"#7EC904"}
            activeOpacity={1}
            onPress={submitHouseholdDetails}
          >
            <Text style={styles.buttonTextStyle}>Save</Text>
          </TouchableHighlight>
        </View>
      </View>
    </ScrollView>
  );
};

export default HouseholdScreen;
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 45,
    marginRight: 45,
    marginTop: 20,
    marginBottom: 25,
    width: 300,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    // borderColor: "#000",
    // shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // elevation: 5,
    // margin: 10,
    // borderRadius: 25,
    // padding: 10,
    // paddingTop: 20,
    // paddingBottom: 20,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    // textAlign: "center",
    fontSize: 18,
    // padding: 10,
    // paddingLeft: 30,
    marginTop: 20,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#727272",
    fontWeight: "bold",
  },
  userInfo: {
    fontSize: 12,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    // marginTop: 10,
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoUsage: {
    fontSize: 14,
    // marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    // flexDirection: "row",
    // paddingLeft: 30,
    // paddingRight: 30,
    width: 360,
  },
  vehicleSelect: {
    // width: 190,
  },
  fuelSelect: {
    // width: 190,
  },
  addContent: {
    paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
  itemUsage: {
    // display: "flex",
    flexDirection: "row",
    marginTop: 10,
    // justifyContent: "space-evenly",
  },
  usageInputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  infoinnerUsage: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    // marginLeft: 50,
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
    // marginLeft: 50,
  },
  miniheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 15,
    padding: 10,
    // width:350,
    // paddingLeft: 30,
    // paddingTop: 20,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
    // height: 700,
    flex: 1,
  },
  addCommute: {
    display: "flex",
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
});
