import React, { useEffect, useState, createRef, useReducer } from "react";
import { useNavigation } from "@react-navigation/native";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";
import {Icon} from "react-native-elements";
import {
  Stack,
  Input,
  useToast,
  Select,
  VStack,
  CheckIcon,
  Center,
  HStack,
  Modal,
  Button,
  Text,
  Divider
} from "native-base";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  TouchableHighlight,
} from "react-native";

const DietScreen = () => {
  const [loading, setLoading] = useState(true);
  const [diet_preferences, setDiet] = useState([]);
  const [knows_calories, setKnowsCalories] = useState([]);
  const [knowsConsumption, setKnowsConsumption] = useState(false);
  const [estimateConsumption, setEstimateConsumption] = useState(false);
  const [calories, setCalories] = useState("");
  const [diet, setUserDiet] = useState("");
  const toast = useToast();
  const [caloriesAmt, setCaloriesAmt] = useState("");
  const [height, setUserHeight] = useState("");
  const [weight, setUserWeight] = useState("");
  const caloriesInputRef = createRef();
  const weightInputRef = createRef();
  const heightInputRef = createRef();
  const [modalVisible, setModalVisible] = useState(false);
  const [ beef, setBeef ] = useState(0);
  const [lamb, setLamb] = useState(0);
  const [prawns, setPrawns] = useState(0);
  const [fish, setFish] = useState(0);
  const [pork, setPork] = useState(0);
  const [chicken, setChicken] = useState(0);
  const [cheese, setCheese] = useState(0);
  const [milk, setMilk] = useState(0);
  const [eggs, setEggs] = useState(0);
  const [beans, setBeans] = useState(0);
  const [people, setPeople] = useState(0);


  const navigation = useNavigation();
  useEffect(() => {
    if (calories === "Yes") {
      setKnowsConsumption(true);
      setEstimateConsumption(false);
    } else if (calories === "No") {
      setKnowsConsumption(false);
      setEstimateConsumption(true);
    }
  }, [calories]);
  // console.log(calories)
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch("https://reducemyco2.farwell-consultants.com/api/v1/diet/details", {
        method: "GET",
        headers: {
          //Header Definition
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson.data);
          if (responseJson.status === "success") {
            const items = responseJson.data;
            setDiet(items.diet_preferences);
            setKnowsCalories(items.knows_calories_i_eat);
            setLoading(false);
          }
        });
    });
  }, []);
  const submitDietDetails = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      lamb: lamb,
      beef: beef,
      prawns: prawns,
      fish: fish,
      chicken: chicken,
      cheese: cheese,
      milk: milk,
      eggs: eggs,
      beans: beans,
      pork: pork,
      people: people,
    });
    console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/diet/details/create",
        {
          method: "POST",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            setLoading(false);
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
            // setModalVisible(true);
            calculateDietEmission();
            navigation.navigate("Eco Profile", { screen: "Home" });
          } else {
            setLoading(false);
            Toast.show({
              type: "error",
              text1: responseJson.message,
              text2: responseJson.msg,
            });
          }
        });
    });
    setCalories("");
    setUserDiet("");
  };
  const calculateFlightEmission = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/calculate/emission/" +
        user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
          } else {
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader
          // setLoading(false);
          console.error(error);
        });
    });
  };
  const calculateDietEmission = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/diet/calculate/emission/" +
        user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
          } else {
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader
          // setLoading(false);
          console.error(error);
        });
    });
  };
  const calculateButton = () => {
    calculateDietEmission();
    navigation.navigate("Eco Profile", { screen: "Home" });
  };
  console.log(beef +"beef")
  const handleBeefIncrement = () => {
    setBeef(prevBeef => prevBeef + 1);
  };

  //Create handleDecrement event handler
  const handleBeefDecrement = () => {
    setBeef(prevBeef => prevBeef - 1);
  };
  const handlePorkIncrement = () => {
    setPork(prevPork => prevPork + 1);
  };

  //Create handleDecrement event handler
  const handlePorkDecrement = () => {
    setPork(prevPork => prevPork - 1);
  };
    const handleChickenIncrement = () => {
    setChicken(prevChicken => prevChicken + 1);
  };

  const handleChickenDecrement = () => {
    setChicken(prevChicken => prevChicken - 1);
  };
      const handlePrawnsIncrement = () => {
    setPrawns(prevPrawns => prevPrawns + 1);
  };

  const handlePrawnsDecrement = () => {
    setPrawns(prevPrawns => prevPrawns - 1);
  };
        const handleFishIncrement = () => {
    setFish(prevPrawns => prevPrawns + 1);
  };

  const handleFishDecrement = () => {
    setFish(prevFish => prevFish - 1);
  };
          const handleLambIncrement = () => {
    setLamb(prevLamb => prevLamb + 1);
  };

  const handleLambDecrement = () => {
    setLamb(prevLamb => prevLamb - 1);
  };

  const handleEggsIncrement = () => {
    setEggs(prevEggs => prevEggs + 1);
  };

  const handleEggsDecrement = () => {
    setEggs(prevEggs => prevEggs - 1);
  };
    const handleCheeseIncrement = () => {
    setCheese(prevCheese => prevCheese + 1);
  };

  const handleCheeseDecrement = () => {
    setCheese(prevCheese => prevCheese - 1);
  };

      const handleMilkIncrement = () => {
    setMilk(prevMilk => prevMilk + 1);
  };

  const handleMilkDecrement = () => {
    setMilk(prevMilk => prevMilk - 1);
  };
        const handleBeansIncrement = () => {
    setBeans(prevBeans => prevBeans + 1);
  };

  const handleBeansDecrement = () => {
    setBeans(prevBeans => prevBeans - 1);
  };
  const handlePeopleIncrement = () => {
    setPeople(prevPeople => prevPeople + 1);
  };

  const handlePeopleDecrement = () => {
    setPeople(prevPeople => prevPeople - 1);
  };
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={styles.itemsContainer}>
        {/* <Loader loading={loading} /> */}
        <Center>
          <Text style={styles.loginheaderStyle}>Describe your diet</Text>
          <Text fontSize="sm">How many times a week do you eat the following</Text>
        </Center>
        <Loader loading={loading} />
        <Modal
          isOpen={modalVisible}
          onClose={setModalVisible}
          size="lg"
          style={styles.centeredView}
        >
          <Modal.Content
            maxH="350"
            style={{ justifyContent: "center", alignItems: "center" }}
          >
            {/* <Modal.CloseButton /> */}
            <Modal.Header>Eco Profile Update Successful</Modal.Header>
            <Modal.Body>
              <Center>
                <Image
                  source={require("../Image/aboutreact.png")}
                  style={{
                    width: "100%",
                    height: 150,
                    resizeMode: "contain",
                    margin: 5,
                  }}
                />
                <Text>Press the button to calculate total Diet Emission.</Text>
              </Center>
              <ScrollView></ScrollView>
            </Modal.Body>
            <Modal.Footer>
              <Button.Group space={2}>
                <Button onPress={calculateButton}>Calculate</Button>
              </Button.Group>
            </Modal.Footer>
          </Modal.Content>
        </Modal>
        <Center mt={3}>
          <VStack space={3} justifyContent="center" minW={380}>
            <HStack  justifyContent="space-around">
              <Text fontSize="sm" color="#7EC925">Protein</Text>
              <Text fontSize="sm" color="#7EC925">Serving</Text>
              <Text fontSize="sm" color="#7EC925">Per week</Text>
            </HStack>
            <HStack justifyContent="space-around" maxW="100%">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm">Beef</Text>
                <Text fontSize="sm">90g</Text>
              </HStack>
              {/* <Text fontSize="sm">Beef</Text>
              <Text fontSize="sm">90g</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleBeefDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{beef}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleBeefIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm">Lamb</Text>
                <Text fontSize="sm">90g</Text>
              </HStack>
              {/* <Text fontSize="sm">Lamb</Text>
              <Text fontSize="sm">90g</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleLambDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{lamb}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleLambIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm">Pork</Text>
                <Text fontSize="sm">90g</Text>
              </HStack>
              {/* <Text fontSize="sm">Pork</Text>
              <Text fontSize="sm">90g</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handlePorkDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{pork}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handlePorkIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm">Chicken</Text>
                <Text fontSize="sm">90g</Text>
              </HStack>
              {/* <Text fontSize="sm">Chicken</Text>
              <Text fontSize="sm">90g</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleChickenDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{chicken}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleChickenIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm">Cheese</Text>
                <Text fontSize="sm">90g</Text>
              </HStack>
              {/* <Text fontSize="sm">Cheese</Text>
              <Text fontSize="sm">90g</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleCheeseDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{cheese}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleCheeseIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm">Milk</Text>
                <Text fontSize="sm">200ml</Text>
              </HStack>
              {/* <Text fontSize="sm">Milk</Text>
              <Text fontSize="sm">200ml</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleMilkDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{milk}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleMilkIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm">Eggs</Text>
                <Text fontSize="sm">90g</Text>
              </HStack>
              {/* <Text fontSize="sm">Eggs</Text>
              <Text fontSize="sm">90g</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleEggsDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{eggs}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleEggsIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm">Beans</Text>
                <Text fontSize="sm">90g</Text>
              </HStack>
              {/* <Text fontSize="sm">Beans</Text>
              <Text fontSize="sm">90g</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleBeansDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{beans}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleBeansIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around" >
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm"> Prawns</Text>
                <Text fontSize="sm">140g</Text>
                </HStack>
                {/* <Text fontSize="sm"> Prawns</Text>
                <Text fontSize="sm">140g</Text> */}
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handlePrawnsDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{prawns}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handlePrawnsIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mx="1" orientation="horizontal" />
            <HStack justifyContent="space-around">
              <HStack space={20} justifyContent="space-around" width={40}>
                <Text fontSize="sm"> Fish</Text>
                <Text fontSize="sm">140g</Text>
                </HStack>
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleFishDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{fish}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handleFishIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
            <Divider bg="#DBDBDB" minW="350" thickness="1" mt="1" mb="4" orientation="horizontal" />
            <HStack  justifyContent="space-around">
              <Text fontSize="sm"> People are in your household?</Text>
              <HStack space={2}>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="remove-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handlePeopleDecrement}
                  size={20}
                  type="material"
                />
                <Text fontSize="sm">{people}</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={handlePeopleIncrement}
                  size={20}
                  type="material"
                />
              </HStack>
            </HStack>
          </VStack>
        </Center>

        <View style={styles.buttonContainer}>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={submitDietDetails}
          >
            <Text style={styles.buttonTextStyle}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default DietScreen;
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 45,
    marginRight: 45,
    marginTop: 20,
    marginBottom: 25,
    width: 300,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    fontSize: 18,
    // paddingLeft: 30,
    marginTop: 20,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
  },
  headerContent: {
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#727272",
    fontWeight: "bold",
  },
  userInfo: {
    fontSize: 12,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    alignItems: "flex-end",
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoUsage: {
    fontSize: 14,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    width: 360,
  },
  addContent: {
    paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
  itemUsage: {
    flexDirection: "row",
    marginTop: 10,
  },
  usageInputStyle: {
    flex: 1,
    color: "#000",
    borderWidth: 1,
    borderRadius: 0,
    borderColor: "#fff",
  },
  infoinnerUsage: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
  },
  miniheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 15,
    padding: 10,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
    minHeight: 700,
  },
  addCommute: {
    display: "flex",
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
});
