import React, { useEffect, useState, createRef } from "react";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  Card,
  Icon,
  BottomSheet,
} from "react-native-elements";
import { useNavigation, StackActions } from "@react-navigation/native";
import Car from "../Image/SVG/car_tab.svg";
import { List, DataTable } from "react-native-paper";
import CheckerComponent from "./Components/CheckerComponent";
import Edit from "../Image/SVG/edit.svg";
import Toast from "react-native-toast-message";
import {
  Stack,
  Input,
  useToast,
  Select,
  Modal,
  CheckIcon,
  Center,
  HStack,
  VStack,
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableHighlight,
  Image,
} from "react-native";


const FlightDisplayScreen = () => {
  const [loading, setLoading] = useState(true);
  const [distance, setDistance] = useState(prevDistance);
  const [destination, setDestination] = useState(prevDestination);
  const [purpose, setPurpose] = useState(prevPurpose);
  const [oneWayRound, setOneWayRound] = useState("");
  const [passengers, setPassengers] = useState(prevPassengers);
  const [flightType, setFlightType] = useState("");
  const [flightClass, setFlightClass] = useState("");
  const [classes, setClasses] = useState([]);
  const [flights, setFlights] = useState([]);
  const [commute, setCommute] = useState(null);
  const [measurement, setMeasurement] = useState("");
  const [ownsVehicle, setOwnsVehicle] = useState();
  const navigation = useNavigation();
  const toast = useToast();
  const [isFirstLaunch, setIsFirstLaunch] = useState(null);
  const [modalVisible, setModalVisible] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const distanceInputRef = createRef();
  const passengerInputRef = createRef();
  const destinationInputRef = createRef();
  const [flightId, setFlightId] = useState("");
  const [newFlights, setFlightsNew] = useState([]);
  const [prevDestination, setPrevDestination] = useState("");
  const [prevDistance, setPrevDistance] = useState(1000);
  const [prevPassengers, setPrevPassengers] = useState(1);
  const [prevPurpose, setPrevPurpose] = useState("");
  const [prevFlightType, setPrevFlightType] = useState("");
  const [prevFlightClass, setPrevFlightClass] = useState("");
  const [aircraft, setAircraft] = useState("");
  const [commercial, setShowCommercial] = useState(false);
  const [chartered, setShowChartered] = useState(false);
  const [engine, setEngine] = useState("");
  const [commercialFlights, setCommercialFlights] = useState([]);
  const [chateredFlight, setChateredFlight] = useState([]);
  const hourInputRef = createRef();
  const minuteInputRef = createRef();
  const [duration, setDuration] = useState("");
  const [hours, setHours] = useState("");
  const [minutes, setMinutes] = useState("");
  const [previousModel, setPreviousModel] = useState("");
  const [previousEngine, setPreviousEngine] = useState("");
  const[previousEngineType, setPreviousEngineType] = useState("");
  // console.log(flightId);
  useEffect(() => {
    if (measurement === "Metric") {
      setMeasurement("Km");
    } else if (measurement === "Imperial") {
      setMeasurement("Miles");
    }
  }, [measurement]);
  useEffect(() => {
    if (flightType === "Commercial") {
      setShowCommercial(true);
      setShowChartered(false);
    }
    else if (flightType === "Chartered") {
      setShowCommercial(false);
      setShowChartered(true);
    }
  }, [flightType]);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/classes/all",
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const items = responseJson.data;
            // console.log(responseJson.data);
            setClasses(items);
            setLoading(false);
          }
        });
    });
  }, []);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch("https://reducemyco2.farwell-consultants.com/api/v1/flights/all", {
        method: "GET",
        headers: {
          //Header Definition
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const items = responseJson.data;
            // console.log(responseJson.data);
            setFlights(items);
            setLoading(false);
          } else {
            setLoading(false);
          }
        });
    });
  }, []);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      //  alert("Refreshed!");
      // getUser();
      getData();
    });
    return unsubscribe;
  }, [navigation]);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/user/all/" +
          user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommute(responseJson.data);

            // setProfileComplete(user.profile_is_complete);
            // setVehicle(user.vehicles);
            // setLoading(false);
            // console.log(user[0]);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);

  const getData = () => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/user/all/" +
          user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommute(responseJson.data);
            // setProfileComplete(user.profile_is_complete);
            // setVehicle(user.vehicles);
            // setLoading(false);
            // console.log(user[0]);
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  // console.log(prevFlightClass + " prev class");
  //  console.log(prevFlightType + " prev type");
  const getFlight = (flightId, destination, distance, type, flightClass) => {
    setLoading(true)
    setFlightId(flightId);
    setPrevDestination(destination);
    // setPrevDistance(JSON.stringify(distance));
     setPrevFlightType(type);
     setPrevFlightClass(flightClass);

    // console.log(passengers + "pppp");
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/user/" +
          flightId,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setFlightsNew(responseJson.data);
            setIsVisible(true);
            setPrevPassengers(JSON.stringify(user.number_of_passengers));
            setPrevPurpose(user.purpose);
            setPreviousModel(user.model);
            setPreviousEngine(user.engine);
            setPreviousEngineType(user.engine_type);
            setPrevDistance(JSON.stringify(user.distance));
            setLoading(false)
            console.log(newFlights)
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  
  const submitFlightButton = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      flight_type: flightType,
      flight_class: flightClass,
      duration: duration,
      one_way_or_round: oneWayRound,
      number_of_passengers: passengers,
      model: aircraft,
      engine_type: engine,
    });
    console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/user/update/" +
          user_id +
          "/" +
          flightId,
        {
          method: "PUT",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          //Hide Loader
          // setLoading(true);
          // console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            //    duration:3000
            // });
              Toast.show({
                type: "success",
                text1: responseJson.status,
                text2: responseJson.msg,
              });
            getData();
            setIsVisible(false);
            calculateFlightEmission();
          } else {
            setLoading(false);
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: "Please check the data and try again",
            //   placement: "top",
            //   duration: 3000,
            // });
                Toast.show({
                  type: "error",
                  text1: responseJson.message,
                  text2: "Please check the data and try again",
                });
          }
        });
    });
    // distanceInputRef.current.clear();
    passengerInputRef.current.clear();
    setFlightClass("");
    setFlightType("");
    setOneWayRound("");
    // destinationInputRef.current.clear();
    setPurpose("");
  };
  const calculateFlightEmission = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/flights/calculate/emission/" +
          user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            //   duration:2000
            // });
                Toast.show({
                  type: "success",
                  text1: responseJson.status,
                  text2: responseJson.msg,
                });
          } else {
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader
          // setLoading(false);
          console.error(error);
        });
    });
  };
  const flightButton = () => {
    setIsFirstLaunch(false);
  };
  useEffect(() => {
    CheckerComponent().then((isFirstLaunch) => {
      setIsFirstLaunch(isFirstLaunch);
    });
  }, []);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/flights/private")
      .then((response) => response.json())
      .then((data) => {
        if (data.status === "success") {
          setChateredFlight(data.data);
        } else {
          setLoading(true);
        }
      }
      ).catch((error) => {
        console.log(error);
      }
      );
  }, []);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/flights/commercial")
      .then((response) => response.json())
      .then((data) => {
        if (data.status === "success") {
          setCommercialFlights(data.data);
        } else {
          setLoading(true);
        }
      }
      ).catch((error) => {
        console.log(error);
      }
      );
  }, []);

  if (isFirstLaunch === null) return null;
  return isFirstLaunch ? (
    <View>
      <View>
        <Text style={styles.miniheaderStyle}>If you don’t fly at all,</Text>
      </View>
      <View>
        <Text style={styles.congratsHeaderStyle}>Congratulations!</Text>
      </View>
      <View style={styles.item}>
        {/* <Text style={styles.miniheaderStyle}>
                Please continue with {"\n"} the rest of your CO
                footprint profile.
              </Text> */}
      </View>
      <View style={styles.addContent}>
        <Text style={styles.miniheaderStyle}>
          Please continue with {"\n"} the rest of your CO
          <Text style={{ fontSize: 10, lineHeight: 37 }}>2</Text> footprint
          profile.
        </Text>
        <Icon
          color="#23A9C6"
          containerStyle={{}}
          disabledStyle={{}}
          iconProps={{}}
          iconStyle={{}}
          name="arrow-forward-circle-outline"
          onPress={() => navigation.navigate("Eco Profile", { screen: "Diet" })}
          size={40}
          type="ionicon"
        />
      </View>
      <View style={styles.buttonContainer}>
        <TouchableHighlight
          style={styles.buttonStyle}
          underlayColor={"#7EC904"}
          activeOpacity={1}
          onPress={flightButton}
        >
          <Text style={styles.buttonTextStyle}>Add Flight</Text>
        </TouchableHighlight>
      </View>
    </View>
  ) : (
    <ScrollView style={styles.itemsContainer}>
      <Loader loading={loading} />
      <View style={styles.itemsContainer}>
        <View>
          <Text style={styles.loginheaderStyle}>Your Flight Summary</Text>
        </View>

        <List.Section title="">
          <List.Accordion
            titleStyle={styles.accordion}
            title="My Flights"
            left={(props) => <List.Icon icon="airplane" />}
          >
            {commute && commute.map((u, i) => {
              // setFlightId(u.id)
              // console.log(u);
              return (
                <List.Item
                  key={i}
                  title=""
                  left={(props) => (
                    <Card containerStyle={styles.card}>
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Flight Class:</Text>
                        <Text style={styles.userInfo}>
                          {u.flight_class_name}
                        </Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Flight Type:</Text>
                        <Text style={styles.userInfo}>{u.flight_type}</Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Duration:</Text>
                        <Text style={styles.userInfo}>
                          {u.duration + " Hours"}
                        </Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Aircraft:</Text>
                        <Text style={styles.userInfo}>{u.model}</Text>
                      </View>
                      <Card.Divider />
                      <View style={styles.cardStyle}>
                        <Text style={styles.name}>Engine Type:</Text>
                        <Text style={styles.userInfo}>{u.engine_type}</Text>
                      </View>

                      <BottomSheet
                        isVisible={isVisible}
                        // containerStyle={styles.itemsContainer}
                      >
                        <Stack
                          space={2}
                          alignItems="center"
                          style={styles.editsContainer}
                        >
                          <Select
                            defaultValue={prevFlightType}
                            variant="underlined"
                            selectedValue={flightType}
                            minWidth={350}
                            fontSize={14}
                            accessibilityLabel="Select your flight"
                            placeholder="Flight Type"
                            onValueChange={(flightType) =>
                              setFlightType(flightType)
                            }
                            _selectedItem={{
                              bg: "teal.600",
                              endIcon: <CheckIcon size={4} />,
                            }}
                          >
                            {flights &&
                              flights.map((flight, index) => (
                                <Select.Item
                                  key={flight.id}
                                  label={flight.flight_type}
                                  value={flight.flight_type}
                                />
                              ))}
                          </Select>

                          <HStack space={2} alignItems="center">
                            <Center>
                              <Select
                                defaultValue={prevFlightClass}
                                variant="underlined"
                                selectedValue={flightClass}
                                minWidth={350}
                                fontSize={14}
                                accessibilityLabel="Class"
                                placeholder="Class"
                                onValueChange={(flightClass) =>
                                  setFlightClass(flightClass)
                                }
                                _selectedItem={{
                                  bg: "teal.600",
                                  endIcon: <CheckIcon size={4} />,
                                }}
                              >
                                {classes &&
                                  classes.map((classes, index) => (
                                    <Select.Item
                                      key={classes.id}
                                      label={classes.flight_class_name}
                                      value={classes.flight_class_name}
                                    />
                                  ))}
                              </Select>
                            </Center>
                            {/* <Center>
                              <Input
                                defaultValue={prevPassengers}
                                keyboardType="number-pad"
                                variant="underlined"
                                size="sm"
                                placeholder="No of passengers"
                                ref={passengerInputRef}
                                onChangeText={(passengers) =>
                                  setPassengers(passengers)
                                }
                                minWidth={150}
                              />
                            </Center> */}
                          </HStack>
                          <HStack space={2} alignItems="center">
                            <Input
                              size="sm"
                              ref={hourInputRef}
                              onChangeText={(hours) => setHours(hours)}
                              keyboardType="number-pad"
                              variant="underlined"
                              placeholder="Hours"
                              minW="175px" />
                            <Input
                              size="sm"
                              keyboardType="number-pad"
                              ref={minuteInputRef}
                              onChangeText={(minutes) => setMinutes(minutes)}
                              variant="underlined"
                              placeholder="Minutes"
                              minW="175px" />
                          </HStack>
                          <HStack space={2} alignItems="center">
                            <Center>
                              <Select
                                defaultValue="One Way"
                                variant="underlined"
                                selectedValue={oneWayRound}
                                minWidth={350}
                                fontSize={14}
                                accessibilityLabel="Select your flight"
                                placeholder="One Way"
                                onValueChange={(oneWayRound) =>
                                  setOneWayRound(oneWayRound)
                                }
                                _selectedItem={{
                                  bg: "teal.600",
                                  endIcon: <CheckIcon size={4} />,
                                }}
                              >
                                <Select.Item label="One way" value={0} />
                                <Select.Item label="Round" value={1} />
                              </Select>
                            </Center>
                          </HStack>
                          {commercial && (
                            <HStack space={2} alignItems="center">
                              <Center>
                                <Select
                                  variant="underlined"
                                  selectedValue={aircraft}
                                  defaultValue={previousModel}
                                  minWidth={350}
                                  fontSize={14}
                                  accessibilityLabel="Select plane make"
                                  placeholder="Select plane make (Commercial)"
                                  onValueChange={(aircraft) => setAircraft(aircraft)}
                                  _selectedItem={{
                                    bg: "teal.600",
                                    endIcon: <CheckIcon size={4} />,
                                  }}
                                >
                                  {commercialFlights &&
                                    commercialFlights.map((commercialFlights, index) => (
                                      <Select.Item
                                        key={commercialFlights.id}
                                        label={commercialFlights.aircraft}
                                        value={commercialFlights.aircraft}
                                      />
                                    ))}
                                </Select>
                              </Center>
                            </HStack>
                          )
                          }
                          {chartered && (
                            <Center>
                              <Select
                                variant="underlined"
                                defaultValue={previousModel}
                                selectedValue={aircraft}
                                minWidth={350}
                                fontSize={14}
                                accessibilityLabel="Select plane make"
                                placeholder="Select plane make (Charter)"
                                onValueChange={(aircraft) => setAircraft(aircraft)}
                                _selectedItem={{
                                  bg: "teal.600",
                                  endIcon: <CheckIcon size={4} />,
                                }}
                              >
                                {chateredFlight &&
                                  chateredFlight.map((chateredFlight, index) => (
                                    <Select.Item
                                      key={chateredFlight.id}
                                      label={chateredFlight.aircraft}
                                      value={chateredFlight.aircraft}
                                    />
                                  ))}
                              </Select>
                            </Center>
                          )}
                          <Center>
                            <Select
                              variant="underlined"
                              defaultValue={previousEngineType}
                              selectedValue={engine}
                              minWidth={350}
                              fontSize={14}
                              accessibilityLabel="Engine Type"
                              placeholder="Engine Type"
                              onValueChange={(engine) => setEngine(engine)}
                              _selectedItem={{
                                bg: "teal.600",
                                endIcon: <CheckIcon size={4} />,
                              }}
                            >
                              <Select.Item label="Jet" value="Jet" />
                              <Select.Item label="Propeller" value="Propeller" />
                            </Select>
                            <Input
                              defaultValue={prevPassengers}
                              keyboardType="number-pad"
                              variant="underlined"
                              size="sm"
                              placeholder="No of passengers"
                              ref={passengerInputRef}
                              onChangeText={(passengers) =>
                                setPassengers(passengers)
                              }
                              minWidth={350}
                              InputRightElement={"Passengers"}
                            />
                          </Center>
                          <VStack space={1} alignItems="center">
                            <HStack space={2} alignItems="center">
                              <Center>
                                <TouchableOpacity
                                  style={styles.buttonCancelStyle}
                                  activeOpacity={0.5}
                                  onPress={() => setIsVisible(false)}
                                >
                                  <Text style={styles.buttonTextStyle}>
                                    Cancel
                                  </Text>
                                </TouchableOpacity>
                              </Center>
                              <Center>
                                <TouchableOpacity
                                  style={styles.buttonEditStyle}
                                  activeOpacity={0.5}
                                  onPress={submitFlightButton}
                                >
                                  <Text style={styles.buttonTextStyle}>
                                    Save
                                  </Text>
                                </TouchableOpacity>
                              </Center>
                            </HStack>
                          </VStack>
                        </Stack>
                      </BottomSheet>
                      <TouchableOpacity
                        style={styles.addContent}
                        activeOpacity={0.2}
                        onPress={() =>
                          getFlight(
                            u.id,
                            u.destination,
                            u.distance,
                            u.flight_type,
                            u.flight_class_name
                          )
                        }
                      >
                        {/* <View style={styles.addContent}> */}
                        <Text style={{ color: "#707070" }}>Edit Flight</Text>
                        <Edit style={styles.editIcon} />
                        {/* </View> */}
                      </TouchableOpacity>
                    </Card>
                  )}
                />
              );
            })}
            {/* <List.Item
                title=""
                titleStyle={styles.titleStyle}
                right={(props) => (
                  <View style={styles.addContent}>
                    <Text style={{ color: "#707070" }}>Edit Flight</Text>
                    <Icon
                      color="#23A9C6"
                      containerStyle={{}}
                      disabledStyle={{}}
                      iconProps={{}}
                      iconStyle={{}}
                      name="add-circle-outline"
                      onLongPress={() => console.log("onLongPress()")}
                      onPress={() => navigation.navigate("FlightScreen")}
                      size={40}
                      type="material"
                    />
                  </View>
                )}
              /> */}
          </List.Accordion>
        </List.Section>

        <VStack space={1} alignItems="center">
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={() => navigation.navigate("FlightScreen")}
          >
            <Text style={styles.buttonTextStyle}>Add Flight</Text>
          </TouchableOpacity>
        </VStack>
      </View>
    </ScrollView>
  );
};

export default FlightDisplayScreen;
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonEditStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 150,
    justifyContent: "center",
  },
  buttonCancelStyle: {
    backgroundColor: "#707070",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#707070",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 150,
    justifyContent: "center",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 45,
    marginRight: 45,
    marginTop: 20,
    marginBottom: 25,
    width: 150,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    fontSize: 20,
    padding: 10,
    paddingLeft: 30,
    marginTop: 20,
    // fontWeight:"bold
    paddingTop: 0,
  },
  congratsHeaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    // paddingLeft: 30,
    paddingTop: 0,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 18,
    color: "#727272",
    fontWeight: "800",
    paddingRight: 20,
  },
  userInfo: {
    fontSize: 17,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    // marginTop: 10,
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 80,
    height: 50,
    // marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoUsage: {
    fontSize: 14,
    // marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    // flexDirection: "row",
    // paddingLeft: 30,
    // paddingRight: 30,
    width: 360,
  },
  vehicleSelect: {
    // width: 190,
  },
  fuelSelect: {
    // width: 190,
  },
  addContent: {
    // paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  editIcon: {
    width: 50,
    height: 40,
  },
  itemUsage: {
    // display: "flex",
    flexDirection: "row",
    marginTop: 10,
    // justifyContent: "space-evenly",
  },
  usageInputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  infoinnerUsage: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    // marginLeft: 50,
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
    // marginLeft: 50,
  },
  miniheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 15,
    padding: 10,
    // width:350,
    // paddingLeft: 30,
    // paddingTop: 20,
  },
  titleStyle: {
    color: "#23A9C6",
  },
  rightTitleStyle: {
    color: "#23A9C6",
    fontSize: 16,
    paddingTop: 5,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    // height: 700,
  },
  editsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    paddingTop: 20,
  },
  addCommute: {
    display: "flex",
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  user: {
    alignItems: "center",
    flexDirection: "row",
  },
  card: {
    width: "90%",
    minWidth: 300,
    // padding: 5,
  },
  cardStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
  },
  cellStyle: {
    minWidth: 70,
    padding: 2,
  },
  accordion: {
    color: "#23A9C6",
  },
});
