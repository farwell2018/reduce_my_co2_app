import React, { useEffect, useState, useCallback } from "react";
import Loader from "./Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Card, BottomSheet, Icon } from "react-native-elements";
import { useNavigation, StackActions } from "@react-navigation/native";
import Car from "../Image/SVG/car_tab.svg";
import { List, Divider } from "react-native-paper";
import Edit from "../Image/SVG/edit.svg";
import Toast from "react-native-toast-message";
import {
  Stack,
  Input,
  FormControl,
  Select,
  VStack,
  CheckIcon,
  Center,
  HStack,
  useToast,
  Modal,
  Button,
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableHighlight,
  SafeAreaView,
} from "react-native";


const EcoDisplayScreen = () => {
  const [profile, setProfile] = useState([]);
  const [profileComplete, setProfileComplete] = useState();
  const [loading, setLoading] = useState(true);
  const [vehicles, setVehicle] = useState([]);
  const [journeyVehicles, setJourneyVehicle] = useState([]);
  const [journey, setJourney] = useState([]);
  const [journeys, setJourneys] = useState([]);
  const [commute, setCommute] = useState([]);
  const [measurement, setMeasurement] = useState("");
  const [ownsVehicle, setOwnsVehicle] = useState();
  const navigation = useNavigation();
  const toast = useToast();
  const [isVisible, setIsVisible] = useState(false);
  const [item, setItems] = useState([]);
  const [data, setData] = useState("");
  const [journeyId, setJourneyId] = useState("");
  const [prevDays, setPrevDays] = useState("");
  const [prevDistance, setPrevDistance] = useState("");
  const [prevJourney, setPrevJourney] = useState("");
  const [vehicleName, setVehicleName] = useState("");
  const [vehicleType, setVehicleType] = useState("");
  const [userFuel, setFuel] = useState("");
  const [fuelConsumptionAmount, setfuelConsumptionAmount] = useState("");
  const [journey_name, setJourneyName] = useState("");
  const [journey_vehicle, setjourneyvehicle] = useState("");
  const [journey_days, setjourneydays] = useState("");
  const [journey_distance, setjourneydistance] = useState("");
  const [commuteId, setCommuteId] = useState("");
  const [commuteVehicle, setCommuteVehicle] = useState("");
  const [commuteDays, setCommuteDays] = useState("");
  const [commuteDistance, setCommuteDistance] = useState("");
  const [days, setDays] = useState("");
  const [distance, setDistance] = useState("");
  const [country, setCountry] = useState("");
  const [id, setId] = useState("");
  const [vehicleData, setVehicleData] = useState("");
  const [carType, setCarType] = useState([]);
  const [fuel, setItemFuel] = useState([]);
  const [disabled, setDisabled] = useState(false);
  const [weeklyFuel, setWeeklyFuel] = useState("");

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      //  alert("Refreshed!");
      getCommutes();
      getJourneys();
      getData();
    });
    return unsubscribe;
  }, [navigation]);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/vehicles/all")
      .then((response) => response.json())
      .then((responseJson) => {
        const carType = responseJson.data;
        setCarType(carType);
        console.log(responseJson)
      });
  }, []);
  useEffect(() => {
    if (measurement === "Metric") {
      setMeasurement("Km");
    } else if (measurement === "Imperial") {
      setMeasurement("Miles");
    }
  }, [measurement]);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/fuels/all")
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status === "success") {
          const fuel = responseJson.data;
          setItemFuel(fuel);
          // console.log(fuel);
          setLoading(false);
        }
      });
  }, []);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          const user = responseJson.data;
          setCountry(user.country.name);
          setItems(user.vehicles);
          // setData(user.preferred_measurement);
          if (user.preferred_measurement === 1) {
            setData("Miles");
          } else if (user.preferred_measurement === 0) {
            setData("KM");
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch("https://reducemyco2.farwell-consultants.com/api/v1/journeys/all", {
        method: "GET",
        headers: {
          //Header Definition
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const items = responseJson.data;
            // console.log(responseJson.data);
            setJourneys(items);
            setLoading(false);
          }
        });
    });
  }, []);
  const submitVehicleButton = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      vehicle_name: vehicleName,
      vehicle_type: vehicleType,
      vehicle_fuel_type: userFuel,
      knows_fuel_consumption: fuelConsumption,
      fuel_per_week: fuelConsumptionAmount,
      motorbike_type: motorBikeType,
    });
    // console.log(formBody);
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/eco/vehicles/create",
        {
          method: "POST",
          body: formBody,
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            // setUpdateSuccess(true);
            // setState(true);
            setLoading(false);
            // setModalVisible(true);
            toast.show({
              title: responseJson.status,
              status: "success",
              description: responseJson.msg,
              placement: "top",
            });
            // calculateCarbon();
            // alert(responseJson.msg);
          } else {
            setLoading(false);
            setErrors(responseJson.message);
            setErrorMessage(responseJson.errors);
            toast.show({
              title: responseJson.message,
              status: "info",
              description: "Vehicle Details were not updated",
              placement: "top",
            });

          }
          calculateCarbon();
        })

        .catch((error) => { });
    });
  };
  const calculateCarbon = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/footprint/user/" +
        user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
            setLoading(false);
          } else {
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: "Please check the data and try again",
            //   placement: "top",
            // });
            Toast.show({
              type: "error",
              text1: responseJson.message,
              text2: "Please check the data and try again",
            });
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader

          console.errorr(error);
        });
    });
    // getData();
    // console.log(item);
  };
  const getData = () => {
    // setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setProfile(user);
            setProfileComplete(user.profile_is_complete);
            setVehicle(user.vehicles);
            setMeasurement(user.user_preferred_measurement);
            setLoading(false);
            // console.log(user);
            if (user.vehicles === []) {
              toast.show({
                title: "No Vehicles Added",
                status: "warning",
                description: "Please add vehicle and commute information",
                placement: "top",
              });
            }
          }
        })
        .catch((err) => console.log(err.message));
    });
  };

  const getJourneys = () => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/journeys/user/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setJourney(responseJson.data);
            // setProfileComplete(user.profile_is_complete);
            // setVehicle(user.vehicles);
            // setLoading(false);
            // console.log(user[0]);
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  const getCommutes = () => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/commutes/user/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommute(responseJson.data);
            // setProfileComplete(user.profile_is_complete);
            // setVehicle(user.vehicles);
            // setLoading(false);
            // console.log(user[0]);
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/journeys/user/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setJourney(responseJson.data);
            // setProfileComplete(user.profile_is_complete);
            // setVehicle(user.vehicles);
            // setLoading(false);
            // console.log(user[0]);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  // console.log(vehicles);
  // console.log(journey);
  // console.log(commute);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/commutes/user/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommute(responseJson.data);
            // setProfileComplete(user.profile_is_complete);
            // setVehicle(user.vehicles);
            // setLoading(false);
            // console.log(user[0]);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  // console.log(commute);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setProfile(user);
            setProfileComplete(user.profile_is_complete);
            setVehicle(user.vehicles);
            setMeasurement(user.user_preferred_measurement);
            setLoading(false);
            // console.log(vehicles);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);

  const updateJourney = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      // vehicle_name: journey_vehicle,
      journey: journey_name,
      days_per_week: journey_days,
      one_way_distance: journey_distance,
    });
    console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/journeys/user/update/" +
        journeyId,
        {
          method: "PATCH",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          //Hide Loader
          // setLoading(true);
          // console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
            getJourneys();
            setIsVisible(false);
            // calculateDietEmission()
          } else {
            setLoading(false);
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "error",
              text1: responseJson.message,
              text2: "Please check the data and try again",
            });
          }
        });
    });
  };
  const updateCommute = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      // vehicle_name: "Demio",
      days_per_week: days,
      distance_per_day: distance,
    });
    console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/commutes/user/update/" +
        commuteId,
        {
          method: "PATCH",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          //Hide Loader
          // setLoading(true);
          // console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            setLoading(false);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "success",
              text1: responseJson.status,
              text2: responseJson.msg,
            });
            getCommutes();
            setIsVisible(false);
            // calculateDietEmission()
          } else {
            setLoading(false);
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
            Toast.show({
              type: "error",
              text1: responseJson.message,
              text2: "Please check the data and try again",
            });
          }
        });
    });
  };
  const updateVehicle = () => {
    var formBody = JSON.stringify({
      vehicle_name: vehicleName,
      vehicle_type: vehicleType,
      vehicle_fuel_type: userFuel,
      fuel_per_week: fuelConsumptionAmount,
    });
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/eco/vehicles/update/" +
        id,
        {
          method: "PATCH",
          body: formBody,
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          setIsVisible(false);
          console.log(responseJson);
        })
        .catch((err) => console.log(err.message));
    });
  }

  const getVehicle = (id) => {
    setIsVisible(true);
    setId(id);
    console.log(id);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/eco/vehicles/" +
        id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const vehicle = responseJson.data;
            console.log(vehicle);
            setVehicleData(vehicle);
            console.log(vehicleData)
            setWeeklyFuel(JSON.stringify(vehicle.fuel_per_week));
          }
        })
        .catch((err) => console.log(err.message));
    });
  }
  const ChoiceComponent = () => {
    const [hasVehicle, setHasVehicle] = useState("Yes");
    const [noVehicle, setNoVehicle] = useState("No");

    const ownVehicle = () => {
      var formBody = JSON.stringify({
        owns_vehicle: hasVehicle,
        // owns_vehicle: noVehicle,
      });
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        console.log(formBody + "formBody");
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/user/owns/vehicle/" +
          user_id,
          {
            method: "PUT",
            body: formBody,
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            const user = responseJson;
            // setUser(user);
          });
      });
      // }, []);
    };
    const yes = () => {
      setHasVehicle((currHasVehicle) => currHasVehicle);
      ownVehicle();
      setOwnsVehicle(true);
      getUser();
    };
    const no = () => {
      // setHasVehicle("No");
      // ownVehicle();
      // getUser();
      // setCongrats(true);
      setEcoView(false);
    };
    const getUser = () => {
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
          {
            method: "GET",
            headers: {
              //Header Definition
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.status === "success") {
              const user = responseJson.data;
              // setUser(user);
              console.log(user.owns_vehicle);
              if (user.owns_vehicle === 1) {
                setOwnsVehicle(true);
                // setLoading(false);
              }
            }
          });
      });
    };

    return (
      <ScrollView style={{ flex: 1 }}>
        <View>
          <Loader loading={loading} />
          <View style={styles.item}>
            <Text style={styles.info}>Do you own a Vehicle?</Text>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={yes}
            >
              <Text style={styles.buttonTextStyle}>Yes</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.buttonRegisterStyle}
              activeOpacity={0.5}
              onPress={no}
            >
              <Text style={styles.buttonTextStyle}>No</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  };

  const getJourney = (journeyId, days, distance, journey, name) => {
    setPrevDays(JSON.stringify(days));
    setJourneyId(journeyId);
    setPrevDistance(JSON.stringify(distance));
    setPrevJourney(journey);
    setVehicleName(name);
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/user/journey/" +
        journeyId,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setIsVisible(true);
            setLoading(false)
          }
        })
        .catch((err) => console.log(err.message));
    });
  };

  const getCommute = (commuteId, name, days) => {
    setCommuteId(commuteId);
    setCommuteVehicle(name)
    setCommuteDays(JSON.stringify(days))
    setLoading(true)
    console.log(commuteDistance + "commute distance")
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/user/commute/" +
        commuteId,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setCommuteDistance(JSON.stringify(user.distance_per_day / 2));
            setIsVisible(true);
            setLoading(false)
          }
        })
        .catch((err) => console.log(err.message));
    });
  };
  useEffect(() => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
        user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            // setUser(user);
            console.log(user + "++++++ niko hapa");
            console.log(user.owns_vehicle);
            if (user.owns_vehicle === 1) {
              setOwnsVehicle(true);
              setLoading(false);
            } else if (user.owns_vehicle === 0) {
              setOwnsVehicle(false);
              setLoading(false);
            }
          }
        });
    });
  }, [ownsVehicle]);
  console.log(ownsVehicle + "++++++++");
  if (ownsVehicle === false) {
    return <ChoiceComponent />;
  } else if (ownsVehicle === true) {
    return (
      <ScrollView style={styles.itemsContainer}>
        <Loader loading={loading} />
        <View style={styles.itemsContainer}>
          <View>
            <Text style={styles.loginheaderStyle}>Your Vehicle Summary</Text>
          </View>
          <List.Section title="">
            <List.Accordion
              titleStyle={styles.accordion}
              title="My Vehicles"
              left={(props) => <List.Icon icon="car" />}
            >
              {vehicles.map((u, i) => {
                return (
                  <Card>
                    <View style={styles.cardStyle}>
                      <Text style={styles.name}>{u.vehicle_name}</Text>
                    </View>
                    {/* <View style={styles.cardStyle}>
                      <Text style={styles.name}>{u.fuel_per_week}</Text>
                    </View> */}
                    <HStack justifyContent='space-between'>
                      <Text style={styles.name}>Fuel Type</Text>
                      <Text style={styles.name}>{u.vehicle_fuel_type == 1 ? "Petrol" : "Diesel"}</Text>
                    </HStack>
                    <HStack justifyContent='space-between'>
                      <Text style={styles.name}>Weekly Fuel</Text>
                      <Text style={styles.name}>{u.fuel_per_week + " "}{country == "Kenya" ? "Ksh" : "£"}</Text>
                    </HStack>
                    <Card.Divider />
                    <TouchableOpacity
                      style={styles.editContent}
                      activeOpacity={0.2}
                      onPress={() => getVehicle(u.id)}
                    >
                      <Text style={{ color: "#707070" }}>Edit Vehicle</Text>
                      <Edit style={styles.editIcon} />
                    </TouchableOpacity>
                  </Card>
                );
              })}
              <List.Item
                title=""
                titleStyle={styles.titleStyle}
                right={(props) =>
                  null
                }
              />
            </List.Accordion>

          </List.Section>
          <BottomSheet
            isVisible={isVisible}
          // containerStyle={styles.itemsContainer}
          >
            <Stack
              space={2}
              // alignItems="center"
              style={styles.editsContainer}
            >
              <Input
                // ref={vehicleInputRef}
                defaultValue={vehicleData.vehicle_name}
                variant="underlined"
                minWidth={200}
                placeholder="Vehicle Name"
                inputContainerStyle={{
                  borderBottomColor: "#00000029",
                }}
                errorProps={{}}
                onChangeText={(vehicleName) => setVehicleName(vehicleName)}
              />
              <FormControl isRequired style={styles.vehicleSelect}>
                <Select
                  selectedValue={vehicleType}
                  minWidth={200}
                  variant="underlined"
                  accessibilityLabel="Type of vehicle"
                  placeholder="Type of vehicle"
                  onValueChange={(vehicleType) => setVehicleType(vehicleType)}
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={5} />,
                  }}
                  mt={1}
                >
                  {carType &&
                    carType.map((car, index) => (
                      <Select.Item
                        key={car.id}
                        label={car.vehicle_name}
                        value={car.vehicle_name}
                      />
                    ))}
                </Select>
              </FormControl>
              <FormControl isRequired style={styles.fuelSelect}>
                <Select
                  defaultValue={vehicleData.vehicle_fuel_type}
                  selectedValue={userFuel}
                  minWidth={200}
                  variant="underlined"
                  accessibilityLabel="Fuel Type"
                  placeholder="Fuel Type"
                  onValueChange={(userFuel) => setFuel(userFuel)}
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={5} />,
                  }}
                  mt={1}
                >
                  {fuel &&
                    fuel.map((fuels, index) => (
                      <Select.Item
                        key={fuels.id}
                        label={fuels.fuel_name}
                        value={fuels.fuel_name}
                      />
                    ))}
                </Select>
              </FormControl>
              <Input
                // ref={}
                defaultValue={weeklyFuel}
                variant="underlined"
                placeholder="Average amount spent on fuel each week"
                inputContainerStyle={{
                  borderBottomColor: "#00000029",
                }}
                //   leftIcon={{ type: "font-awesome", name: "calendar" }}
                isDisabled={disabled}
                errorProps={{}}
                mb={5}
                onChangeText={(fuelConsumptionAmount) =>
                  setfuelConsumptionAmount(fuelConsumptionAmount)
                }
              />
              <Center>
                <TouchableOpacity
                  style={styles.buttonEditStyle}
                  activeOpacity={0.5}
                  onPress={() => setIsVisible(false)}
                >
                  <Text style={styles.buttonTextStyle}>
                    Save
                  </Text>
                </TouchableOpacity>
              </Center>
 
            </Stack>
          </BottomSheet>
          <VStack space={1} alignItems="center">
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={() => navigation.navigate("Eco Home")}
            >
              <Text style={styles.buttonTextStyle}>Add Vehicle</Text>
            </TouchableOpacity>
          </VStack>
        </View>
      </ScrollView>
    );
  } else
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1, padding: 16 }}>
          <Loader loading={loading} />
        </View>
      </SafeAreaView>
    );
};

export default EcoDisplayScreen;
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 45,
    marginRight: 45,
    marginTop: 20,
    marginBottom: 25,
    width: 300,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    fontSize: 20,
    // padding: 10,
    paddingLeft: 30,
    marginTop: 20,
    // fontWeight:"bold"
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#727272",
    fontWeight: "bold",
  },
  userInfo: {
    fontSize: 12,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    // marginTop: 10,
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 80,
    height: 50,
    // marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoUsage: {
    fontSize: 14,
    // marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    // flexDirection: "row",
    // paddingLeft: 30,
    // paddingRight: 30,
    width: 360,
  },
  vehicleSelect: {
    // width: 190,
  },
  fuelSelect: {
    // width: 190,
  },
  addContent: {
    // paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    // marginTop: 20,
  },
  itemUsage: {
    // display: "flex",
    flexDirection: "row",
    marginTop: 10,
    // justifyContent: "space-evenly",
  },
  usageInputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  infoinnerUsage: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    // marginLeft: 50,
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
    // marginLeft: 50,
  },
  miniheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 15,
    padding: 10,
    // width:350,
    // paddingLeft: 30,
    // paddingTop: 20,
  },
  titleStyle: {
    color: "#23A9C6",
  },
  rightTitleStyle: {
    color: "#23A9C6",
    fontSize: 16,
    paddingTop: 5,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    // height: 700,
  },
  addCommute: {
    display: "flex",
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  user: {
    alignItems: "center",
    flexDirection: "row",
  },
  card: {
    minWidth: 250,
    padding: 5,
  },
  accordion: {
    color: "#23A9C6",
  },
  cardStyle: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
  },
  name: {
    fontSize: 18,
    color: "#727272",
    fontWeight: "800",
    paddingRight: 20,
  },
  userInfo: {
    fontSize: 17,
    color: "#778899",
    fontWeight: "600",
  },
  editContent: {
    // paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    // marginTop: 20,
  },
  editIcon: {
    width: 50,
    height: 40,
    padding: 0,
  },
  editsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    padding: 20,
  },
  buttonEditStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 150,
    justifyContent: "center",
  },
  buttonCancelStyle: {
    backgroundColor: "#707070",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#707070",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 150,
    justifyContent: "center",
  },
});
