import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useEffect, useState, createRef } from "react";
const HAS_LAUNCHED = "hasLaunched";

function setAppLaunched() {
  AsyncStorage.setItem(HAS_LAUNCHED, "true");
}

export default async function CheckerComponent() {
  try {
    const hasLaunched = await AsyncStorage.getItem(HAS_LAUNCHED);
    if (hasLaunched === null) {
      setAppLaunched();
      return true;
    }
    return false;
  } catch (error) {
    return false;
  }
}
