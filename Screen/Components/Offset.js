import React, { useEffect, useState, createRef } from "react";
import {
  Avatar,
  Card,
  Title,
  Paragraph,
  Divider,
} from "react-native-paper";
import { styles } from "../../Styles/Styles";
import { TouchableHighlight, ScrollView, View } from "react-native";
import { VStack, Center, Input, Text, Button } from "native-base";
import AsyncStorage from "@react-native-async-storage/async-storage";

const Offset = ({ navigation }) => {
  const offsetInputRef = createRef();
  const [offset, setOffset] = useState(null);
  const [amount, setAmount] = useState(0)
  const [readMore, setReadMore] = useState(false)
  const [totalEmission, setTotalEmission] = useState(0);
  const LeftContent = (props) => <Avatar.Icon {...props} icon="folder" />;

  useEffect(() => {
    if (offset === null) {
      setAmount(0);
    } else {
      setAmount(Math.round(offset * 14));
    }
  }, [offset]);
    useEffect(() => {
      setOffset(Math.ceil(totalEmission / 0.5) * 0.5);
    }, [totalEmission]);
  const readBtn = () => {
    setReadMore((prevReadMore) => !prevReadMore)
  }
    useEffect(() => {
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/total/footprint/user/" +
            user_id,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            // console.log(responseJson);
            if (responseJson.status === "success") {
              const user = responseJson.data;
              setTotalEmission(user.total);        
            }
          });
      });
    }, []);
  return (
    <ScrollView style={styles.itemsContainer}>
      <Card style={styles.offsetCard}>
        <Card.Title
        // titleStyle={styles.offsetTitle}
        // title="Mt Kenya Trust"
        // subtitle="Card Subtitle"
        // left={LeftContent}
        />
        <Card.Content>
          <Title style={styles.offsetTitle}>Mt Kenya Trust</Title>
        </Card.Content>
        <Card.Cover source={require("../../Image/mkenya.png")} />
        <Paragraph>
          Mount Kenya Trust (MKT) is a Kenyan not-for-profit working to protect
          and conserve the forest, water, people and wildlife around Africa’s
          second highest mountain.
        </Paragraph>
        {readMore && (
          <View>
            <Paragraph>
              Mount Kenya’s afromontane forests, vast bamboo stands and
              moorlands host incredible biodiversity including numerous species
              of IUCN concern such as African elephant and critically endangered
              mountain bongo, along with 81 endemic plant species.
            </Paragraph>

            <Paragraph>
              It is one of the largest forests remaining in Kenya and an UNESCO
              World Heritage Site, described as; “one of the most impressive
              landscapes of Eastern Africa, with its rugged glacier clad
              summits, Afro-Alipine Moorlands and diverse forests, which
              illustrate outstanding ecological processes.
            </Paragraph>
          </View>
        )}

        <Paragraph style={styles.offsetReadMore}>
          <Button onPress={readBtn} size="sm">
            Read more
          </Button>
        </Paragraph>

        {/* <Divider /> */}
        <View>
          <View style={styles.offsetContainer}>
            <Text bold style={styles.offsetTitleSmall}>
              My Offset
            </Text>
            <Text bold style={styles.offsetTitleRight}>
              {totalEmission + "t"}
            </Text>
          </View>
          {/* <View style={styles.offsetContainer}>
            <Text bold style={styles.offsetTitleSmall}>
              Minimum Offset
            </Text>
            <Text bold>{0.5}t</Text>
          </View> */}
          <View style={styles.offsetContainer}>
            <Text bold style={styles.offsetTitleSmall}>
              Offset my CO
              <Text style={{ fontSize: 10, lineHeight: 20, color: "#23A9C6" }}>
                2
              </Text>
            </Text>
            {/* <Input
              size="md"
              variant="underlined"
              placeholder="1.5t"
              ref={offsetInputRef}
              onChangeText={(offset) => setOffset(offset)}
            /> */}
            <Text bold style={styles.offsetTitleRight}>
              {offset + "t"}
            </Text>
          </View>
          <View style={styles.offsetContainer}>
            <Text bold style={styles.offsetTitleSmall}>
              Payment
            </Text>
            <Text bold style={styles.offsetTitleRight}>
              {"$" + amount}
            </Text>
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.offsetButtonStyle}
            underlayColor={"#7EC904"}
            activeOpacity={1}
            onPress={() => navigation.navigate("PaymentSubmission")}
          >
            <Text style={styles.buttonTextStyle}>Donate </Text>
          </TouchableHighlight>
        </View>
      </Card>
    </ScrollView>
  );
};

export default Offset;
