import React, { useEffect, useState, useCallback } from "react";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { useNavigation, CommonActions } from "@react-navigation/native";
import Loader from "../Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  SafeAreaView,
} from "react-native";

const CongratsScreen = () => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
  const [info, setInfo] = useState([]);

    const calculateCarbon = () => {
      setLoading(true);
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        // console.log(user_id);
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/footprint/user/" +
            user_id,
          {
            method: "POST",
            headers: {
              //Header Definition
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.status === "success") {
              setLoading(false);
            }
          })
          .catch((error) => {
            //Hide Loader
            setLoading(false);
            console.errorr(error);
          });
      });
      getData();
      // console.log(item);
    };
    const getData = () => {
      setLoading(true);
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        // console.log(user_id[1]);
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
            user_id,
          {
            method: "GET",
            headers: {
              //Header Definition
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson + "=========");
            if (responseJson.status === "success") {
              const user = responseJson.data;
              console.log(user);

              setInfo(user.breakdown);

              const newData = user.breakdown;
              console.log(user.total);

               setLoading(false);
            }
          });
      });
    };
    const homeButton = () => {
      // calculateCarbon()
      getData()
      navigation.navigate("Eco Profile", { screen: "Flight" });
      // navigation.dispatch(StackActions.popToTop());
    }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Loader loading={loading} />
      <View style={{ flex: 1, padding: 16 }}>
        <View style={{ alignItems: "center" }}>
          <Image
            source={require("../../Image/aboutreact.png")}
            style={{
              width: "100%",
              height: 150,
              resizeMode: "contain",
              margin: 5,
            }}
          />
        </View>
        <View style={styles.sectionCard}>
          <View>
            <Text style={styles.loginheaderStyle}>Congratulations!</Text>
          </View>
          <View style={styles.SectionStyle}>
            <Text style={styles.ValidateTextStyle}>
              Your vehicle details have been updated
            </Text>
          </View>
          <View></View>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={homeButton}
          >
            <Text style={styles.buttonTextStyle}>Next</Text>
          </TouchableOpacity>
        </View>

        <Text
          style={{
            fontSize: 18,
            textAlign: "center",
            color: "grey",
          }}
        ></Text>
        <Text
          style={{
            fontSize: 16,
            textAlign: "center",
            color: "grey",
          }}
        ></Text>
      </View>
    </SafeAreaView>
  );
};

export default CongratsScreen;
const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: "#FCFAFA",
  },
  SectionStyle: {
    flexDirection: "row",
    // height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
    marginBottom: 40,
    justifyContent: "center",
  },
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    // backgroundColor: '#ffffff',
    // borderColor: '#000',
    // shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
    // margin: 10,
    // borderRadius: 25,
    padding: 10,
    // paddingTop: 20,
    // paddingBottom: 20,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 0,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
});
const OtpStyle = StyleSheet.create({
  borderStyleBase: {
    width: 30,
    height: 45,
  },

  borderStyleHighLighted: {
    borderColor: "#03DAC6",
  },

  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
  },

  underlineStyleHighLighted: {
    borderColor: "#03DAC6",
  },
});
