import React, { useState, createRef } from "react";
import { Icon } from "react-native-elements";
import EcoProfileScreen from "../DrawerScreens/EcoProfile";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  SafeAreaView,
} from "react-native";

const ProfileHeader = ({navigation}) => {

  fetch("https://reducemyco2.farwell-consultants.com/api/v1", {});

  return (
    
      <View style={styles.sectionCard}>
        {/* <View>
          <Text style={styles.loginheaderStyle}>Eco Profile</Text>
        </View> */}

        {/* <View style={styles.header}>
          <View style={styles.headerContent}>
            <Icon
              color="#23A9C6"
              raised
              containerStyle={{}}
              disabledStyle={{}}
              iconProps={{}}
              iconStyle={{}}
              name="directions-car"
              onLongPress={() => console.log("onLongPress()")}
              onPress={() => console.log("onPress()")}
              size={30}
              type="material"
            />
            <Icon
              color="#23A9C6"
              containerStyle={{}}
              disabledStyle={{}}
              iconProps={{}}
              iconStyle={{}}
              name="flight"
              onLongPress={() => console.log("onLongPress()")}
              onPress={() => console.log("onPress()")}
              raised
              size={30}
              type="material"
            />
            <Icon
              color="#23A9C6"
              containerStyle={{}}
              disabledStyle={{}}
              iconProps={{}}
              iconStyle={{}}
              name="restaurant"
              onLongPress={() => console.log("onLongPress()")}
              onPress={() => console.log("onPress()")}
              raised
              size={30}
              type="material"
            />
            <Icon
              color="#23A9C6"
              containerStyle={{}}
              disabledStyle={{}}
              iconProps={{}}
              iconStyle={{}}
              name="house"
              onLongPress={() => console.log("onLongPress()")}
              onPress={() => console.log("onPress()")}
              raised
              size={30}
              type="material"
            />
          </View>
        </View> */}
        {/* <UserDetails/> */}
        <View style={styles.body}>
          <View style={styles.item}>
            <Text style={styles.info}>Do you own a Vehicle?</Text>
          </View>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={() => console.log("------")}
          >
            <Text style={styles.buttonTextStyle}>Yes</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonRegisterStyle}
            activeOpacity={0.5}
            onPress={() => console.log("------")}
          >
            <Text style={styles.buttonTextStyle}>No</Text>
          </TouchableOpacity>
        </View>
      </View>
    
  );
};

export default ProfileHeader;
const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: "#FCFAFA",
  },
  SectionStyle: {
    flexDirection: "row",
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
    marginBottom: 40,
    justifyContent: "space-around",
  },
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
    width: 300,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    // borderColor: "#000",
    // shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // elevation: 5,
    // margin: 10,
    // borderRadius: 25,
    // padding: 10,
    // paddingTop: 20,
    // paddingBottom: 20,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 0,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#727272",
    fontWeight: "bold",
  },
  userInfo: {
    fontSize: 12,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    marginTop: 10,
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    // flexDirection: "row",
    // paddingLeft: 30,
    // paddingRight: 30,
    width: 330,
  },
  vehicleSelect: {
    // width: 190,
  },
  fuelSelect: {
    // width: 190,
  },
  addContent: {
    paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
});