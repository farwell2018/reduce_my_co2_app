import React from "react";
import { styles } from "../../Styles/Styles";
import { VStack, Center, Heading, NativeBaseProvider } from "native-base";
import { ListItem, Icon } from "react-native-elements";
import { Text, View, ScrollView, Image } from "react-native";
const ReduceHome = () => {
     const list = [
       {
         title: "Switch lights off /Unplug",
         icon: "circle",
       },
       { title: "Choose organic and local", icon: "circle" },
       { title: "Adopt solar power usage", icon: "circle" },
       { title: "Don’t buy fast fashion", icon: "circle" },
       { title: "Use reusable shopping bags", icon: "circle" },
     ];
  return (
    <ScrollView style={styles.itemsContainer}>
      <View style={styles.reduceCard}>
        <View style={styles.homeHeaderStyle}>
          <Text style={styles.altMiniHeaderStyle}>
            How best to reduce your CO2
          </Text>
          <Image
            source={require("../../Image/Home.png")}
            style={styles.hintsIcon}
          />
          <Text style={styles.altHeaderStyle}>Home</Text>
              <View>
            {list.map((item, i) => (
              <ListItem key={i}>
                <Icon size={10} color="#7EC904" name={item.icon} />
                <ListItem.Content>
                  <ListItem.Title style={styles.hintsStyle}>
                    {item.title}
                  </ListItem.Title>
                </ListItem.Content>
              </ListItem>
            ))}
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default ReduceHome;
