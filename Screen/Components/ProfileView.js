import React, { useEffect, useState } from "react";
import Loader from "../Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Icon } from "react-native-elements";
import { useNavigation, useIsFocused } from "@react-navigation/native";
import { Avatar, Badge } from "react-native-elements";
import Edit from "../../Image/SVG/edit.svg";
import { Divider } from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  TouchableHighlight,
} from "react-native";

export const ProfileView = () => {
  const [data, setData] = useState(null);
  const [item, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const navigation = useNavigation();
  const isFocused = useIsFocused();

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      getUser();
    });
    return unsubscribe;
  }, [navigation]);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            console.log(user);
            setItems(user);
            setData(user.country);
            setLoading(false);
            console.log(data);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  const getUser = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            console.log(user);
            setItems(user);
            setData(user.country);
            setLoading(false);
            console.log(data);
          }
        })
        .catch((err) => console.log(err.message));
    });
  };

  const calculateCarbon = () => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/footprint/user/" +
          user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            console.log(responseJson.data);
            AsyncStorage.setItem("totalEmission", user.emissions).then(
              (res) => {}
            );
          }
        });
    });
  };
  const buttonPress = () => {
    navigation.navigate("Eco Tab");
  };

  // if (item.profile_is_complete === 0){
  //   return <ProfileScreen/>
  // }else
  return (
    <ScrollView style={styles.itemsContainer}>
      <Loader loading={loading} />
      <View style={styles.sectionCard}>
        <View></View>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <View>
              <Avatar
                activeOpacity={0.2}
                avatarStyle={{}}
                containerStyle={{ backgroundColor: "#BDBDBD" }}
                icon={{}}
                iconStyle={{}}
                imageProps={{}}
                overlayContainerStyle={{}}
                placeholderSource={require("../../Image/avatar/avatar_1.png")}
                rounded
                size={130}
                source={{
                  uri:
                    "https://reducemyco2.farwell-consultants.com/images/user_profile/" +
                    item.avatar,
                }}
                title={item.full_name}
                titleStyle={{}}
              />
              {/* <Badge
                status="primary"
                value="+"
                containerStyle={{ position: "absolute", bottom: 4, right: -4 }}
              /> */}
            </View>
            <View style={styles.userInfoContainer}>
              {item && <Text style={styles.name}>{item.full_name}</Text>}
              {item && <Text style={styles.userInfo}>{item.phone_number}</Text>}
              {item && <Text style={styles.userInfo}>{item.email} </Text>}
            </View>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.item}>
            <View style={styles.infoContent}>
              <Text style={styles.info}>Gender</Text>
            </View>
            <View style={styles.infoinner}>
              {item && <Text style={styles.infos}>{item.gender_type}</Text>}
            </View>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Divider my={2} />
          </View>
          <View style={styles.item}>
            <View style={styles.infoContent}>
              <Text style={styles.info}>Birth Year</Text>
            </View>
            <View style={styles.infoinner}>
              {item && <Text style={styles.infos}>{item.year_of_birth}</Text>}
            </View>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Divider my={2} />
          </View>
          <View style={styles.item}>
            <View style={styles.infoContent}>
              <Text style={styles.info}>Country</Text>
            </View>
            <View style={styles.infoinner}>
              {data &&<Text style={styles.infos}>{data.name}</Text>}
            </View>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Divider my={2} />
          </View>
          <View style={styles.item}>
            <View style={styles.infoContent}>
              <Text style={styles.info}>Twitter Handle</Text>
            </View>
            <View style={styles.infoinner}>
              {item && <Text style={styles.infos}>{item.twitter_handle}</Text>}
            </View>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Divider my={2} />
          </View>
          <View style={styles.item}>
            <View style={styles.infoContent}>
              <Text style={styles.info}>Measurements in</Text>
            </View>
            <View style={styles.infoinner}>
              {item && (
                <Text style={styles.infos}>
                  {item.user_preferred_measurement}
                </Text>
              )}
            </View>
          </View>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Divider my={2} />
          </View>
        </View>
        <View style={styles.addContent}>
          <Text style={{ color: "#707070" }}>Edit Profile</Text>
          <Edit
            color="#23A9C6"
            style={styles.editIcon}
            onLongPress={() => console.log("onLongPress()")}
            onPress={() => navigation.navigate("Edit Profile")}
            size={40}
          />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableHighlight
            style={styles.buttonStyle}
            underlayColor={"#7EC904"}
            activeOpacity={1}
            onPress={() => navigation.navigate("Eco Profile")}
          >
            <Text style={styles.buttonTextStyle}>
              Let’s calculate your CO
              <Text style={{ fontSize: 10, lineHeight: 37 }}>2</Text> Footprint
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    </ScrollView>
  );
};

export default ProfileView;
const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: "#FCFAFA",
  },
  SectionStyle: {
    flexDirection: "row",
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
    marginBottom: 40,
    justifyContent: "center",
  },
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    // paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    // borderColor: "#000",
    // shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    // marginTop: 20,
    // borderRadius: 25,
    // padding: 10,
    paddingTop: 40,
    // paddingBottom: 20,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 0,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    paddingRight: 30,
    paddingLeft: 30,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#727272",
    fontWeight: "bold",
  },
  userInfo: {
    fontSize: 12,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    // marginTop: 10,
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    // marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
    fontWeight: "100",
  },
  infoinner: {
    fontSize: 18,
    // marginTop: 20,
    color: "#727272",
    marginLeft: 50,
    fontWeight: "bold",
  },
  infos: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
    fontWeight: "bold",
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  addContent: {
    paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  editIcon: {
    width: 50,
    height: 50,
  },
  itemsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    // height: 700,
  },
});
