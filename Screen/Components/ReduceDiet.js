import React from "react";
import { styles } from "../../Styles/Styles";
import { VStack, Center, Heading, NativeBaseProvider } from "native-base";
import { Text, View, ScrollView, Image } from "react-native";
import { ListItem, Icon } from "react-native-elements";
const ReduceDiet = () => {
   const list = [
     {
       title: "Eat low on the food chain. i.e fruits, veggies, grains",
       icon: "circle",
     },
     { title: "Choose organic and local", icon: "circle" },
     { title: "Buy foodstuffs in bulk", icon: "circle" },
     { title: "Reduce your food waste", icon: "circle" },
     { title: "Compost", icon: "circle" },
   ];
  return (
    <ScrollView style={styles.itemsContainer}>
      <View style={styles.reduceCard}>
        <View style={styles.homeHeaderStyle}>
          <Text style={styles.altMiniHeaderStyle}>
            How best to reduce your CO2
          </Text>
          <Image
            source={require("../../Image/diet.png")}
            style={styles.hintsIcon}
          />
          <Text style={styles.altHeaderStyle}>Diet</Text>
          <View>
            {list.map((item, i) => (
              <ListItem key={i}>
                <Icon size={10} color="#7EC904" name={item.icon} />
                <ListItem.Content>
                  <ListItem.Title style={styles.hintsStyle}>
                    {item.title}
                  </ListItem.Title>
                </ListItem.Content>
              </ListItem>
            ))}
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default ReduceDiet;
