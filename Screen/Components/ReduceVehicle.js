import React from "react";
import { styles } from "../../Styles/Styles";
import { VStack, Center, Heading, NativeBaseProvider } from "native-base";
import Vehicle from '../../Image/SVG/vehicleHint.svg';
import { ListItem, Icon } from "react-native-elements";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  RefreshControl,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Image,
  SafeAreaView,
} from "react-native";
const ReduceVehicle = () => {
  
const list = [
  { title: "Walking to the shops", icon: "circle" },
  { title: "Biking to work", icon: "circle" },
  { title: "Car sharing/pooling", icon: "circle" },
  { title: "Low Emitting car", icon: "circle" },
];
  return (
    <ScrollView style={styles.itemsContainer}>
      <View style={styles.reduceCard}>
        <VStack space={4} alignItems="center">
          {/* <Vehicle style={styles.hintsIcon} /> */}

          <View style={styles.homeHeaderStyle}>
            <Text style={styles.altMiniHeaderStyle}>
              How best to reduce your CO2
            </Text>
            <Image
              source={require("../../Image/Vehicle.png")}
              style={styles.hintsIcon}
            />
            <Text style={styles.altHeaderStyle}>Vehicle</Text>
            <View>
              {list.map((item, i) => (
                <ListItem key={i}>
                  <Icon size={10} color="#7EC904" name={item.icon} />
                  <ListItem.Content>
                    <ListItem.Title style={styles.hintsStyle}>
                      {item.title}
                    </ListItem.Title>
                  </ListItem.Content>
                </ListItem>
              ))}
            </View>
          </View>
        </VStack>
      </View>
    </ScrollView>
  );
};

export default ReduceVehicle;
