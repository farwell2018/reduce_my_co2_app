import React from 'react';
import {StyleSheet, View, Modal, ActivityIndicator, Text} from 'react-native';
import AnimatedLoader from "react-native-animated-loader";

const Loader = (props) => {
  const {loading, ...attributes} = props;

  return (
    // <Modal
    //   transparent={true}
    //   animationType={'none'}
    //   visible={loading}
    //   onRequestClose={() => {
    //     console.log('close modal');
    //   }}>
    //   <View style={styles.modalBackground}>
    //     <View style={styles.activityIndicatorWrapper}>
    //       <ActivityIndicator
    //         animating={true}
    //         color="#000000"
    //         size="large"
    //         style={styles.activityIndicator}
    //       />
    //     </View>
    //   </View>
    // </Modal>
    <AnimatedLoader
      visible={loading}
      overlayColor="rgba(255,255,255,0.75)"
      source={require("../../assets/carbon_loader.json")}
      animationStyle={styles.lottie}
      speed={1}
    >
      <Text>
        Loading Reduce my CO
        <Text style={{ fontSize: 10, lineHeight: 37 }}>2 ...</Text>
      </Text>
    </AnimatedLoader>
  );
};

export default Loader;

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  },
    lottie: {
    width: 100,
    height: 100
  },
});