import React, { useEffect, useState } from "react";
import ProfileAvatar from "../Components/ProfileAvatar";
import { Avatar, Badge } from "react-native-elements";
import UploadImage from "../Components/ImageUpload";
import * as ImagePicker from "expo-image-picker";
import Loader from "../Components/Loader";
import ProfileView from "../Components/ProfileView";
import { styles } from "../../Styles/Styles";
import Toast from "react-native-toast-message";
import {
  useNavigation
} from "@react-navigation/native";
import {
  Input,
  FormControl,
  Select,
  useToast,
  CheckIcon,
  Center,
  NativeBaseProvider,
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  TouchableHighlight,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const PaymentSubmission = () => {
  
  const [userSelection, setSelection] = useState("");
  const [userNumber, setNumber] = useState("");
  const [item, setItems] = useState([]);
  const [otherNumber, setOtherNumber] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setErrors] = useState("");
  const navigation = useNavigation();

  useEffect(() => {
   
    if (userSelection === "use_other") {
      setOtherNumber(true);
    } else {
      setOtherNumber(false);
    }
  }, [userSelection]);
  
  const submitProfileButton = () => {
    setErrors("");
    setLoading(true);
    var formBody = JSON.stringify({
      selection: userSelection,
      number: userNumber,
    });
    console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/payment/make/user/" +
          user_id,
        {
          method: "POST",
          body: JSON.stringify({
            selection: userSelection,
            number: userNumber,
          }),
          headers: {
            //Header Defination
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          //Hide Loader
          // setLoading(false);
          // console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            // setLoading(false);
            // setProfileUpdated(true);
            //   //  toast.show({
            //   //    title: responseJson.status,
            //   //    status: "success",
            //   //    description: responseJson.msg,
            //   //    placement: "top",
            //   //  });
            //      Toast.show({
            //        type: "success",
            //        text1: responseJson.status,
            //        text2: responseJson.msg,
            //      });
            // alert(responseJson.msg);
          } else {
            // setErrors(responseJson.msg)
            // //  toast.show({
            // //    title: responseJson.msg,
            // //    status: "warning",
            // //    description: "",
            // //    placement: "top",
            // //  });
            //    Toast.show({
            //      type: "error",
            //      text1: responseJson.msg,
            //      text2: "",
            //    });
            // setLoading(false);
          }
        })

        .catch((error) => {
          //Hide Loader
          setLoading(false);
          console.error(error);
        });
    });
  };
  // const [data, setData] = useState([]);

  const buttonPress = () => {
    submitProfileButton()
    // navigation.navigate("My Profile");
  }
  // if (isProfileUpdate) {
  //   return <ProfileView />;
  // }
  return (
    <ScrollView style={{ flex: 1 }}>
      
      <View style={styles.profileSectionCard}>
       
        <View style={styles.body}>
          <View style={styles.item}>
            <View style={styles.infoContent}></View>
          </View>
          <FormControl isRequired>
            <Select
              // selectedValue={value}
              minWidth={200}
              variant="underlined"
              accessibilityLabel="Gender"
              placeholder="Select option"
              onValueChange={(userSelection) => setSelection(userSelection)}
              _selectedItem={{
                bg: "teal.600",
                endIcon: <CheckIcon size={5} />,
              }}
              mt={1}
            >

              <Select.Item label="Use registration number" value="registration_number" />
              <Select.Item label="Use alternative number" value="use_other" />
              
            </Select>
          </FormControl>
          <View style={styles.item}>
            <View style={styles.yearPicker}>
              <View>
              {otherNumber && (
                <Input
                  // defaultValue={item.year_of_birth}
                  variant="underlined"
                  placeholder="Alternative Phone Number"
                  inputContainerStyle={{
                    borderBottomColor: "#00000029",
                  }}
                  //   leftIcon={{ type: "font-awesome", name: "calendar" }}
                  errorMessage="Oops! that's not correct."
                  errorProps={{}}
                  onChangeText={(userNumber) => setNumber(userNumber)}
                />
              )}
              </View>
            </View>
          </View>
        </View>
        {/* {error != "" ? (
                <Text style={styles.errorTextStyle}>{error}</Text>
              ) : null} */}
        <TouchableHighlight
          style={styles.buttonRegisterStyle}
          underlayColor={"#23A9C6"}
          activeOpacity={1}
          onPress={buttonPress}
        >
          <Text style={styles.buttonTextStyle}>Donate</Text>
        </TouchableHighlight>
      </View>
    </ScrollView>
  );
};

export default PaymentSubmission;
