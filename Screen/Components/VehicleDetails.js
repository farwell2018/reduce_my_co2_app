import React, { useEffect, useState, createRef } from "react";
import { Icon } from "react-native-elements";
import EcoProfileScreen from "../DrawerScreens/EcoProfile";
import Loader from "../Components/Loader";
import CongratsScreen from "./Congratulations";
import {
  useNavigation,
  useFocusEffect,
  useIsFocused,
  StackActions,
} from "@react-navigation/native";
import {
  Stack,
  Input,
  FormControl,
  Select,
  VStack,
  CheckIcon,
  Center,
  HStack,
  useToast,
  Modal,
  Button,
} from "native-base";
import UploadImage from "./ImageUpload";
import Toast from "react-native-toast-message";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  TouchableHighlight,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

const VehicleDetails = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState("");
  const [addJourneys, setAddJourney] = useState(false);
  const [item, setItems] = useState([]);
  const [days, setDays] = useState("");
  const [distance, setDistance] = useState("");
  const [vehicle_name, setVehicleName] = useState("");
  const [journey, setJourney] = useState([]);
  const [journey_name, setJourneyName] = useState("");
  const [journey_vehicle, setjourneyvehicle] = useState("");
  const [journey_days, setjourneydays] = useState("");
  const [journey_distance, setjourneydistance] = useState("");
  const [isUpdate, setUpdateSuccess] = useState(false);
  const [newUsage, setnewUsage] = useState(true);
  const [error, setErrors] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [isUsage, setIsUsage] = useState(false);
  const [isComplete, setIsComplete] = useState(false);
  const [journeyCheck, setJourneyCheck] = useState(false);
  const [usageCheck, setUsageCheck] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const isFocused = useIsFocused();
  const distanceInputRef = createRef();
  const journey_distanceInputRef = createRef();
  const journey_daysInputRef = createRef();
  const toast = useToast();
  const navigation = useNavigation();

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      //  alert("Refreshed!");
      
      getData();
    });
    return unsubscribe;
  }, [navigation]);
const getData = () => {
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
            user_id,
          {
            method: "GET",
            headers: {
              //Header Definition
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            const user = responseJson.data;
            console.log(user);
            setItems(user.vehicles);
            // setData(user.preferred_measurement);
            if (user.preferred_measurement === 1) {
              setData("Miles");
            } else if (user.preferred_measurement === 0) {
              setData("KM");
            }
          })
          .catch((err) => console.log(err.message));
      });
}
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          const user = responseJson.data;
          console.log(user);
          setItems(user.vehicles);
          // setData(user.preferred_measurement);
          if (user.preferred_measurement === 1) {
            setData("Miles");
          } else if (user.preferred_measurement === 0) {
            setData("KM");
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  //  console.log(data);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/user/" +
          user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          const user = responseJson.data;
          // console.log(responseJson.data);
          // setItems(user);
        });
    });
  }, []);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id[1]);
      fetch("https://reducemyco2.farwell-consultants.com/api/v1/journeys/all", {
        method: "GET",
        headers: {
          //Header Definition
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + token,
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const items = responseJson.data;
            console.log(responseJson.data);
            setJourney(items);
            setLoading(false);
          }
        });
    });
  }, []);
  console.log(journey);
  const submitJourney = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      vehicle_name: journey_vehicle,
      journey: journey_name,
      days_per_week: journey_days,
      one_way_distance: journey_distance,
    });
    console.log(formBody);
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/user/journey/create",
        {
          method: "POST",
          body: JSON.stringify({
            vehicle_name: journey_vehicle,
            journey: journey_name,
            days_per_week: journey_days,
            one_way_distance: journey_distance,
          }),
          headers: {
            //Header Defination
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          //Hide Loader
          setLoading(true);
          // console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            setModalVisible(true);
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
               Toast.show({
                 type: "success",
                 text1: responseJson.status,
                 text2: responseJson.msg,
               });
            setUpdateSuccess(true);
            setLoading(false);
            setJourneyCheck(true);
          } else {
            // toast.show({
            //   title: responseJson.msg,
            //   status: "warning",
            //   description: "",
            //   placement: "top",
            // });
              Toast.show({
                type: "error",
                text1: responseJson.msg,
                text2: ""
              });
            setLoading(false);
            setErrors(responseJson.msg);
          }
        });
    });
  };
    const submitFirstJourney = () => {
      setLoading(true);
      var formBody = JSON.stringify({
        vehicle_name: journey_vehicle,
        journey: journey_name,
        days_per_week: journey_days,
        one_way_distance: journey_distance,
      });
      console.log(formBody);
      AsyncStorage.getItem("access_token", (err, value) => {
        let token = value;
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/user/journey/create",
          {
            method: "POST",
            body: formBody,
            headers: {
              //Header Defination
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            //Hide Loader
            setLoading(true);
            console.log(responseJson);
            // If server response message same as Data Matched
            if (responseJson.status === "success") {
              // setModalVisible(true);
              // toast.show({
              //   title: responseJson.status,
              //   status: "success",
              //   description: responseJson.msg,
              //   placement: "top",
              // });
                   Toast.show({
                     type: "success",
                     text1: responseJson.status,
                     text2: responseJson.msg,
                   });
              setUpdateSuccess(true);
              setLoading(false);
              setJourneyCheck(true);
            } else {
              //  setModalVisible(true);
              // toast.show({
              //   title: responseJson.message,
              //   status: "warning",
              //   description: responseJson.message,
              //   placement: "top",
              // });
                Toast.show({
                  type: "error",
                  text1: responseJson.message,
                  text2: "",
                });
              setLoading(false);
              setErrors(responseJson.msg);
            }
          });
      });
    };
  console.log(error);
  const submitVehicleButton = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      days_per_week: days,
      distance_per_day: distance,
      vehicle_name: vehicle_name,
    });
    console.log(formBody);
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/user/commute/create",
        {
          method: "POST",
          body: JSON.stringify({
            days_per_week: days,
            distance_per_day: distance,
            vehicle_name: vehicle_name,
          }),
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          //Hide Loader
          // setLoading(true);
          console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            // navigation.navigate("Journey Details");
            setUsageCheck(true);
            setLoading(false);
            setnewUsage(false);
            setAddJourney(true)
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
              Toast.show({
                type: "success",
                text1: responseJson.status,
                text2: responseJson.msg,
              });
          } else {
            setLoading(false);
            setErrors(responseJson.message);
            setErrorMessage(responseJson.errors);
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: "Please check the data and try again",
            //   placement: "top",
            // });
               Toast.show({
                 type: "error",
                 text1: responseJson.message,
                 text2: "Please check the data and try again",
               });
            if (days === "") {
              setErrors(errorMessage.days_per_week[0]);
              setLoading(false);
              return;
            }
            if (distance === "") {
              setErrors(errorMessage.distance_per_day[0]);
              setLoading(false);
              return;
            }
            if (vehicle_name === "") {
              setErrors(errorMessage.vehicle_name[0]);
              setLoading(false);
              return;
            }
          }
        });
    });
  };
  console.log(errorMessage + "============");
  const addUsage = () => {
    setLoading(true);
    submitVehicleButton();
    distanceInputRef.current.clear();
    setVehicleName("");
    setDays("");
    // vehicle_nameInputRef.current.select.clearValue();
    // daysInputRef.current.select.clearValue();
  };
  const calculateCarbon = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/footprint/user/" +
          user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            // toast.show({
            //   title: responseJson.status,
            //   status: "success",
            //   description: responseJson.msg,
            //   placement: "top",
            // });
              Toast.show({
                type: "success",
                text1: responseJson.status,
                text2: responseJson.msg,
              });
            setLoading(false);
          } else {
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: "Please check the data and try again",
            //   placement: "top",
            // });
              Toast.show({
                type: "error",
                text1: responseJson.message,
                text2: "Please check the data and try again",
              });
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader

          console.errorr(error);
        });
    });
    // getData();
    // console.log(item);
  };
  // const pushAction = StackActions.push("Eco Profile", { screen: "Flight" });
  const submitVehicleDetails = () => {
    setLoading(true);
    submitVehicleButton();
    navigation.navigate("Journey Details");

  };
  console.log(addJourneys);
  const addJourney = () => {
    submitFirstJourney();
    journey_distanceInputRef.current.clear();
    journey_daysInputRef.current.clear();
    setJourneyName("");
    setjourneyvehicle("");
  };
    const calculateButton = () => {
      calculateCarbon();
      navigation.navigate("Eco Profile", { screen: "Flight" });
    };
  if (isComplete) {
    return <CongratsScreen />;
  } else
    return (
      <ScrollView style={styles.itemsContainer}>
        <Loader loading={loading} />
        <View style={styles.sectionCard}>
          {/* <View style={styles.body}> */}

          <VStack space={4}>
            <View>
              <Text style={styles.loginheaderStyle}>Vehicle Usage</Text>
            </View>
            <Stack space={2} alignItems="center">
              <HStack space={2} alignItems="center">
                <Center>
                  <Input
                    minWidth={330}
                    variant="underlined"
                    placeholder="My commute is"
                    inputContainerStyle={{
                      borderBottomColor: "#fff",
                    }}
                    fontSize={14}
                    keyboardType="number-pad"
                    errorProps={{}}
                    ref={distanceInputRef}
                    onChangeText={(distance) => setDistance(distance)}
                    InputRightElement={data + " one way"}
                  />
                </Center>
                {/* <Center>
                    <Text style={styles.info}>{data + " one way"}</Text>
                  </Center> */}
              </HStack>
            </Stack>

            <Stack space={2} alignItems="center">
              <HStack space={2} alignItems="center">
                <Center>
                  <FormControl isRequired>
                    <Select
                      selectedValue={days}
                      width={330}
                      variant="underlined"
                      accessibilityLabel="Gender"
                      placeholder="How often do you use your car?"
                      onValueChange={(days) => setDays(days)}
                      InputRightElement={"per week"}
                      _selectedItem={{
                        bg: "teal.600",
                        endIcon: <CheckIcon size={5} />,
                      }}
                      fontSize={13}
                      mt={1}
                    >
                      <Select.Item label="1 day" value="1" />
                      <Select.Item label="2 days" value="2" />
                      <Select.Item label="3 days" value="3" />
                      <Select.Item label="4 days" value="4" />
                      <Select.Item label="5 days" value="5" />
                      <Select.Item label="6 days" value="6" />
                      <Select.Item label="7 days" value="7" />
                    </Select>
                  </FormControl>
                </Center>
                {/* <Center>
                    <Text style={styles.info}>per week</Text>
                  </Center> */}
              </HStack>
              <HStack space={2} alignItems="center">
                <Center>
                  <FormControl>
                    <Select
                      selectedValue={vehicle_name}
                      minWidth={330}
                      fontSize={14}
                      variant="underlined"
                      accessibilityLabel="Vehicle Name"
                      placeholder="Vehicle"
                      onValueChange={(vehicle_name) =>
                        setVehicleName(vehicle_name)
                      }
                      _selectedItem={{
                        bg: "teal.600",
                        endIcon: <CheckIcon size={5} />,
                      }}
                      ml={5}
                    >
                      {item &&
                        item.map((car, index) => (
                          <Select.Item
                            key={car.id}
                            label={car.vehicle_name}
                            value={car.vehicle_name}
                          />
                        ))}
                    </Select>
                  </FormControl>
                </Center>
                <Center>
                  {/* <Text style={styles.info}>{data + " one way"}</Text> */}
                </Center>
              </HStack>
            </Stack>
            {/* </View> */}
            <View style={styles.addCommute}>
              <Text style={{ color: "#707070" }}>Add Another</Text>
              <Icon
                color="#23A9C6"
                containerStyle={{}}
                disabledStyle={{}}
                iconProps={{}}
                iconStyle={{}}
                name="add-circle-outline"
                onLongPress={() => console.log("onLongPress()")}
                onPress={addUsage}
                size={40}
                type="material"
              />
            </View>
            <View style={styles.buttonContainer}>
              <TouchableHighlight
                style={styles.buttonStyle}
                underlayColor={"#7EC904"}
                activeOpacity={1}
                onPress={submitVehicleDetails}
              >
                <Text style={styles.buttonTextStyle}>Save</Text>
              </TouchableHighlight>
            </View>
          </VStack>

          {/* {addJourneys && (
            <VStack space={4}>
              <Text style={styles.miniheaderStyle}>
                Please provide details of other regular weekly journeys you make
              </Text>
              <Stack space={2} alignItems="center">
                <HStack space={2} alignItems="center">
                  <Center>
                    <FormControl isRequired>
                      <Select
                        selectedValue={journey_name}
                        minWidth={230}
                        variant="underlined"
                        accessibilityLabel="Journey"
                        placeholder="Journey"
                        onValueChange={(journey_name) =>
                          setJourneyName(journey_name)
                        }
                        _selectedItem={{
                          bg: "teal.600",
                          endIcon: <CheckIcon size={5} />,
                        }}
                        fontSize={14}
                        // mt={1}
                      >
                        {journey &&
                          journey.map((trip, index) => (
                            <Select.Item
                              key={trip.id}
                              label={trip.name}
                              value={trip.name}
                            />
                          ))}
                      </Select>
                    </FormControl>
                  </Center>
                  <Center>
                    <Input
                      width={130}
                      variant="underlined"
                      placeholder="Days"
                      ref={journey_daysInputRef}
                      inputContainerStyle={{
                        borderBottomColor: "#fff",
                      }}
                      fontSize={14}
                      keyboardType="number-pad"
                      errorProps={{}}
                      onChangeText={(journey_days) =>
                        setjourneydays(journey_days)
                      }
                    />
                  </Center>
                </HStack>
              </Stack>
              <Stack space={2} alignItems="center">
                <HStack space={2} alignItems="center">
                  <Center>
                    <FormControl isRequired>
                      <Select
                        selectedValue={journey_vehicle}
                        minWidth={230}
                        variant="underlined"
                        accessibilityLabel="Vehicle"
                        placeholder="Vehicle"
                        onValueChange={(journey_vehicle) =>
                          setjourneyvehicle(journey_vehicle)
                        }
                        _selectedItem={{
                          bg: "teal.600",
                          endIcon: <CheckIcon size={5} />,
                        }}
                        fontSize={14}
                        // mt={1}
                      >
                        {item &&
                          item.map((car, index) => (
                            <Select.Item
                              key={car.id}
                              label={car.vehicle_name}
                              value={car.vehicle_name}
                            />
                          ))}
                      </Select>
                    </FormControl>
                  </Center>
                  <Center>
                    <Input
                      width={130}
                      variant="underlined"
                      placeholder={data + " one way"}
                      ref={journey_distanceInputRef}
                      inputContainerStyle={{
                        borderBottomColor: "#fff",
                      }}
                      fontSize={14}
                      keyboardType="number-pad"
                      errorProps={{ errorMessage }}
                      onChangeText={(journey_distance) =>
                        setjourneydistance(journey_distance)
                      }
                    />
                  </Center>
                </HStack>
              </Stack>
              <View style={styles.addContent}>
                <Text style={{ color: "#707070" }}>Add Another</Text>
                <Icon
                  color="#23A9C6"
                  containerStyle={{}}
                  disabledStyle={{}}
                  iconProps={{}}
                  iconStyle={{}}
                  name="add-circle-outline"
                  onLongPress={() => console.log("onLongPress()")}
                  onPress={addJourney}
                  size={40}
                  type="material"
                />
              </View>
              <View style={styles.buttonContainer}>
                <TouchableHighlight
                  style={styles.buttonStyle}
                  underlayColor={"#7EC904"}
                  activeOpacity={1}
                  onPress={submitJourney}
                >
                  <Text style={styles.buttonTextStyle}>Save</Text>
                </TouchableHighlight>
              </View>
            </VStack>
          )} */}
          {/* <Modal
            isOpen={modalVisible}
            onClose={setModalVisible}
            size="lg"
            style={styles.centeredView}
          >
            <Modal.Content
              maxH="350"
              style={{ justifyContent: "center", alignItems: "center" }}
            >
              
              <Modal.Header>Eco Profile Update Successful</Modal.Header>
              <Modal.Body>
                <Center>
                  <Image
                    source={require("../../Image/aboutreact.png")}
                    style={{
                      width: "100%",
                      height: 150,
                      resizeMode: "contain",
                      margin: 5,
                    }}
                  />
                  <Text>
                    Press the button to calculate total Vehicle Emission.
                  </Text>
                </Center>
                <ScrollView></ScrollView>
              </Modal.Body>
              <Modal.Footer>
                <Button.Group space={2}>
              
                  <Button onPress={calculateButton}>Calculate</Button>
                </Button.Group>
              </Modal.Footer>
            </Modal.Content>
          </Modal> */}
        </View>
      </ScrollView>
    );
};

export default VehicleDetails;
const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: "#FCFAFA",
  },
  SectionStyle: {
    flexDirection: "row",
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
    marginBottom: 40,
    justifyContent: "center",
  },
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    borderColor: "#000",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // elevation: 5,
    // margin: 10,
    // borderRadius: 25,
    // padding: 10,
    // paddingRight: 20,
    paddingBottom: 20,
    // width:360,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    // textAlign: "center",
    fontSize: 22,
    // padding: 10,
    paddingLeft: 30,
    marginTop: 20,
  },
  miniheaderStyle: {
    color: "#23A9C6",
    // textAlign: "center",
    fontSize: 15,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 20,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    paddingRight: 30,
    paddingLeft: 30,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#727272",
    fontWeight: "bold",
  },
  userInfo: {
    fontSize: 12,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    // paddingRight: 10,
    // paddingLeft: 10,
    marginTop: 10,
    // width:300,
  },
  item: {
    // display: "flex",
    flexDirection: "row",
    marginTop: 10,
    // justifyContent: "space-evenly",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 12,
    // marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    // marginLeft: 50,
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
    // marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  addContent: {
    paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  addCommute: {
    display: "flex",
    paddingRight: 20,
    // paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  journeyStyles: {
    flexDirection: "column",
    width: 100,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    // height: 700,
  },
});
