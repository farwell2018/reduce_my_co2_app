import React from "react";
import { styles } from "../../Styles/Styles";
import { VStack, Center, Heading, NativeBaseProvider } from "native-base";
import { ListItem, Icon } from "react-native-elements";
import { Text, View, ScrollView, Image } from "react-native";
const ReduceFlight = () => {
  const list = [
    { title: "Avoid connecting flights", icon: "circle" },
    { title: "Fly Economy Class", icon: "circle" },
    { title: "Fly fewer round trips<", icon: "circle" },
  ];
  return (
    <ScrollView style={styles.itemsContainer}>
      <View style={styles.reduceCard}>
        <View style={styles.homeHeaderStyle}>
          <Text style={styles.altMiniHeaderStyle}>
            How best to reduce your CO2
          </Text>
          <Image
            source={require("../../Image/Plane.png")}
            style={styles.hintsIcon}
          />
          <Text style={styles.altHeaderStyle}>Flights</Text>
          <View>
            {list.map((item, i) => (
              <ListItem key={i}>
                <Icon size={10} color="#7EC904" name={item.icon} />
                <ListItem.Content>
                  <ListItem.Title style={styles.hintsStyle}>
                    {item.title}
                  </ListItem.Title>
                </ListItem.Content>
              </ListItem>
            ))}
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default ReduceFlight;

