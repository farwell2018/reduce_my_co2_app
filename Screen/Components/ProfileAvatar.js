import React, {useState, useEffect} from 'react';
import {Avatar, Badge, BottomSheet, Text} from 'react-native-elements';
import { View, Image, StyleSheet } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
const ProfileAvatar = () => {
   const [data, setData] = useState([]);
   const [item, setItems] = useState([]);
  
    // useEffect(() => {
    //   const unsubscribe = navigation.addListener("focus", () => {
    //     getUser();
    //   });
    //   return unsubscribe;
    // }, [navigation]);
    useEffect(() => {
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
            user_id,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.status === "success") {
              const user = responseJson.data;
              console.log(user);
              setItems(user);
      
            }
          })
          .catch((err) => console.log(err.message));
      });
    }, []);
  return (
    <View style={stylesSidebar.profileHeader}>
      <Avatar
        activeOpacity={0.2}
        avatarStyle={{}}
        containerStyle={{ backgroundColor: "#BDBDBD" }}
        icon={{}}
        iconStyle={{}}
        imageProps={{}}
        overlayContainerStyle={{}}
        // placeholderSource={require("../../Image/avatar/avatar_1.png")}
        rounded
        size="large"
        source={{
          uri:
            "https://reducemyco2.farwell-consultants.com/images/user_profile/" +
            item.avatar,
        }}
        title={item.full_name}
        titleStyle={{}}
      />
      <Text style={stylesSidebar.profileHeaderText}>{item.full_name}</Text>
    </View>
  );
};
export default ProfileAvatar;
const stylesSidebar = StyleSheet.create({
  profileHeaderText: {
    color: "white",
    alignSelf: "center",
    paddingHorizontal: 10,
    fontWeight: "bold",
  },
  profileHeader: {
    flexDirection: "row",
    backgroundColor: "#7EC904",
    padding: 15,
    textAlign: "center",
  },
});