import React, { useState } from "react";
import { ButtonGroup } from "react-native-elements";

const GenderButtons = () => {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [selectedIndexes, setSelectedIndexes] = useState([]);

  // console.log(selectedIndex);

  return (
    <ButtonGroup
      // buttonStyle={{width: 100}}
      buttonContainerStyle={{}}
      buttons={["Female", "Male", "Prefer not to say"]}
      containerStyle={{ width: 300 }}
      disabledStyle={{}}
      disabledTextStyle={{}}
      disabledSelectedStyle={{}}
      disabledSelectedTextStyle={{}}
      innerBorderStyle={{}}
      onPress={(index) => setSelectedIndex(index)}
      selectedButtonStyle={{}}
      selectedIndex={selectedIndex}
      selectedTextStyle={{}}
      textStyle={{ fontSize: 11 }}
    />
  );
};
export default GenderButtons;
