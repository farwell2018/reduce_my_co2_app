// import React, { useEffect, useState, useCallback } from "react";
// import Loader from "./Loader";
// import AsyncStorage from "@react-native-async-storage/async-storage";
// import { Card, ListItem, Button, Icon } from "react-native-elements";
// import { VStack, Spacer, useToast, NativeBaseProvider } from "native-base";
// import * as rssParser from "react-native-rss-parser";
// import { TabView, SceneMap } from 'react-native-tab-view';
// import {  useIsFocused } from '@react-navigation/native';
// import moment from 'moment';
// import {
//   StyleSheet,
//   View,
//   Text,
//   ScrollView,
//   Linking,
//   useWindowDimensions 
// } from "react-native";


// const NewsFeed = () => {
//   const [feed, setFeed] = useState([]);
//   const [loading, setLoading] = useState(false);
//   const isFocused = useIsFocused();

//   // useEffect(() => {
//   //   fetchData();
//   // }, [isFocused]);

//   useEffect(() => {
//     async function fetchData() {
//       setLoading(true);
//       const response = await fetch(
//         "http://media-tor.herokuapp.com/api/conservation"
//       )
//         .then((response) => response.json())
//          .then((responseJson) => {
        
      
//             const user = responseJson.data;
//             setFeed(responseJson);
//           setLoading(false);
//         })
        
//     }
//     fetchData();
//   }, []);



//   return (
//     <ScrollView style={styles.itemsContainer}>
//       {/* <Loader loading={loading} /> */}
//       <View style={styles.itemsContainer}>
//         <VStack space={1} alignItems="center">
//           {feed.map((u, i) => {
//             return (
//               <Card containerStyle={styles.card} key={u.id}>
//                 <View style={styles.cardStyle}>
//                   <Text numberOfLines={1} style={styles.name}>
//                     {moment(u.isoDate).format('Do MMM YYYY')}
//                   </Text>
//                 </View>
//                 <View style={styles.cardStyle}>
//                   <Text numberOfLines={4} style={styles.Title}>
//                     {u.title}
//                   </Text>
//                 </View>
//                 <View style={styles.cardStyle}>
//                   <Text numberOfLines={4} style={styles.description}>
//                     {u.content}
//                   </Text>
//                 </View>
//                 <View style={styles.cardStyle}>
//                   <Button
//                     buttonStyle={styles.newsButton}
//                     iconRight
//                     title="Read More"
//                     onPress={() => {
//                       Linking.openURL(u.link);
//                     }}
//                   />
//                 </View>
//               </Card>
//             );
//           })}
//         </VStack>
//       </View>
//     </ScrollView>
//   );
// };

// export default NewsFeed;
// const styles = StyleSheet.create({
//   loginheaderStyle: {
//     color: "#23A9C6",
//     fontSize: 20,
//     padding: 10,
//     paddingLeft: 30,
//     marginTop: 20,
//     paddingTop: 0,
//   },
//   name: {
//     fontSize: 13,
//     color: "#ACA9A9",
//     fontWeight: "100",
//     // paddingRight: 20,
//     // width: 120,
//   },
//   Title: {
//     fontSize: 17,
//     color: "#778899",
//     fontWeight: "bold",
//     width: 300,
//     flex: 1,
//   },
//   description: {
//     fontSize: 15,
//     color: "#778899",
//     fontWeight: "normal",
//     width: 300,
//     flex: 1,
//   },
//   itemsContainer: {
//     backgroundColor: "#fff",
//     flex: 1,
//     // height: 700,
//   },
//   card: {
//     width: "90%",
//     minWidth: 300,
//     borderRadius: 15,
//     elevation: 5,
//   },
//   cardStyle: {
//     flexDirection: "row",
//     justifyContent: "space-between",
//     alignContent: "center",
//     // borderRadius: 10,
//     padding: 6,
//   },
//   cellStyle: {
//     minWidth: 70,
//     padding: 2,
//   },
//   newsButton: {
//     borderRadius: 20,
//     backgroundColor: "#2EA9C4",
//     width: "80%",
//     height: 35,
//   },
// });
