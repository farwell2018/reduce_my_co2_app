import React, { useState, useEffect } from "react";
import { Avatar, Badge,} from "react-native-elements";
import {
  Spinner,
  HStack,
  Heading,
  Center,
  NativeBaseProvider,
} from "native-base";
import * as ImagePicker from "expo-image-picker";
import FormData from "form-data";
import mime from "mime";
import ProfilePicture from "react-native-profile-picture";
import {
  Image,
  View,
  Platform,
  TouchableOpacity,
  Text,
  StyleSheet,
  Button,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";


export const UploadImage = () => {
  const [file, setFile] = useState(null);
  const [image, setImage] = useState(null);
  const [uploading, startUploading] = useState(false);
  const [items, setItems] = useState([]);
  const [token, setToken] = useState();
  const [user_id, setId] = useState();

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const getMimeType = (ext) => {
    // mime type mapping for few of the sample file types
    switch (ext) {
      case "jpg":
        return "image/jpeg";
      case "jpeg":
        return "image/jpeg";
      case "png":
        return "image/png";
    }
  };
const getData = () => {
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        setToken(token);
        setId(user_id);
        console.log(token);
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
            user_id,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson + "====== here");
            const user = responseJson.data;
            setItems(user);
          });
      });
}
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      setToken(token);
      setId(user_id);
      console.log(token);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson + "====== here");
          const user = responseJson.data;
          setItems(user);
        });
    });
  }, []);
  console.log(items +"======");

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    //  console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
      uploadFile();
    }
    // return await fetch(
    //   "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/image/update/" +
    //     user_id,
    //   {
    //     method: "PUT",
    //     body: data,
    //     headers: {
    //       "content-type": 'multipart/form-data',
    //     },
    //   }
    // );
    
  };

  const uploadFile = async () => {
    if (file || image) {
      const fileUri = file ? file : image;
      let filename = fileUri.split("/").pop();

      const extArr = /\.(\w+)$/.exec(filename);
      const type = getMimeType(extArr[1]);
      setImage(null);
      setFile(null);
      startUploading(true);
      let fileType = fileUri
        .substring(fileUri.lastIndexOf(":") + 1, fileUri.lastIndexOf(";"))
        .split("/")
        .pop();
      // let imageToUpload = {
      //   uri: fileUri,
      //   name: filename,
      //   type: mime.getType(fileUri),
      // };
      let formData = new FormData();

      formData.append("avatar", {
        uri: fileUri,
        name: filename,
        type: mime.getType(fileUri),
      });
      console.log(formData);
        // var formBody = JSON.stringify({
        //   avatar: formData,
          
        // });
        // console.log(formBody);
      const response = await fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/image/update/" +
          user_id,
        {
          method: "POST",
          body: formData,
          headers: {
            Accept: "application/json",
            "content-type": "multipart/form-data",
            Authorization: "Bearer " + token,
          },
        }
      );
      startUploading(false);
      const responseAgain = await response.text();
       getData();
      console.log(responseAgain);
      return response;
    }
  };
  useEffect(() => {
    if (image !== null) {
      uploadFile();
    }
    getData()
  }, [image]);

console.log(image)
  return (
    <View>
      <View>
        <Avatar
          activeOpacity={0.2}
          avatarStyle={{}}
          containerStyle={{ backgroundColor: "#BDBDBD" }}
          icon={{}}
          iconStyle={{}}
          imageProps={{}}
          onLongPress={uploadFile}
          onPress={pickImage}
          overlayContainerStyle={{}}
          placeholderStyle={{}}
          placeholderSource={{ uri: image }}
          rounded
          size={120}
          source={{
            uri:
              image != null
                ? image
                : "https://reducemyco2.farwell-consultants.com/images/user_profile/" +
                  items.avatar,
          }} //else show random
          title=""
          titleStyle={{}}
        />
        {uploading ? (
          <Spinner
            style={{ position: "absolute", bottom: 4, right: 4 }}
            accessibilityLabel="Loading image"
          />
        ) : (
          <Badge
            onPress={uploadFile}
            status="primary"
            value="+"
            containerStyle={{ position: "absolute", bottom: 4, right: 4 }}
          />
        )}
      </View>
      {/* {uploading ? (
        <View>
          <Spinner accessibilityLabel="Loading posts" />
          <Heading color="primary.500" fontSize="md">
            Loading
          </Heading>
        </View>
      ) : (
        <Badge
          onPress={uploadFile}
          status="primary"
          value="+"
          containerStyle={{ position: "absolute", bottom: 4, right: 2 }}
        />
      )} */}
    </View>
  );
};
export default UploadImage;
// const imageUploaderStyles = StyleSheet.create({
//   container: {
//     elevation: 2,
//     height: 100,
//     width: 100,
//     backgroundColor: "#efefef",
//     position: "relative",
//     borderRadius: 999,
//     overflow: "hidden",
//   },
//   uploadBtnContainer: {
//     opacity: 0.7,
//     position: "absolute",
//     right: 0,
//     bottom: 0,
//     backgroundColor: "lightgrey",
//     width: "100%",
//     height: "25%",
//   },
//   uploadBtn: {
//     display: "flex",
//     alignItems: "center",
//     justifyContent: "center",
//   },
// });
