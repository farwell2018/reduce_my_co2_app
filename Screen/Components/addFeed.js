import React, { useEffect, useState, useCallback } from "react";
import OTPInputView from "@twotalltotems/react-native-otp-input";
import { useNavigation, CommonActions } from "@react-navigation/native";
import Loader from "../Components/Loader";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Toast from "react-native-toast-message";
import { ListItem, Icon } from "react-native-elements";
import axios from "axios";
import { VStack, CheckIcon, Avatar, Center, Modal, Select, Heading, FlatList, Text, Box, HStack, Spacer } from "native-base";
import {
    StyleSheet,
    TextInput,
    View,
    ScrollView,
    Image,
    Keyboard,
    TouchableOpacity,
    KeyboardAvoidingView,
    Button,
    SafeAreaView,
} from "react-native";

const AddFeed = () => {
    const navigation = useNavigation();
    const [loading, setLoading] = useState(true);
    const [info, setInfo] = useState([]);
    const [infos, setInfos] = useState(false);
    const [newsfeed, setNewsfeed] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [userFeed, setUserFeed] = useState("");
    const [selectedItems, setSelectedItems] = useState([]);

    useEffect(() => {
        if (newsfeed !== []) {
            setInfos(true)
        }
 
    }, [newsfeed])
   
    const postSelectedItems = () => {
        // setLoading(true)
        console.log(selectedItems);
        AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {

            let token = value[0][1];
            let user_id = value[1][1];
            const config = {
                headers: { Authorization: `Bearer ${token}` }
            };

            var formBody = JSON.stringify({
                newsfeed: selectedItems,
                user_id: user_id
            });
            console.log(formBody);
            fetch(
                "https://reducemyco2.farwell-consultants.com/api/v1/profile/newsfeed/create",
                {
                    method: "POST",
                    body: formBody,
                    headers: {
                        //Header Defination
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        Authorization: "Bearer " + token,
                    },
                }
            )
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.status === "success") {
                        const user = responseJson.data;
                        // console.log(user)
                        getUserNewsfeed();
                        setLoading(false);                  
                        Toast.show({
                            type: "success",
                            text1: responseJson.status,
                            text2: responseJson.msg,
                        });

                    }
                    else {
                        setLoading(false);
                    }
                });
            // .then(function (response) {
            //   console.log(response);
            //   setLoading(false);
            // })
            // .catch(function (error) {
            //   console.log(error);

            // });
        });
    };
    useEffect(() => {
        AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
            let token = value[0][1];
            let user_id = value[1][1];
            fetch(
                "https://reducemyco2.farwell-consultants.com/api/v1/newsfeed/user/" +
                user_id,
                {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        Authorization: "Bearer " + token,
                    },
                }
            )
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.status === "success") {
                        // console.log(responseJson.data);
                        const user = responseJson.data;
                        // console.log(user + "user");
                        setUserFeed(user);
                        setLoading(false);
                    }
                    else {
                        setLoading(false);
                        console.log("siko")
                    }
                });
        });
    }, []);
    const getUserNewsfeed = () => {
        AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
            let token = value[0][1];
            let user_id = value[1][1];
            fetch(
                "https://reducemyco2.farwell-consultants.com/api/v1/newsfeed/user/" +
                user_id,
                {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        Authorization: "Bearer " + token,
                    },
                }

            )
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.status === "success") {
                        // console.log(responseJson.data);
                        const user = responseJson.data;
                        // console.log(user + "user");
                        setUserFeed(user);
                        setLoading(false);
                    }
                    else {
                        setLoading(false);
                        console.log("siko")
                    }
                });
        });
    };

    useEffect(() => {
        async function getNews() {
            // setLoading(true)
            try {
                const response = await axios.get('https://reducemyco2.farwell-consultants.com/api/v1/newsfeed/all')
                    .then((response) => response.data)
                    .then((responseJson) => {
                       console.log(responseJson)
                        if (responseJson.status === "success") {
                            const feed = responseJson.data;
                            console.log(feed)
                            setNewsfeed(responseJson.data);
                           
                            setLoading(false)
                            
                        }
                        else {
                            // setLoading(true)
                        }
                    })
            } catch (error) {
                console.error(error);
            }
        }

        getNews();
    }, [])

    const getData = () => {
        setLoading(true);
        AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
            let token = value[0][1];
            let user_id = value[1][1];
            // console.log(user_id[1]);
            fetch(
                "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
                user_id,
                {
                    method: "GET",
                    headers: {
                        //Header Definition
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        Authorization: "Bearer " + token,
                    },
                }
            )
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(responseJson + "=========");
                    if (responseJson.status === "success") {
                        const user = responseJson.data;
                        console.log(user);

                        setInfo(user.breakdown);

                        const newData = user.breakdown;
                        console.log(user.total);

                        setLoading(false);
                    }
                });
        });
    };
    const homeButton = () => {
        // calculateCarbon()
        postSelectedItems()
        getData()
        // setTimeout(() => {
        //     navigation.navigate("NewsFeed"); 
        // }, 3000);
        
        // navigation.dispatch(StackActions.popToTop());
    }
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <Loader loading={loading} />
            < >
                <View style={styles.sectionCard}>
                    <View>
                        <Text style={styles.loginheaderStyle}>Add News Feeds</Text>
                    </View>
                    <Center>
                        <VStack space={4} alignItems="flex-start">
                            <Box>
                                <Heading fontSize="xl"  pb="3">
                    Selected Feeds
                  </Heading>
                                {(userFeed && userFeed.map(
                                    (item, index) => (
                                        // <Text fontSize="lg" key={index}>{item.title}</Text>
                                                      <ListItem key={index}>
                                            <Icon size={10} color="#7EC904" name="circle" />
                <ListItem.Content>
                  <ListItem.Title style={styles.hintsStyle}>
                    {item.title}
                  </ListItem.Title>
                </ListItem.Content>
              </ListItem>
                                    )))}
                                {infos && (
                                    <Select
                                        minWidth={200}
                                        variant="underlined"
                                        accessibilityLabel="Select your country"
                                        placeholder="Add News Feed"
                                        onValueChange={(selectedItems) => setSelectedItems(selectedItems)}
                                        _selectedItem={{
                                            bg: "teal.600",
                                            endIcon: <CheckIcon size={5} />,
                                        }}
                                        mt={1}
                                    >
                                        {newsfeed &&
                                            newsfeed.map((news, index) => (
                                                <Select.Item
                                                    key={index}
                                                    label={news.title}
                                                    value={news.id}
                                                />
                                            ))}
                                    </Select>
                                )}
                            </Box>
                        </VStack>
                        {/* <Loader loading={loading} /> */}
                        <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
                            <Modal.Content maxWidth="400px">
                                <Modal.CloseButton />
                                <Modal.Header>Selected Feeds</Modal.Header>
                                <Modal.Body>
                                    <Center>
                                        <Box>
                                
                                            {(userFeed && userFeed.map(
                                                (item, index) => (
                                                    <Text fontSize="lg" key={index}>{item.title}</Text>
                                                )))}
                                            {newsfeed && (
                                                <Select
                                                    minWidth={200}
                                                    variant="underlined"
                                                    accessibilityLabel="Select your country"
                                                    placeholder="Add News Feed"
                                                    onValueChange={(selectedItems) => setSelectedItems(selectedItems)}
                                                    _selectedItem={{
                                                        bg: "teal.600",
                                                        endIcon: <CheckIcon size={5} />,
                                                    }}
                                                    mt={1}
                                                >
                                                    {newsfeed &&
                                                        newsfeed.map((news, index) => (
                                                            <Select.Item
                                                                key={index}
                                                                label={news.title}
                                                                value={news.id}
                                                            />
                                                        ))}
                                                </Select>
                                            )}
                                        </Box>
                                        <VStack space={3} width={"100%"}>


                                        </VStack>
                                    </Center>
                                </Modal.Body>
                                <Modal.Footer>

                                    <Button
                                        onPress={postSelectedItems}
                                        title="Submit"
                                        color="#7EC904"
                                    />


                                </Modal.Footer>
                            </Modal.Content>
                        </Modal>
                    </Center>
                    <View></View>
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        activeOpacity={0.5}
                        onPress={homeButton}
                    >
                        <Text style={styles.buttonTextStyle}>Submit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        activeOpacity={0.5}
                        onPress={() => navigation.goBack()}
                    >
                        <Text style={styles.buttonTextStyle}>Back To NewsFeed</Text>
                    </TouchableOpacity>
                </View>

                <Text
                    style={{
                        fontSize: 18,
                        textAlign: "center",
                        color: "grey",
                    }}
                ></Text>
                <Text
                    style={{
                        fontSize: 16,
                        textAlign: "center",
                        color: "grey",
                    }}
                ></Text>
            </>
        </SafeAreaView>
    );
};

export default AddFeed;
const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: "#FCFAFA",
    },
    SectionStyle: {
        flexDirection: "row",
        // height: 40,
        marginTop: 20,
        marginLeft: 35,
        marginRight: 35,
        margin: 10,
        marginBottom: 40,
        justifyContent: "center",
    },
    buttonStyle: {
        backgroundColor: "#23A9C6",
        borderWidth: 0,
        color: "#FFFFFF",
        borderColor: "#23A9C6",
        height: 40,
        alignItems: "center",
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 25,
    },
    buttonRegisterStyle: {
        backgroundColor: "#7EC904",
        borderWidth: 0,
        color: "#FFFFFF",
        borderColor: "#7EC904",
        height: 40,
        alignItems: "center",
        borderRadius: 30,
        marginLeft: 35,
        marginRight: 35,
        marginTop: 20,
        marginBottom: 25,
    },
    buttonTextStyle: {
        color: "#FFFFFF",
        paddingVertical: 10,
        fontSize: 16,
    },
    inputStyle: {
        flex: 1,
        color: "#000",
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 0,
        borderBottomColor: "#707070",
        borderColor: "#fff",
    },
    registerTextStyle: {
        color: "#FFFFFF",
        textAlign: "center",
        fontWeight: "bold",
        fontSize: 14,
        alignSelf: "center",
        padding: 10,
    },
    errorTextStyle: {
        color: "red",
        textAlign: "center",
        fontSize: 14,
    },
    sectionCard: {
        // backgroundColor: '#ffffff',
        // borderColor: '#000',
        // shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5,
        // margin: 10,
        // borderRadius: 25,
        padding: 10,
        // paddingTop: 20,
        // paddingBottom: 20,
    },
    loginheaderStyle: {
        color: "#23A9C6",
        textAlign: "center",
        fontSize: 28,
        padding: 10,
        paddingLeft: 30,
        paddingTop:10,
    },
    ValidateTextStyle: {
        color: "#727272",
        fontSize: 15,
        textAlign: "center",
    },
    alternativeStyle: {
        color: "#727272",
        textAlign: "center",
        fontSize: 18,
        width: 50,
    },
    hintsStyle: {
        color: "#707070",
        textAlign: "left",
        fontSize: 18,
        // padding: 10,
        // paddingLeft: 30,
        paddingTop: 0,
        // fontWeight: "100",
    }
});
const OtpStyle = StyleSheet.create({
    borderStyleBase: {
        width: 30,
        height: 45,
    },

    borderStyleHighLighted: {
        borderColor: "#03DAC6",
    },

    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
    },

    underlineStyleHighLighted: {
        borderColor: "#03DAC6",
    },
});
