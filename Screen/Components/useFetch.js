import { useEffect, useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
const useFetch = (url) => {
  const [data, setData] = useState(null);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        url + user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setData(responseJson.data);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, [url]);

  return {data}
};
export default useFetch;
