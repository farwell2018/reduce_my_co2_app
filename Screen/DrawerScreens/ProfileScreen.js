import React, { useEffect, useState } from "react";
import ProfileAvatar from "../Components/ProfileAvatar";
import { Avatar, Badge } from "react-native-elements";
import UploadImage from "../Components/ImageUpload";
import * as ImagePicker from "expo-image-picker";
import Loader from "../Components/Loader";
import ProfileView from "../Components/ProfileView";
import { styles } from "../../Styles/Styles";
import Toast from "react-native-toast-message";
import {
  useNavigation
} from "@react-navigation/native";
import {
  Input,
  FormControl,
  Select,
  useToast,
  CheckIcon,
  Center,
  NativeBaseProvider,
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  TouchableHighlight,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const ProfileScreen = () => {
  const [userTwitter, setTwitter] = useState("");
  const [userGender, setGender] = useState("");
  const [userBirthYear, setBirthYear] = useState("");
  const [avatar, setAvatar] = useState();
  const [measurement, setMeasurement] = useState("");
  const [userCountry, setCountry] = useState("");
  const [data, setData] = useState([]);
  const [item, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isProfileUpdate, setProfileUpdated] = useState(false);
  const [error, setErrors] = useState("")
  const toast = useToast();
  const navigation = useNavigation();
  const [residence, setResidence] = useState("");

  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            console.log(user)
            setItems(user);
            setResidence(user.country.name)
            setLoading(false);
          }
          else {
            setLoading(false);
          }
        });
    });
  }, []);

console.log(item)
  const submitProfileButton = () => {
    setErrors("");
    setLoading(true);
    var formBody = JSON.stringify({
      twitter_handle: userTwitter,
      gender: userGender,
      year_of_birth: userBirthYear,
      preferred_measurement: measurement,
      country: userCountry,
    });
    console.log(formBody);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/update/" +
          user_id,
        {
          method: "PATCH",
          body: JSON.stringify({
            twitter_handle: userTwitter,
            gender: userGender,
            year_of_birth: userBirthYear,
            preferred_measurement: measurement,
            country: userCountry,
          }),
          headers: {
            //Header Defination
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          //Hide Loader
          // setLoading(false);
          // console.log(responseJson);
          // If server response message same as Data Matched
          if (responseJson.status === "success") {
            setLoading(false);
            setProfileUpdated(true);
              //  toast.show({
              //    title: responseJson.status,
              //    status: "success",
              //    description: responseJson.msg,
              //    placement: "top",
              //  });
                 Toast.show({
                   type: "success",
                   text1: responseJson.status,
                   text2: responseJson.msg,
                 });
            // alert(responseJson.msg);
          } else {
            setErrors(responseJson.msg)
            //  toast.show({
            //    title: responseJson.msg,
            //    status: "warning",
            //    description: "",
            //    placement: "top",
            //  });
               Toast.show({
                 type: "error",
                 text1: responseJson.msg,
                 text2: "",
               });
            setLoading(false);
          }
        })

        .catch((error) => {
          //Hide Loader
          setLoading(false);
          console.error(error);
        });
    });
  };
  // const [data, setData] = useState([]);
console.log(error)
  const [countries, setItem] = useState([]);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/countries/all")
      .then((response) => response.json())
      .then((responseJson) => {
        const countries = responseJson.data;
        setItem(countries);
        // console.log(countries);
      });
  }, []);
  const buttonPress = () => {
    submitProfileButton()
    navigation.navigate("My Profile");
  }
  // if (isProfileUpdate) {
  //   return <ProfileView />;
  // }
  return (
    <ScrollView style={{ flex: 1 }}>
      <Loader loading={loading} />
      <View style={styles.profileSectionCard}>
        <View>
          {/* <Text style={styles.loginheaderStyle}>Edit Profile</Text> */}
        </View>

        <View style={styles.header}>
          <View style={styles.headerContent}>
            <UploadImage />
            <View style={styles.userInfoContainer}>
              {item && <Text style={styles.name}>{item.full_name}</Text>}
              {item && <Text style={styles.userInfo}>{item.phone_number}</Text>}
              {item && <Text style={styles.userInfo}>{item.email} </Text>}
            </View>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.item}>
            <View style={styles.infoContent}></View>
          </View>
          <FormControl isRequired>
            <Select
              // selectedValue={value}
              minWidth={200}
              variant="underlined"
              accessibilityLabel="Gender"
              placeholder={
                item.gender_type == "Prefer not to say"
                  ? "Gender"
                  : item.gender_type
              }
              // defaultValue={item.gender_type}
              onValueChange={(userGender) => setGender(userGender)}
              _selectedItem={{
                bg: "teal.600",
                endIcon: <CheckIcon size={5} />,
              }}
              mt={1}
            >
              <Select.Item label="Male" value="Male" />
              <Select.Item label="Female" value="Female" />
              <Select.Item
                label="Prefer not to say"
                value="Prefer not to say"
              />
            </Select>
          </FormControl>
          <View style={styles.item}>
            <View style={styles.yearPicker}>
              <View>
                <Input
                  // defaultValue={item.year_of_birth}
                  variant="underlined"
                  placeholder={
                    item.year_of_birth == null
                      ? "Birth Year"
                      : item.year_of_birth
                  }
                  keyboardType="number-pad"
                  inputContainerStyle={{
                    borderBottomColor: "#00000029",
                  }}
                  //   leftIcon={{ type: "font-awesome", name: "calendar" }}
                  errorMessage="Oops! that's not correct."
                  errorProps={{}}
                  onChangeText={(userBirthYear) => setBirthYear(userBirthYear)}
                />
              </View>
            </View>
          </View>

          <View style={styles.item}>
            <View style={styles.countryPicker}>
              <FormControl isRequired>
                {/* <FormControl.Label>Select Country</FormControl.Label> */}
                <Select
                  // defaultValue={residence}
                  // selectedValue={userCountry}
                  minWidth={200}
                  variant="underlined"
                  accessibilityLabel="Select your country"
                  placeholder={
                    residence == "" ? "Country of Residence" : residence
                  }
                  onValueChange={(userCountry) => setCountry(userCountry)}
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={5} />,
                  }}
                  mt={1}
                >
                  {countries &&
                    countries.map((country, index) => (
                      <Select.Item
                        key={country.id}
                        label={country.name}
                        value={country.name}
                      />
                    ))}
                </Select>
              </FormControl>
            </View>
          </View>

          <View style={styles.item}>
            <View style={styles.iconContent} />
            <View style={styles.infoContent}>
              <View>
                <Input
                  // defaultValue={"@" + item.twitter_handle}
                  variant="underlined"
                  placeholder={
                    item.twitter_handle == null
                      ? "Twitter Handle"
                      : "@" + item.twitter_handle
                  }
                  inputContainerStyle={{
                    borderBottomColor: "#00000029",
                  }}
                  onChangeText={(userTwitter) => setTwitter(userTwitter)}
                />
              </View>
            </View>
          </View>
          <View style={styles.item}>
            <View style={styles.iconContent} />
            <View style={styles.infoContent}>
              <FormControl isRequired>
                <Select
                  // defaultValue={item.user_preferred_measurement}
                  // selectedValue={value}
                  minWidth={200}
                  variant="underlined"
                  accessibilityLabel="Preferred Measurement"
                  placeholder={
                    item.user_preferred_measurement == null
                      ? "Preferred Measurement"
                      : item.user_preferred_measurement
                  }
                  onValueChange={(measurement) => setMeasurement(measurement)}
                  _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size={5} />,
                  }}
                  mt={1}
                >
                  <Select.Item label="Metric" value="Metric" />
                  <Select.Item label="Imperial" value="Imperial" />
                </Select>
              </FormControl>
            </View>
          </View>
          <View style={styles.measureDetails}>
            <View style={styles.infoMetric}>
              <Text style={styles.infoHeader1}>Metric</Text>
              <Text style={styles.infos}>Distance - km</Text>
              <Text style={styles.infos}>Fluid - Litres</Text>
              <Text style={styles.infos}>Weight - kg</Text>
            </View>
            <View style={styles.infoImperial}>
              <Text style={styles.infoHeader}>Imperial</Text>
              <Text style={styles.infos1}>Distance - Miles</Text>
              <Text style={styles.infos1}>Fluid - Gallons</Text>
              <Text style={styles.infos1}>Weight - Lb</Text>
            </View>
          </View>
        </View>
        {/* {error != "" ? (
                <Text style={styles.errorTextStyle}>{error}</Text>
              ) : null} */}
        <TouchableHighlight
          style={styles.buttonRegisterStyle}
          underlayColor={"#23A9C6"}
          activeOpacity={1}
          onPress={buttonPress}
        >
          <Text style={styles.buttonTextStyle}>Save</Text>
        </TouchableHighlight>
      </View>
    </ScrollView>
  );
};

export default ProfileScreen;
