import * as React from "react";
import { View, useWindowDimensions, StyleSheet, Text } from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { Icon } from "react-native-elements";
import { HScrollView } from "react-native-head-tab-view";
import { CollapsibleHeaderTabView } from "react-native-tab-view-collapsible-header";
import ReduceVehicle from "../Components/ReduceVehicle";
import ReduceFlights from "../Components/ReduceFlights";
import ReduceDiet from "../Components/ReduceDiet";
import ReduceHome from "../Components/ReduceHome";
import Offset from '../Components/Offset'
import HintsScreen from "../HintsScreen";

const getTabBarIcon = ({ route, focused }) => {
  // const { route } = props;
  let iconColor = focused ? "#23A9C6" : "#c2c3c8";
  if (route.key === "eye") {
    return <Icon name="visibility" size={20} color={iconColor} raised />;
  } else if (route.key === "vehicle") {
    return <Icon name="directions-car" size={20} color={iconColor} raised />;
  } else if (route.key === "flight") {
    return <Icon name="flight" size={20} color={iconColor} raised />;
  } else if (route.key === "restaurant") {
    return <Icon name="restaurant" size={20} color={iconColor} raised />;
  } else {
    return <Icon name="house" size={20} color={iconColor} raised />;
  }
};
const FirstRoute = () => (
  <HScrollView index={0}>
    <HintsScreen />
  </HScrollView>
);

const SecondRoute = () => (
  <HScrollView index={1}>
    <ReduceVehicle />
  </HScrollView>
);
const ThirdRoute = () => (
  <HScrollView index={2}>
    <ReduceFlights />
  </HScrollView>
);
const FourthRoute = () => (
  <HScrollView index={3}>
    {/* <HouseholdScreen/> */}
    <ReduceDiet />
  </HScrollView>
);
const FifthRoute = () => (
  <HScrollView index={4}>
    {/* <HouseholdScreen/> */}
    <ReduceHome />
  </HScrollView>
);

const renderScene = SceneMap({
  eye: FirstRoute,
  vehicle: SecondRoute,
  flight: ThirdRoute,
  restaurant: FourthRoute,
  house: FifthRoute,
});

export default function EcoTabView({ navigation }) {
  const layout = useWindowDimensions();
  const [loading, setLoading] = React.useState(true);
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
     { key: "eye", title: "Eye" },
    { key: "vehicle", title: "Vehicle" },
    { key: "flight", title: "Flight" },
    { key: "restaurant", title: "Restaurant" },
    { key: "house", title: "House" },
  ]);

  return (
    <CollapsibleHeaderTabView
      renderScrollHeader={() => (
        // <View>
        //   <Text style={styles.loginheaderStyle}>Eco Profile</Text>
        // </View>
        <View style={{ height: 1, backgroundColor: "#fff" }} />
      )}
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
      renderTabBar={(props) => (
        <TabBar
          {...props}
          indicatorStyle={{ backgroundColor: "#23A9C6" }}
          renderIcon={(props) => getTabBarIcon(props)}
          tabStyle={styles.bubble}
          labelStyle={styles.noLabel}
          inactiveColor={"#fff"}
          activeColor={"#23A9C6"}
        />
      )}
    />
  );
}
const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  noLabel: {
    display: "none",
    height: 50,
  },
  bubble: {
    backgroundColor: "#fff",
    paddingHorizontal: 18,
    paddingVertical: 10,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 0,
    // height: 80,
    alignItems: "center",
    backgroundColor: "#fff",
    justifyContent: "center",
    paddingTop: 20,
    borderBottomColor: "#fff",
  },
});
