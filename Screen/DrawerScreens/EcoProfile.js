import React, { createRef, useEffect, useState } from "react";
import { Icon } from "react-native-elements";
import CongratsScreen from "../Components/Congratulations";
// import VehicleDetails from "../Components/VehicleDetails";
import { useNavigation } from "@react-navigation/native";
import Loader from "../Components/Loader";
import SuccessScreen from "../SuccessScreen";
import VehicleDetails from "../Components/VehicleDetails";
import EcoDisplayScreen from "../EcoDisplayScreen";
import {
  Input,
  FormControl,
  Select,
  useToast,
  CheckIcon,
  Center,
  Divider,
  Button,
  Modal
} from "native-base";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  TouchableHighlight,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const EcoProfileScreen = () => {
  const [vehicleType, setVehicle] = useState("");
  const [userFuel, setFuel] = useState("");
  const [vehicleName, setVehicleName] = useState("");
  const [fuelConsumption, setfuelConsumption] = useState("");
  const [fuelConsumptionAmount, setfuelConsumptionAmount] = useState("");
  const [user, setUser] = useState([]);
  const [fuel, setItemFuel] = useState([]);
  const [motorBike, setMotorBike] = useState([]);
  const [motorBikeType, setMotorBikeType] = useState("");
  const [carType, setCarType] = useState([]);
  const [isUpdate, setUpdateSuccess] = useState(false);
  const [addVehicles, setState] = useState(true);
  const [isMoto, setIsMoto] = useState(false);
  const navigation = useNavigation();
  const [next, setNext] = useState(false);
  const [loading, setLoading] = useState(true);
  const [ecoView, setEcoView] = useState(false);
  const [error, setErrors] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [congrats, setCongrats] = useState(false);
  const vehicleInputRef = createRef();
  const fuelAmountRef = createRef();
  const [disabled, setDisabled] = useState(false);
  const toast = useToast();
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    if (vehicleType === "Motorbike") {
      setIsMoto(true);
    } else {
      setIsMoto(false);
    }
  }, [vehicleType]);
 
  // useEffect(() => {
  //   if (fuelConsumption === "No") {
  //     setDisabled(true);
  //   }else if (fuelConsumption === "Yes"){
  //      setDisabled(false);
  //   }
  // }, [fuelConsumption]);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setUser(user);
            // console.log(user.owns_vehicle);
            if (user.owns_vehicle === 0) {
              setEcoView(true);
              setLoading(false);
            }
          }
        });
    });
  }, []);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/motorbikes/all")
      .then((response) => response.json())
      .then((responseJson) => {
        const bike = responseJson.data;
        setMotorBike(bike);
        //   console.log(vehicle)
      });
  }, []);
  //  console.log(motorBike)
    useEffect(() => {
      fetch("https://reducemyco2.farwell-consultants.com/api/v1/vehicles/all")
        .then((response) => response.json())
        .then((responseJson) => {
          const carType = responseJson.data;
          setCarType(carType);
          //   console.log(vehicle)
        });
    }, []);
  useEffect(() => {
    fetch("https://reducemyco2.farwell-consultants.com/api/v1/fuels/all")
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.status === "success") {
          const fuel = responseJson.data;
          setItemFuel(fuel);
          // console.log(fuel);
          setLoading(false);
        }
      });
  }, []);
  const addVehicle = () => {
    saveVehicleButton();
    setFuel("");
    setVehicle("");
    setMotorBikeType("")
    setfuelConsumptionAmount("")
    vehicleInputRef.current.clear();
    fuelAmountRef.current.clear();
    // if (isUpdate) {
    //   setState((prevaddVehicles) => !prevaddVehicles);
    // } else {

    // }
  };
  const nextButton = () => {
    submitVehicleButton();
    getUser();
    navigation.navigate("Eco Profile", { screen: "Flight" });
    setNext(true);
  };
  const getUser = () => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            const userInfo = responseJson.data;
            // setUser(user);
            // console.log(userInfo);
          }
        });
    });
  };
  const saveVehicleButton = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      vehicle_name: vehicleName,
      vehicle_type: vehicleType,
      vehicle_fuel_type: userFuel,
      knows_fuel_consumption: fuelConsumption,
      fuel_per_week: fuelConsumptionAmount,
      motorbike_type: motorBikeType,
    });
    // console.log(formBody);
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/eco/vehicles/create",
        {
          method: "POST",
          body: formBody,
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            setUpdateSuccess(true);
            setState(true);
            setLoading(false);
            // setModalVisible(true);
            toast.show({
              title: responseJson.status,
              status: "success",
              description: responseJson.msg,
              placement: "top",
            });
           
          } else {
            setLoading(false);
            setErrors(responseJson.message);
            setErrorMessage(responseJson.errors);
            toast.show({
              title: responseJson.message,
              status: "info",
              description: "Vehicle Details were not updated",
              placement: "top",
            });
            if (vehicleName === "") {
              setErrors(errorMessage.vehicle_name[0]);
              setLoading(false);
              return;
            }
            if (vehicleType === "") {
              setErrors(errorMessage.vehicle_type[0]);
              setLoading(false);
              return;
            }
            if (userFuel === "") {
              setErrors(errorMessage.vehicle_fuel_type[0]);
              setLoading(false);
              return;
            }
          }
          calculateCarbon();
        })

        .catch((error) => { console.log(error) });
    });
  };
  const submitVehicleButton = () => {
    setLoading(true);
    var formBody = JSON.stringify({
      vehicle_name: vehicleName,
      vehicle_type: vehicleType,
      vehicle_fuel_type: userFuel,
      knows_fuel_consumption: fuelConsumption,
      fuel_per_week: fuelConsumptionAmount,
      motorbike_type: motorBikeType,
    });
    // console.log(formBody);
    AsyncStorage.getItem("access_token", (err, value) => {
      let token = value;
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/eco/vehicles/create",
        {
          method: "POST",
          body: formBody,
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson);
          if (responseJson.status === "success") {
            setUpdateSuccess(true);
            setState(true);
            setLoading(false);
            // setModalVisible(true);
            toast.show({
              title: responseJson.status,
              status: "success",
              description: responseJson.msg,
              placement: "top",
            });
            // calculateCarbon();
            // alert(responseJson.msg);
          } else {
            setLoading(false);
            setErrors(responseJson.message);
            setErrorMessage(responseJson.errors);
            toast.show({
              title: responseJson.message,
              status: "info",
              description: "Vehicle Details were not updated",
              placement: "top",
            });
            if (vehicleName === "") {
              setErrors(errorMessage.vehicle_name[0]);
              setLoading(false);
              return;
            }
            if (vehicleType === "") {
              setErrors(errorMessage.vehicle_type[0]);
              setLoading(false);
              return;
            }
            if (userFuel === "") {
              setErrors(errorMessage.vehicle_fuel_type[0]);
              setLoading(false);
              return;
            }
          }
          calculateCarbon();
        })

        .catch((error) => {});
    });
  };
    const calculateCarbon = () => {
    setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      // console.log(user_id);
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/vehicles/footprint/user/" +
          user_id,
        {
          method: "POST",
          headers: {
            //Header Definition
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
              Toast.show({
                type: "success",
                text1: responseJson.status,
                text2: responseJson.msg,
              });
            setLoading(false);
          } else {
            // toast.show({
            //   title: responseJson.message,
            //   status: "warning",
            //   description: "Please check the data and try again",
            //   placement: "top",
            // });
              Toast.show({
                type: "error",
                text1: responseJson.message,
                text2: "Please check the data and try again",
              });
            setLoading(false);
          }
        })
        .catch((error) => {
          //Hide Loader

          console.errorr(error);
        });
    });
    // getData();
    // console.log(item);
  };
     const calculateButton = () => {
      calculateCarbon();
      navigation.navigate("Eco Profile", { screen: "Flight" });
    };

    return (
      <ScrollView style={styles.itemsContainer}>
        <View style={styles.sectionCard}>
          <View>
            <Center>
            <Text style={styles.loginheaderStyle}>
               Vehicle details
            </Text>
            </Center>

          </View>
          {addVehicles && (
            <View style={styles.body}>
              <View style={styles.itemVehicle}>
                <View style={styles.inputStyle}>
                  <Input
                    ref={vehicleInputRef}
                    variant="underlined"
                    placeholder="Vehicle Name"
                    inputContainerStyle={{
                      borderBottomColor: "#00000029",
                    }}
                    errorProps={{}}
                    onChangeText={(vehicleName) => setVehicleName(vehicleName)}
                  />
                </View>
              </View>
              <View style={styles.itemVehicle}>
                <View style={styles.inputStyle}>
                  <FormControl isRequired style={styles.vehicleSelect}>
                    <Select
                      selectedValue={vehicleType}
                      minWidth={200}
                      variant="underlined"
                      accessibilityLabel="Type of vehicle"
                      placeholder="Type of vehicle"
                      onValueChange={(vehicleType) => setVehicle(vehicleType)}
                      _selectedItem={{
                        bg: "teal.600",
                        endIcon: <CheckIcon size={5} />,
                      }}
                      mt={1}
                    >
                      {carType &&
                        carType.map((car, index) => (
                          <Select.Item
                            key={car.id}
                            label={car.vehicle_name}
                            value={car.vehicle_name}
                          />
                        ))}
                    </Select>
                  </FormControl>
                </View>
                {/* {isMoto && (
                  <View style={styles.inputStyle}>
                    <FormControl isRequired style={styles.fuelSelect}>
                      <Select
                        selectedValue={motorBikeType}
                        minWidth={200}
                        variant="underlined"
                        accessibilityLabel="Motorcycle Type"
                        placeholder="Motorcycle Type"
                        onValueChange={(motorBikeType) =>
                          setMotorBikeType(motorBikeType)
                        }
                        _selectedItem={{
                          bg: "teal.600",
                          endIcon: <CheckIcon size={5} />,
                        }}
                        mt={1}
                      >
                        {motorBike &&
                          motorBike.map((bike, index) => (
                            <Select.Item
                              key={bike.id}
                              label={bike.type}
                              value={bike.type}
                            />
                          ))}
                      </Select>
                    </FormControl>
                  </View>
                )} */}
                <View style={styles.inputStyle}>
                  <FormControl isRequired style={styles.fuelSelect}>
                    <Select
                      selectedValue={userFuel}
                      minWidth={200}
                      variant="underlined"
                      accessibilityLabel="Fuel Type"
                      placeholder="Fuel Type"
                      onValueChange={(fuel) => setFuel(fuel)}
                      _selectedItem={{
                        bg: "teal.600",
                        endIcon: <CheckIcon size={5} />,
                      }}
                      mt={1}
                    >
                      {fuel &&
                        fuel.map((fuels, index) => (
                          <Select.Item
                            key={fuels.id}
                            label={fuels.fuel_name}
                            value={fuels.fuel_name}
                          />
                        ))}
                    </Select>
                  </FormControl>
                </View>

                <View style={styles.inputStyle}>
                  <Input
                    ref={fuelAmountRef}
                    variant="underlined"
                    placeholder="Average amount spent on fuel each week"
                    inputContainerStyle={{
                      borderBottomColor: "#00000029",
                    }}
                    //   leftIcon={{ type: "font-awesome", name: "calendar" }}
                    isDisabled={disabled}
                    errorProps={{}}
                    onChangeText={(fuelConsumptionAmount) =>
                      setfuelConsumptionAmount(fuelConsumptionAmount)
                    }
                  />
                </View>
              </View>
            </View>
          )}
          <View style={styles.addContent}>
            <Text style={{ color: "#707070" }}>Add Another</Text>
            <Icon
              color="#23A9C6"
              containerStyle={{}}
              disabledStyle={{}}
              iconProps={{}}
              iconStyle={{}}
              name="add-circle-outline"
              onLongPress={() => console.log("onLongPress()")}
              onPress={addVehicle}
              size={40}
              type="material"
            />
          </View>
          {error != "" ? (
            <Text style={styles.errorTextStyle}>{error}</Text>
          ) : null}
          <View style={styles.buttonContainer}>
            <TouchableHighlight
              style={styles.buttonStyle}
              underlayColor={"#7EC904"}
              activeOpacity={1}
              onPress={nextButton}
            >
              <Text style={styles.buttonTextStyle}>Save</Text>
            </TouchableHighlight>
          </View>
        </View>
                  <Modal
            isOpen={modalVisible}
            onClose={setModalVisible}
            size="lg"
            style={styles.centeredView}
          >
            <Modal.Content
              maxH="350"
              style={{ justifyContent: "center", alignItems: "center" }}
            >
              
              <Modal.Header>Eco Profile Update Successful</Modal.Header>
              <Modal.Body>
                <Center>
                  <Image
                    source={require("../../Image/aboutreact.png")}
                    style={{
                      width: "100%",
                      height: 150,
                      resizeMode: "contain",
                      margin: 5,
                    }}
                  />
                  <Text>
                    Press the button to calculate total Vehicle Emission.
                  </Text>
                </Center>
                <ScrollView></ScrollView>
              </Modal.Body>
              <Modal.Footer>
                <Button.Group space={2}>
              
                  <Button onPress={calculateButton}>Calculate</Button>
                </Button.Group>
              </Modal.Footer>
            </Modal.Content>
          </Modal>
      </ScrollView>
    );
  // } else
  //   return (
  //     <SafeAreaView style={{ flex: 1 }}>
  //       <View style={{ flex: 1, padding: 16 }}>
  //         <Loader loading={loading} />
  //       </View>
  //     </SafeAreaView>
  //   );
};

export default EcoProfileScreen;
const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    // marginLeft: 45,
    // marginRight: 45,
    marginTop: 40,
    marginBottom: 25,
    width: 300,
    justifyContent: "center",
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 45,
    marginRight: 45,
    marginTop: 20,
    marginBottom: 25,
    width: 300,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    // borderColor: "#000",
    // shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    // elevation: 5,
    // margin: 10,
    // borderRadius: 25,
    // padding: 10,
    // paddingTop: 20,
    // paddingBottom: 20,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    // textAlign: "center",
    fontSize: 20,
    // padding: 10,
    // paddingLeft: 30,
    marginTop: 20,
  },
  ValidateTextStyle: {
    color: "#727272",
    fontSize: 15,
    textAlign: "center",
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
  header: {
    justifyContent: "center",
    // backgroundColor: '#DCDCDC',
  },
  headerContent: {
    // paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 22,
    color: "#727272",
    fontWeight: "bold",
  },
  userInfo: {
    fontSize: 12,
    color: "#778899",
    fontWeight: "600",
  },
  body: {
    // backgroundColor: '#778899',
    // height: 500,
    alignItems: "center",
    justifyContent: "center",
    paddingRight: 30,
    paddingLeft: 30,
    marginTop: 20,
  },
  item: {
    flexDirection: "row",
    marginTop: 10,
    justifyContent: "space-around",
  },
  infoContent: {
    flex: 1,
    fontSize: 18,
    color: "#707070",
    marginTop: 20,
  },
  countryPicker: {
    flex: 1,
    zIndex: 1000,
  },
  yearPicker: {
    flex: 1,
    zIndex: 2000,
  },
  iconContent: {
    // flex: 1,
    alignItems: "flex-end",
    // paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 10,
    color: "#727272",
  },
  infoUsage: {
    fontSize: 14,
    // marginTop: 10,
    color: "#727272",
  },
  infoinner: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    marginLeft: 50,
  },
  infos: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    // textAlign: 'right',
  },
  infos1: {
    fontSize: 14,
    marginTop: 10,
    color: "#727272",
    textAlign: "right",
  },
  infoMetric: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingRight: 60,
    textAlign: "right",
  },
  infoImperial: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    color: "#727272",
    paddingLeft: 60,
  },
  infoHeader: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "right",
  },
  infoHeader1: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#727272",
    textAlign: "left",
  },
  userInfoContainer: {
    marginLeft: 30,
  },
  buttons: {
    flexDirection: "row",
  },
  measureDetails: {
    flexDirection: "row",
  },
  itemVehicle: {
    // flexDirection: "row",
    // paddingLeft: 30,
    // paddingRight: 30,
    width: 360,
  },
  vehicleSelect: {
    // width: 190,
  },
  fuelSelect: {
    // width: 190,
  },
  addContent: {
    paddingRight: 20,
    paddingLeft: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 20,
  },
  itemUsage: {
    // display: "flex",
    flexDirection: "row",
    marginTop: 10,
    // justifyContent: "space-evenly",
  },
  usageInputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  infoinnerUsage: {
    fontSize: 18,
    marginTop: 20,
    color: "#727272",
    // marginLeft: 50,
  },
  infoinners: {
    fontSize: 18,
    marginTop: 5,
    color: "#727272",
    // marginLeft: 50,
  },
  miniheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 15,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 20,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  itemsContainer: {
    backgroundColor: "#fff",
    flex: 1,
    // height: 700,
  },
});
