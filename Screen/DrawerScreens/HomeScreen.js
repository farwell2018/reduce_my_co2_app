import React, { useEffect, useState, useCallback } from "react";
import ProfileScreen from "./ProfileScreen";
import {
  useNavigation,
  useFocusEffect,
  useIsFocused,
} from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Circle, G, Line, Svg } from "react-native-svg";
import { VictoryPie, VictoryLabel, VictoryTheme } from "victory-native";
import { styles } from "../../Styles/Styles";
import Calculator from "../../Image/SVG/Calculator.svg";
import Reduce from "../../Image/SVG/reduce.svg";
import Offset from "../../Image/SVG/offset.svg";
import Target from "../../Image/SVG/targets.svg";
import Food from "../../Image/SVG/food_icon.svg";
import House from "../../Image/SVG/house_icon.svg";
import Car from "../../Image/SVG/car_icon.svg";
import Plane from "../../Image/SVG/plane_icon.svg";
import Newsfeed from "../../Image/SVG/newsfeed.svg";
import Loader from "../Components/Loader";
import Vehicle from "../../Image/SVG/vehicleHint.svg";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  RefreshControl,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Image,
  SafeAreaView,
} from "react-native";
import { useToast } from "native-base";
export const HomeScreen = () => {
  const [item, setItems] = useState([]);
  const [profile, setProfile] = useState([]);
  const [profileComplete, setProfileComplete] = useState();
  const [info, setInfo] = useState([]);
  const [isfetch, setFetchSuccess] = useState(false);
  const [loading, setLoading] = useState(true);
  const [firstEmission, setFirstEmission] = useState(0);
  const [householdEmission, setHouseholdEmission] = useState(25);
  const [flightEmission, setFlightEmission] = useState(25);
  const [foodEmission, setFoodEmission] = useState(25);
  const [totalEmission, setTotalEmission] = useState(0);
  const [vehicleEmission, setVehicleEmission] = useState(0);
  const [dateCalculated, setCalculated] = useState(null);
  const navigation = useNavigation();
  const [country, setCountry] = useState(null);
  const toast = useToast();
  const [countryEmission, setCountryEmission] = useState(null);
  const data = [
    { y: flightEmission },
    { y: householdEmission },
    { y: foodEmission },
    { y: vehicleEmission },
  ];
  const colorScale = ["#FB7D23", "#FFF636", "#7EC925", "#2EA9C4"];
  console.log(flightEmission);
  // useEffect(() => {
  //   const unsubscribe = navigation.addListener("focus", () => {
  //     //  alert("Refreshed!");
  //     getUser();
  //     getData();
  //   });
  //   return unsubscribe;
  // }, [navigation]);
  const isFocused = useIsFocused();
  useEffect(() => {
    getUser();
    getData();
  }, [isFocused]);
  useEffect(() => {
    if (country === "Kenya") {
      setCountryEmission(0.03);
    } else if (country === "United Kingdom") {
      setCountryEmission(0.45);
    }
  }, [country]);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setProfile(user);
            setProfileComplete(user.profile_is_complete);
            setLoading(false);
            setCountry(user.country.name);
          }
        })
        .catch((err) => console.log(err.message));
    });
  }, []);
  // console.log(country);
  const getUser = () => {
    // setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
          user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setProfile(user);
            setProfileComplete(user.profile_is_complete);
            setLoading(false);
            setCountry(user.country.name);
          } else {
            // setLoading(false);
          }
        });
    });
  };
  // console.log(flightEmission);
  useEffect(() => {
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/total/footprint/user/" +
          user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          if (responseJson.status === "success") {
            const user = responseJson.data;
            // console.log(user);
            setRefreshing(false);
            setInfo(user.breakdown);

            const newData = user.breakdown;
            setTotalEmission(user.total);
            setVehicleEmission(user.vehicle.percentage);
            setFlightEmission(user.flight.percentage);
            setFoodEmission(user.food.percentage);
            setHouseholdEmission(user.household.percentage);
            setCalculated(user.date_last_calculated);
            //  setLoading(false);
          }
        });
    });
  }, []);
  const getData = () => {
    // setLoading(true);
    AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
      let token = value[0][1];
      let user_id = value[1][1];
      fetch(
        "https://reducemyco2.farwell-consultants.com/api/v1/total/footprint/user/" +
          user_id,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
          },
        }
      )
        .then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.status === "success") {
            const user = responseJson.data;
            setRefreshing(false);
            setInfo(user.breakdown);
            const newData = user.breakdown;
            setTotalEmission(user.total);
            setVehicleEmission(user.vehicle.percentage);
            setFlightEmission(user.flight.percentage);
            setFoodEmission(user.food.percentage);
            setHouseholdEmission(user.household.percentage);
            setLoading(false);
          } else {
            setLoading(false);
          }
        });
    });
  };
  const wait = (timeout) => {
    return new Promise((resolve) => setTimeout(resolve, timeout));
  };
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    getData();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  AsyncStorage.getItem("totalEmission", (err, value) => {
    let totalEmission = value;
  });
  const SuccessScreen = () => {
    const navigation = useNavigation();
    const [loading, setLoading] = useState(true);
    const [profileComplete, setProfileComplete] = useState();
    const [profile, setProfile] = useState([]);
    useEffect(() => {
      const unsubscribe = navigation.addListener("focus", () => {
        getUser();
      });
      return unsubscribe;
    }, [navigation]);
    useEffect(() => {
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
            user_id,
          {
            method: "GET",
            headers: {
              //Header Definition
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.status === "success") {
              const user = responseJson.data;
              setProfile(user);
              setProfileComplete(user.profile_is_complete);
              setLoading(false);
            }
          })
          .catch((err) => console.log(err.message));
      });
    }, []);
    const getUser = () => {
      setLoading(true);
      AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
        let token = value[0][1];
        let user_id = value[1][1];
        fetch(
          "https://reducemyco2.farwell-consultants.com/api/v1/user/profile/" +
            user_id,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: "Bearer " + token,
            },
          }
        )
          .then((response) => response.json())
          .then((responseJson) => {
            if (responseJson.status === "success") {
              const user = responseJson.data;
              setProfile(user);
              setProfileComplete(user.profile_is_complete);
              setLoading(false);
            } else {
              // setLoading(false);
            }
          });
      });
    };

    const getStarted = () => {
      getUser();
      setProfileComplete(1)
      navigation.navigate("Edit Profile");
    };
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Loader loading={loading} />
        <View style={{ flex: 1, padding: 16 }}>
          <View style={{ alignItems: "center" }}>
            <Image
              source={require("../../Image/aboutreact.png")}
              style={{
                width: "100%",
                height: 150,
                resizeMode: "contain",
                margin: 5,
              }}
            />
          </View>
          <View style={styles.sectionCard}>
            <View>
              <Text style={styles.loginheaderStyle}>Congratulations!</Text>
            </View>
            <View style={styles.SectionStyle}>
              <Text style={styles.ValidateTextStyle}>
                Your registration is complete. Now lets’ start reducing our
                carbon footprint.
              </Text>
            </View>
            <View></View>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={getStarted}
            >
              <Text style={styles.buttonTextStyle}>Lets get going…</Text>
            </TouchableOpacity>
          </View>

          <Text
            style={{
              fontSize: 18,
              textAlign: "center",
              color: "grey",
            }}
          ></Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: "center",
              color: "grey",
            }}
          ></Text>
        </View>
      </SafeAreaView>
    );
  };

  const getStarted = () => {
    setProfileComplete(1);
    getUser();
    navigation.navigate("Edit Profile");
  };
  if (profileComplete === 1) {
    return (
      <ScrollView
        style={{ flex: 1 }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <Loader loading={loading} />
        <View style={styles.sectionCard}>
          <View style={styles.homeHeaderStyle}>
            <Text style={styles.miniheaderStyle}>
              My Eco Profile for {dateCalculated}
            </Text>
            {vehicleEmission === 0 || flightEmission === 0 ? (
              <Text style={styles.miniheaderStyle}>Refresh</Text>
            ) : null}
          </View>
          <View style={styles.pieHero}>
            <View style={styles.mainAxisLeft}>
              <View style={styles.topIcons}>
                <Car style={styles.chartIconStyles} />
                <Text>{vehicleEmission + "%"}</Text>
              </View>
              <View style={styles.bottomIcons}>
                <Food style={styles.chartIconStyles} />
                <Text>{foodEmission + "%"}</Text>
              </View>
            </View>
            <Svg viewBox="0 0 400 250">
              <VictoryPie
                standalone={false}
                width={400}
                height={250}
                data={data}
                colorScale={colorScale}
                animate={{ easing: "exp" }}
                innerRadius={100}
                labelRadius={100}
                labels={() => null}
                style={{ labels: { fontSize: 20, fill: "white" } }}
              />
              <VictoryLabel
                textAnchor="middle"
                style={{ fontSize: 20, fill: "#23A9C6" }}
                x={200}
                y={120}
                text={[totalEmission + "t", " CO2 per month"]}
              />
            </Svg>
            <View style={styles.mainAxisRight}>
              <View style={styles.topIcons}>
                <Plane style={styles.chartIconStyles} />
                <Text>{flightEmission + "%"}</Text>
              </View>
              <View style={styles.bottomIcons}>
                <House style={styles.chartIconStyles} />
                <Text>{householdEmission + "%"}</Text>
              </View>
            </View>
          </View>
        { country && <Text style={styles.ValidateTextStyle}>
            Average CO
            <Text style={{ fontSize: 10, lineHeight: 20 }}>2</Text> emission in{" "}
            {country} is {""}
            {countryEmission}t per month
          </Text>}
          <Text style={styles.ValidateTextStyle}>
            Global average CO
            <Text style={{ fontSize: 10, lineHeight: 37 }}>2</Text> footprint is
            0.37t per month
          </Text>
          <View>
            <Text style={styles.miniheaderStyle}>
              Now its time to take action.
            </Text>
          </View>
          <View>
            <View style={styles.iconItem}>
              <TouchableOpacity onPress={() => navigation.navigate("Eco Tab")}>
                <View style={styles.iconBox}>
                  <Reduce style={styles.iconStyles} />
                  <Text style={styles.iconText}>
                    Reduce my CO
                    <Text style={{ fontSize: 7 }}>2</Text>{" "}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("NewsFeed")}>
                <View style={styles.iconBox}>
                  <Newsfeed style={styles.iconStyles} />
                  <Text style={styles.iconText}>Newsfeed</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.iconItem}>
              <TouchableOpacity
                onPress={() => navigation.navigate("Eco Profile")}
              >
                <View style={styles.iconBox}>
                  <Calculator style={styles.iconStyles} />
                  <Text style={styles.iconText}>Recalculate</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Target")}>
                <View style={styles.iconBox}>
                  <Target style={styles.iconStyles} />
                  <Text style={styles.iconText}>My Targets</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate("Offset")}>
                <View style={styles.iconBox}>
                  <Offset style={styles.iconStyles} />
                  <Text style={styles.iconText}>
                    Offset my CO
                    <Text style={{ fontSize: 7 }}>2</Text>
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  } else if (profileComplete === 0)
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Loader loading={loading} />
        <View style={{ flex: 1, padding: 16 }}>
          <View style={{ alignItems: "center" }}>
            <Image
              source={require("../../Image/aboutreact.png")}
              style={{
                width: "100%",
                height: 150,
                resizeMode: "contain",
                margin: 5,
              }}
            />
          </View>
          <View style={styles.sectionCard}>
            <View>
              <Text style={styles.loginheaderStyle}>Congratulations!</Text>
            </View>
            <View style={styles.SectionStyle}>
              <Text style={styles.ValidateTextStyle}>
                Your registration is complete. Now lets’ start reducing our
                carbon footprint.
              </Text>
            </View>
            <View></View>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={getStarted}
            >
              <Text style={styles.buttonTextStyle}>Lets get going…</Text>
            </TouchableOpacity>
          </View>

          <Text
            style={{
              fontSize: 18,
              textAlign: "center",
              color: "grey",
            }}
          ></Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: "center",
              color: "grey",
            }}
          ></Text>
        </View>
      </SafeAreaView>
    );
  else
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1, padding: 16 }}>
          <Loader loading={loading} />
        </View>
      </SafeAreaView>
    );
};

export default HomeScreen;
