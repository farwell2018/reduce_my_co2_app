import React from "react";
import { styles } from "../Styles/Styles";
import { VStack, Center, Heading, HStack } from "native-base";
import { Icon } from "react-native-elements";
import { Text, View, ScrollView } from "react-native";
const HintsScreen = () => {
  return (
    <ScrollView style={styles.itemsContainer}>
      <View style={styles.reduceCard}>
        <View style={styles.homeHeaderStyle}>
          <Text style={styles.altHeaderOrange}>IT’S TIME FOR ACTION</Text>
        </View>
        <View style={styles.homeHeaderStyle}>
          <Text style={styles.offsetTextStyle}>
            According to 2017 report by the UK Department for Business, Energy
            and Industrial Strategy transportation and households contribute 34%
            and 17% respectively to Green House Gas (GHG) emissions.
          </Text>
          <Text style={styles.offsetTextStyle}>
            Reducing personal CO2 emissions and can seem like a daunting task.
            However, simple lifestyle changes can have a massive impact.
          </Text>
          {/* <Text style={styles.offsetTextBlue}>
            Click on an icon to set a CO2 reduction target and start making a
            difference every day.
          </Text> */}
        </View>
        {/* <HStack space={3} alignItems="center">
          <Icon name="directions-car" size={20} color="#23A9C6" raised />
          <Icon name="flight" size={20} color="#23A9C6" raised />
          <Icon name="restaurant" size={20} color="#23A9C6" raised />
          <Icon name="house" size={20} color="#23A9C6" raised />
        </HStack> */}
      </View>
    </ScrollView>
  );
};

export default HintsScreen;
