import React, { useState, createRef } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  Card,
  TouchableHighlight,
} from "react-native";

import AsyncStorage from "@react-native-async-storage/async-storage";
import PasswordInputText from "react-native-hide-show-password-input";
import Loader from "./Components/Loader";

const LoginScreen = ({ navigation }) => {
  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [errortext, setErrortext] = useState("");

  const passwordInputRef = createRef();

  const handleSubmitPress = () => {
    setErrortext("");
    if (!userEmail) {
      alert("Please fill Email");
      return;
    }
    if (!userPassword) {
      alert("Please fill Password");
      return;
    }
    setLoading(true);
    let dataToSend = { email: userEmail, password: userPassword };
    let formBody = [];
    for (let key in dataToSend) {
      let encodedKey = encodeURIComponent(key);
      let encodedValue = encodeURIComponent(dataToSend[key]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    fetch("https://reducemyco2.farwell-consultants.com/api/v1/auth/signin", {
      method: "POST",
      body: JSON.stringify({
        email: userEmail,
        password: userPassword,
      }),
      headers: {
        //Header Defination
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //Hide Loader
        setLoading(false);
        // If server response message same as Data Matched
        if (responseJson.status === "success") {
          setLoading(false)
          AsyncStorage.multiSet([
            ["access_token", responseJson.access_token],
            ["user_id", JSON.stringify(responseJson.user_id)],
          ]).then((res) => {
            AsyncStorage.multiGet(["access_token", "user_id"], (err, value) => {
              // console.log(value);
            });
          });
          navigation.replace("DrawerNavigationRoutes");
        } else {
          setErrortext(responseJson.msg);
          console.log(responseJson.msg);
        }
      })
      .catch((error) => {
        //Hide Loader
        setLoading(false);
        console.error(error);
      });
  };

  return (
    // <View style={styles.mainBody}>
    // <Loader loading={loading} />
    <ScrollView>
      <Loader loading={loading} />
      <View style={styles.mainBody}>
        <KeyboardAvoidingView enabled>
          <View style={{ alignItems: "center" }}>
            <Image
              source={require("../Image/aboutreact.png")}
              style={{
                width: "100%",
                height: 150,
                resizeMode: "contain",
                margin: 5,
              }}
            />
          </View>
          <View style={styles.sectionCard}>
            <View>
              <Text style={styles.loginheaderStyle}>Log In</Text>
            </View>
            <View style={styles.SectionStyle}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(UserEmail) => setUserEmail(UserEmail)}
                placeholder="Email Address" //dummy@abc.com
                placeholderTextColor="#8b9cb5"
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                onSubmitEditing={() =>
                  passwordInputRef.current && passwordInputRef.current.focus()
                }
                underlineColorAndroid="#f000"
                blurOnSubmit={false}
              />
            </View>
            <View style={styles.PasswordSectionStyle}>
              <PasswordInputText
                style={styles.PasswordinputStyle}
                onChangeText={(UserPassword) => setUserPassword(UserPassword)}
                label=" Password" //12345
                baseColor="#8b9cb5"
                placeholderTextColor="#8b9cb5"
                keyboardType="default"
                ref={passwordInputRef}
                onSubmitEditing={Keyboard.dismiss}
                blurOnSubmit={false}
                secureTextEntry={true}
                underlineColorAndroid="#f000"
                returnKeyType="next"
                fontSize={15}
                lineWidth={1}
                iconColor="#8b9cb5"
              />
            </View>
            {errortext != "" ? (
              <Text style={styles.errorTextStyle}>{errortext}</Text>
            ) : null}
            <TouchableHighlight
              style={styles.buttonStyle}
              underlayColor={"#7EC904"}
              activeOpacity={1}
              onPress={handleSubmitPress}
            >
              <Text style={styles.buttonTextStyle}>Log In</Text>
            </TouchableHighlight>

            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginBottom: 10,
                marginTop: 10,
              }}
            >
              <View
                style={{
                  flex: 1,
                  height: 1,
                  backgroundColor: "black",
                  marginLeft: 30,
                }}
              />
              <View>
                <Text style={styles.alternativeStyle}>Or</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  height: 1,
                  backgroundColor: "black",
                  marginRight: 30,
                }}
              />
            </View>

            <TouchableHighlight
              style={styles.buttonRegisterStyle}
              underlayColor={"#23A9C6"}
              activeOpacity={1}
              onPress={() => navigation.navigate("RegisterScreen")}
            >
              <Text style={styles.buttonTextStyle}>Register</Text>
            </TouchableHighlight>
            {/* <TouchableOpacity
                style={styles.buttonRegisterStyle}
                activeOpacity={0.5}
                onPress={() => navigation.navigate('ValidationScreen')}>
                <Text style={styles.buttonTextStyle}>Validation</Text>
              </TouchableOpacity> */}
          </View>
        </KeyboardAvoidingView>
      </View>
    </ScrollView>
  );
};
export default LoginScreen;

const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: "#FCFAFA",
  },
  PasswordSectionStyle: {
    flexDirection: "row",
    // height: 40,
    // marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    // margin: 10,
  },
  PasswordinputStyle: {
    flex: 1,
    color: "#000",
    // paddingLeft: 15,
    // paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    // borderBottomColor: "#707070",
    borderColor: "#fff",
  },
  SectionStyle: {
    flexDirection: "row",
    height: 40,
    marginTop: 20,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  buttonStyle: {
    backgroundColor: "#23A9C6",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#23A9C6",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  buttonRegisterStyle: {
    backgroundColor: "#7EC904",
    borderWidth: 0,
    color: "#FFFFFF",
    borderColor: "#7EC904",
    height: 40,
    alignItems: "center",
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  buttonTextStyle: {
    color: "#FFFFFF",
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: "#000",
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 0,
    borderBottomColor: "#8b9cb5",
    borderColor: "#fff",
  },
  registerTextStyle: {
    color: "#FFFFFF",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 14,
    alignSelf: "center",
    padding: 10,
  },
  errorTextStyle: {
    color: "red",
    textAlign: "center",
    fontSize: 14,
  },
  sectionCard: {
    backgroundColor: "#ffffff",
    borderColor: "#000",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    margin: 10,
    borderRadius: 25,
    padding: 10,
    paddingTop: 20,
    paddingBottom: 20,
  },
  loginheaderStyle: {
    color: "#23A9C6",
    textAlign: "center",
    fontSize: 28,
    padding: 10,
    paddingLeft: 30,
    paddingTop: 0,
  },
  alternativeStyle: {
    color: "#727272",
    textAlign: "center",
    fontSize: 18,
    width: 50,
  },
});
